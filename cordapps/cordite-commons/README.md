<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Module `cordite-commons`

These are a set of general-purpose capabilities that are shared across cordite cordapps. They can also be used in other, bespoke, cordapps.

## Features

1. [Interface for simplifying Braid configuration](src/main/kotlin/io/cordite/commons/braid/BraidCordaService.kt).
2. Useful [database](src/main/kotlin/io/cordite/commons/database/Database.kt) extension functions for managing transactions and executing vendor-agnostic SQL statements.
3. An implementation of [Data Distribution Groups](src/main/kotlin/io/cordite/commons/distribution/README.md).
4. A utility to [initialise Jackson](src/main/kotlin/io/cordite/commons/utils/jackson/CorditeJacksonInit.kt) with all the necessary serialisers.
5. Extension methods to work with [ServiceHub](src/main/kotlin/io/cordite/commons/utils/ServiceHubUtils.kt).
6. Extension methods to work with the [Vault](src/main/kotlin/io/cordite/commons/utils/VaultUtilities.kt).
7. Extension methods to work with [java resources](src/main/kotlin/io/cordite/commons/utils/Resources.kt).
8. Extension methods for working with [Vertx Futures](src/main/kotlin/io/cordite/commons/utils/Vertx.kt).
9. A very useful [log4jJSON Layout](src/main/kotlin/io/cordite/commons/log4j/CorditeJsonLayout.kt).
