/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.braid

import io.vertx.core.json.Json
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.core.json.jackson.DatabindCodec
import kotlin.reflect.KClass

/**
 * Simple functions to query JSON strings, [JsonObject] and [JsonArray] using
 * <a href="http://tools.ietf.org/html/draft-ietf-appsawg-json-pointer-03">JSON Pointer</a> syntax
 */
object JsonQuery {

  /**
   * JSON Pointer escape code for the character '~'
   */
  const val TILDA = "~0"

  /**
   * JSON Pointer escape code for the character '/
   */
  const val SLASH = "~1"

  // INLINE functions

  inline fun <reified T : Any> String.queryOrNull(expression: String): T? {
    return queryOrNull(this, expression, T::class)
  }

  inline fun <reified T : Any> JsonObject.queryOrNull(expression: String): T? {
    return queryOrNull(this, expression, T::class)
  }

  inline fun <reified T : Any> JsonArray.queryOrNull(expression: String): T? {
    return queryOrNull(this, expression, T::class)
  }

  inline fun <reified T : Any> String.query(expression: String): T {
    return query(this, expression, T::class)
  }

  inline fun <reified T : Any> JsonObject.query(expression: String): T {
    return query(this, expression, T::class)
  }

  inline fun <reified T : Any> JsonArray.query(expression: String): T {
    return query(this, expression, T::class)
  }

  // Static functions
  fun <T : Any> query(json: String, expression: String, returnType: KClass<T>): T {
    return queryOrNull(json, expression, returnType) ?: error("could not find $expression")
  }

  @Suppress("UNCHECKED_CAST")
  fun <T : Any> queryOrNull(json: String, expression: String, returnType: KClass<T>): T? {
    val jsonNode = DatabindCodec.mapper().readTree(json).at(expression)
    return when (jsonNode) {
      null -> null
      else -> {
        val str = DatabindCodec.mapper().writeValueAsString(jsonNode)
        when (returnType) {
          JsonObject::class -> JsonObject(str) as T
          JsonArray::class -> JsonArray(str) as T
          String::class -> {
            if (str.startsWith("\"") && str.endsWith("\"")) {
              Json.decodeValue<String>(str, String::class.java) as T
            } else {
              str as T
            }
          }
          else -> Json.decodeValue(str, returnType.java)
        }
      }
    }
  }

  fun <T : Any> query(json: JsonObject, expression: String, returnType: KClass<T>): T {
    return query(json.toString(), expression, returnType)
  }

  fun <T : Any> query(json: JsonArray, expression: String, returnType: KClass<T>): T {
    return query(json.toString(), expression, returnType)
  }

  fun <T : Any> queryOrNull(json: JsonObject, expression: String, returnType: KClass<T>): T {
    return query(json.toString(), expression, returnType)
  }

  fun <T : Any> queryOrNull(json: JsonArray, expression: String, returnType: KClass<T>): T? {
    return queryOrNull(json.toString(), expression, returnType)
  }
}