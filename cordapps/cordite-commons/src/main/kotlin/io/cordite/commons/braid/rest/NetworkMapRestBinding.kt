/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.braid.rest

import io.cordite.braid.corda.BraidConfig
import io.cordite.braid.corda.services.SimpleNetworkMapServiceImpl
import io.cordite.commons.braid.BraidCordaService
import io.cordite.commons.braid.amendRestConfig
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import javax.ws.rs.QueryParam

class NetworkMapRestBinding(appServiceHub: AppServiceHub) : BraidCordaService {
  private val networkMapService = SimpleNetworkMapServiceImpl(appServiceHub, BraidConfig())

  override fun configureWith(config: BraidConfig) = config.amendRestConfig {
    withPaths {
      group("network") {
        protected {
          get("/network/nodes", ::nodes)
          get("/network/notaries", ::notaries)
          get("/network/my-node-info", networkMapService::myNodeInfo)
        }
      }
    }
  }

  @ApiOperation(value = "Retrieves all nodes if neither query parameter is supplied. Otherwise returns a list of one node matching the supplied query parameter.")
  fun nodes(
    @ApiParam(value = "[host]:[port] for the Corda P2P of the node", example = "localhost:10000")
    @QueryParam(value = "host-and-port")
    hostAndPort: String? = null,
    @ApiParam(value = "the X500 name for the node", example = "O=PartyB, L=New York, C=US")
    @QueryParam(value = "x500-name")
    x500Name: String? = null
  ): List<SimpleNetworkMapServiceImpl.SimpleNodeInfo> {
    return when {
      hostAndPort?.isNotEmpty() ?: false -> listOfNotNull(networkMapService.getNodeByAddress(hostAndPort!!))
      x500Name?.isNotEmpty()
        ?: false -> listOfNotNull(networkMapService.getNodeByLegalName(CordaX500Name.parse(x500Name!!)))
      else -> networkMapService.allNodes()
    }
  }

  @ApiOperation(value = "Retrieves all notaries in the network.")
  fun notaries(@ApiParam(value = "the X500 name for the node", example = "O=PartyB, L=New York, C=US") @QueryParam(value = "x500-name") x500Name: String? = null): List<Party> {
    return when {
      x500Name?.isNotEmpty() ?: false -> listOfNotNull(networkMapService.getNotary(CordaX500Name.parse(x500Name!!)))
      else -> networkMapService.notaryIdentities()
    }
  }
}