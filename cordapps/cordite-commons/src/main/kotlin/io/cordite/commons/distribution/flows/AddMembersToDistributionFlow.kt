/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.distribution.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.distribution.dataDistribution
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import net.corda.core.utilities.unwrap

/**
 * This flow is executed by a member of one or more distribution group to ask the maintainer to add a set of respective
 * parties. It is intended to be used as an inline flow. The sender must be a member of all the distribution groups in
 * [additions]
 */
@StartableByService
@StartableByRPC
@InitiatingFlow
open class AddMembersToDistributionFlow(
  private val maintainer: Party,
  private val additions: Set<Pair<UniqueIdentifier, Party>>
) : FlowLogic<Boolean>() {
  @Suspendable
  override fun call() : Boolean{
    val session = this.initiateFlow(maintainer)
    val payload = AddMemberRequestPayload(serviceHub.myInfo.legalIdentities.first(), additions)
    session.send(payload)
    return session.receive<Boolean>().unwrap { it }
  }
}

/**
 * Handler for the flow above. This flow
 */
@Suppress("unused") // initiated flow, therefore no direct code references
@InitiatedBy(AddMembersToDistributionFlow::class)
open class AddMemberToDistributionHandlerFlow(private val otherSession: FlowSession) : FlowLogic<Unit>() {
  @Suspendable
  override fun call() {
    val distributionService = serviceHub.dataDistribution()
    val payload = otherSession.receive<AddMemberRequestPayload>().unwrap { payload ->
      requireThat {
        "requester matches the session party" using (otherSession.counterparty == payload.requester)
        "requester is member of all distribution lists being referred to" using (
            payload.additions.all { (groupId, _) -> distributionService.hasMember(groupId, payload.requester) }
          )
      }
      payload
    }
    val additions = distributionService.addMembers(payload.additions)
    otherSession.send(additions.isNotEmpty())
  }
}

@CordaSerializable
data class AddMemberRequestPayload(val requester: Party, val additions: Set<Pair<UniqueIdentifier, Party>>)