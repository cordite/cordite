/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.distribution.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.distribution.dataDistribution
import io.cordite.commons.utils.getLinearState
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.StatesToRecord
import net.corda.core.transactions.SignedTransaction
import net.corda.core.utilities.contextLogger

/**
 * This flow is invoked to update reference data for a set of recipients
 */
@StartableByRPC
@StartableByService
@InitiatingFlow
open class UpdateDistributionGroupFlow(
  private val groupId: UniqueIdentifier
) : FlowLogic<Unit>() {
  companion object {
    private val log = contextLogger()
  }
  @Suspendable
  override fun call() {
    val state = serviceHub.vaultService.getLinearState<LinearState>(groupId)
      ?: error("could not find LinearState distribution group data $groupId")

    val tx = serviceHub.validatedTransactions.getTransaction(state.ref.txhash) ?: error("Can't find tx with specified hash.")

    serviceHub
      .dataDistribution().getMembers(groupId)
      .apply { log.info("updating $size parties with the latest state $groupId") }
      .forEach { send(it, tx) }
      .also { log.info("done update for $groupId") }
  }

  @Suspendable
  private fun send(party: Party, tx: SignedTransaction) {
    val session = initiateFlow(party)
    subFlow(SendTransactionFlow(session, tx))
  }
}

/**
 * Received state updates from a distribution list maintainer
 * This class is an "inline" flow and should be invoked from a top level [InitiatedBy] flow
 */
@InitiatedBy(UpdateDistributionGroupFlow::class)
open class ReceiveUpdateDistributionGroupFlow(private val otherSession: FlowSession) : FlowLogic<Unit>() {
  companion object {
    private val log = contextLogger()
  }
  @Suspendable
  override fun call() {
    log.info("receiving update to distribution group")
    subFlow(ReceiveTransactionFlow(otherSession, statesToRecord = StatesToRecord.ALL_VISIBLE))
    log.info("finished receiving update to distribution group")
  }
}