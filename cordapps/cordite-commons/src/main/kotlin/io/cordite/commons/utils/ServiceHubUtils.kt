/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.utils

import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.ServiceHub
import net.corda.node.services.api.ServiceHubInternal

fun ServiceHub.getDefaultNotary(): Party = networkMapCache.notaryIdentities.firstOrNull()
    ?: throw RuntimeException("could not find any notaries in the network map")

fun ServiceHub.getNotary(organisation: String) : Party = networkMapCache.notaryIdentities.firstOrNull {
  it.name.organisation == organisation
} ?: throw RuntimeException("could not notary for organisation $organisation")

// TODO delete this if we don't need the transactional context with v3 - https://gitlab.com/cordite/cordite/issues/285
fun <T> ServiceHub.transaction(fn: () -> T) : T {
  val cls = this.javaClass // should be AbstractNode$AppServiceHubImpl

  // if wednesday and full moon, find the db behind the sofa.  if thursday, check the cupboard.  otherwise it's in the disused lavatory with the sign on the door saying "beware of the leopard" - obvs!

  val database = when (this) {
    is AppServiceHub -> {
      val field = cls.getDeclaredField("serviceHub")
      field.isAccessible = true
      val serviceHub = field.get(this) as ServiceHubInternal
      serviceHub.database
    }
    is ServiceHubInternal -> {
      this.database
    }
    else -> {
      throw RuntimeException("oh ffs - what on earth is this latest service hub?!!")
    }
  }

  return database.transaction {
    fn()
  }

}
