/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.cordapp.braid

import io.cordite.braid.corda.BraidConfig
import io.cordite.braid.core.http.HttpServerConfig
import io.cordite.braid.core.rest.AuthSchema
import io.cordite.commons.braid.BraidCordaService
import io.cordite.commons.braid.amendRestConfig
import io.cordite.commons.braid.auth.BasicAuthProvider
import io.cordite.commons.braid.bootstrapAndLog
import io.cordite.commons.utils.Resources
import io.cordite.commons.utils.jackson.CorditeJacksonInit
import io.cordite.cordapp.certs.CertsToJksOptionsConverter
import io.cordite.cordapp.utils.StringExtensions.toEnvVarName
import io.vertx.core.Vertx
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.core.net.JksOptions
import io.vertx.ext.auth.AuthProvider
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.loggerFor
import java.io.File

const val BRAID_CONFIG_FILENAME = "braid-config.json"
const val BRAID_DISABLED_PORT = -1
private const val BRAID_PORT_FIELD = "port"

@CordaService
class BraidServerCordaService(private val serviceHub: AppServiceHub) : SingletonSerializeAsToken() {
  companion object {

    const val CORDITE_CERT_PATH_PROPERTY = "cordite.tls.cert.path"
    const val CORDITE_KEY_PATH_PROPERTY = "cordite.tls.key.path"
    const val CORDITE_TLS_ENABLED = "cordite.tls.enabled"
    const val CORDITE_WEBSOCKET_FRAME_SIZE = "cordite.websocket.frame.size"
    const val CORDITE_WEBSOCKET_MESSAGE_SIZE = "cordite.websocket.message.size"
    const val CORDITE_MESSAGE_LIMIT = 50 * 1024 * 1024 // 10 MB
    const val CORDITE_AUTH_USERNAME = "cordite.auth.username"
    const val CORDITE_AUTH_PASSWORD = "cordite.auth.password"
    private val log = loggerFor<BraidServerCordaService>()

    init {
      CorditeJacksonInit.init()
    }

  }

  private val org = serviceHub.myInfo.legalIdentities.first().name.organisation.replace(" ", "")

  init {
    val baseConfig = baseConfig()
    when (baseConfig.port) {
      BRAID_DISABLED_PORT -> {
        log.info("no braid configuration nor environment variable for node $org")
        log.info("not starting braid for node $org")
      }
      else -> startUpBraid(baseConfig)
    }
  }

  private fun startUpBraid(config: BraidConfig) {
    log.info("starting $org braid with config $config")
    log.info("starting $org braid on port ${config.port}")
    val finalConfig = BraidCordaService.startAllBraidServicesAndUpdateConfig(config, serviceHub)
    finalConfig.bootstrapAndLog(serviceHub)
  }

  private fun getBraidPortFromEnvironment(): Int? {
    return (getConfigProperty("braid.$org.port") ?: getConfigProperty("braid.port"))?.toInt()
  }

  private fun getConfigProperty(propertyName: String, default: String? = null): String? {
    return getSystemProperty(propertyName) ?: getEnvironmentProperty(propertyName)
    ?: getCordappConfigProperty(propertyName) ?: default.also {
      log.info("could not find property $propertyName. defaulting to $default")
    }
  }

  private fun getSystemProperty(propertyName: String): String? {
    return System.getProperty(propertyName).also {
      when (it) {
        null -> log.info("found system property: $propertyName with value $it")
        else -> log.info("could not find system property: $propertyName")
      }
    }
  }

  private fun getEnvironmentProperty(propertyName: String): String? {
    val envVariableName = propertyName.toEnvVarName()
    return System.getenv(envVariableName).also {
      when (it) {
        null -> log.info("did not find env variable $envVariableName")
        else -> log.info("found env variable: $envVariableName with value $it")
      }
    }
  }

  private fun getCordappConfigProperty(propertyName: String): String? {
    return try {
      serviceHub.getAppContext().config.getString(propertyName).also {
        log.info("found cordapp property: $propertyName with value $it")
      }
    } catch (err: Throwable) {
      log.info("could not find cordapp property: $propertyName")
      null
    }
  }

  private fun createHttpServerOptions(): HttpServerOptions {
    val tlsEnabled = getConfigProperty(CORDITE_TLS_ENABLED, "true")?.toBoolean() ?: true
    return when {
      tlsEnabled -> createTlsHttpServerOptiions()
      else -> tlsDisabledHttpServerOptions()
    }.applyWebsocketFramesize()
  }

  private fun createAuthConstructor(): ((Vertx) -> AuthProvider)? {
    val username = getConfigProperty(CORDITE_AUTH_USERNAME)
    val password = getConfigProperty(CORDITE_AUTH_PASSWORD)
    return when {
      username == null || password == null -> null
      else -> {
        { BasicAuthProvider(username, password) }
      }
    }
  }

  private fun tlsDisabledHttpServerOptions(): HttpServerOptions {
    return HttpServerOptions()
  }

  private fun createTlsHttpServerOptiions(): HttpServerOptions {
    val cp = getConfigProperty(CORDITE_CERT_PATH_PROPERTY)
    val kp = getConfigProperty(CORDITE_KEY_PATH_PROPERTY)
    val config = if (cp == null || kp == null) {
      log.info("without either cert or key paths not present, defaulting to developer HttpServerOptions")
      HttpServerConfig.defaultServerOptions()
    } else if (!File(cp).exists()) {
      log.error("certificate path does not exist $cp")
      HttpServerConfig.defaultServerOptions()
    } else if (!File(kp).exists()) {
      log.error("key path does not exists $kp")
      HttpServerConfig.defaultServerOptions()
    } else HttpServerOptions().apply {
      isSsl = true
      keyCertOptions = convertPemFilesToJksOptions(cp, kp)
    }
    return config
  }

  private fun HttpServerOptions.applyWebsocketFramesize() :HttpServerOptions {
    val maxFramesize = getConfigProperty(CORDITE_WEBSOCKET_FRAME_SIZE)?.toInt() ?: when {
      serviceHub.networkParameters.maxTransactionSize > CORDITE_MESSAGE_LIMIT -> {
        log.warn(
          "network max message size exceeds default Cordite websocket frame size." +
            "you can override this using the system property $CORDITE_WEBSOCKET_FRAME_SIZE or " +
            "the environment variable ${CORDITE_WEBSOCKET_FRAME_SIZE.toEnvVarName()}"
        )
        CORDITE_MESSAGE_LIMIT
      }
      else -> serviceHub.networkParameters.maxTransactionSize
    }
    val maxMessageSize = getConfigProperty(CORDITE_WEBSOCKET_MESSAGE_SIZE)?.toInt() ?: when {
      serviceHub.networkParameters.maxTransactionSize > CORDITE_MESSAGE_LIMIT -> {
        log.warn(
          "network max message size exceeds default Cordite websocket frame size." +
            "you can override this using the system property $CORDITE_WEBSOCKET_FRAME_SIZE or " +
            "the environment variable ${CORDITE_WEBSOCKET_FRAME_SIZE.toEnvVarName()}"
        )
        CORDITE_MESSAGE_LIMIT
      }
      else -> serviceHub.networkParameters.maxTransactionSize
    }
    return this.setMaxWebSocketFrameSize(maxFramesize).setMaxWebSocketMessageSize(maxMessageSize)
  }


  private fun convertPemFilesToJksOptions(certificatePath: String, privateKeyPath: String): JksOptions {
    return CertsToJksOptionsConverter(certificatePath, privateKeyPath).createJksOptions()
  }

  private fun baseConfig(): BraidConfig {
    val jsonConfig = loadJsonConfig()
    val vertx = Vertx.vertx()
    return Json.decodeValue(jsonConfig.toString(), BraidConfig::class.java)
      .withVertx(vertx)
      .withHttpServerOptions(createHttpServerOptions())
      .let {
        val authConstructor = createAuthConstructor()
        when {
          authConstructor != null -> it.withAuthConstructor(authConstructor).amendRestConfig {
            this
              .withAuth(authConstructor(vertx))
              .withAuthSchema(AuthSchema.Token)
          }
          else -> it
        }
      }
  }

  private fun loadJsonConfig(): JsonObject {
    val json = try {
      JsonObject(Resources.loadResourceAsString(BRAID_CONFIG_FILENAME)).let {
        log.info("found $BRAID_CONFIG_FILENAME", it)
        it
      }
    } catch (_: Throwable) {
      log.warn("could not find braid config $BRAID_CONFIG_FILENAME")
      JsonObject()
    }
    return updatePort(json)
  }

  private fun updatePort(jsonConfig: JsonObject): JsonObject {
    val json = if (jsonConfig.containsKey(org)) {
      log.info("found config for org $org")
      jsonConfig.getJsonObject(org)
    } else {
      log.warn("cannot find braid config for $org")
      JsonObject().put("port", BRAID_DISABLED_PORT)
    }

    val overridePort = getBraidPortFromEnvironment()
    when {
      overridePort != null -> json.put(BRAID_PORT_FIELD, overridePort)
      json.containsKey(BRAID_PORT_FIELD) -> try {
        json.getInteger(BRAID_PORT_FIELD)
      } catch (_: Throwable) {
        log.error("'$BRAID_PORT_FIELD' is not an int. default to $BRAID_DISABLED_PORT")
        json.put(
          BRAID_PORT_FIELD,
          BRAID_DISABLED_PORT
        )
      }
      else -> {
        log.error("no port provided for $org in config nor environment variable. defaulting to $BRAID_DISABLED_PORT ")
        json.put(
          BRAID_PORT_FIELD,
          BRAID_DISABLED_PORT
        )
      }
    }
    return json
  }
}



