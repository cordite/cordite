/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.cordapp

import io.cordite.braid.core.async.getOrThrow
import io.cordite.braid.core.http.*
import io.cordite.braid.core.rest.LoginRequest
import io.cordite.commons.braid.JsonQuery.query
import io.cordite.cordapp.braid.BraidServerCordaService.Companion.CORDITE_AUTH_PASSWORD
import io.cordite.cordapp.braid.BraidServerCordaService.Companion.CORDITE_AUTH_USERNAME
import io.cordite.dgl.api.CreateAccountRequest
import io.cordite.dgl.api.LedgerApi
import io.cordite.dgl.api.impl.CreateAccountsPayload
import io.cordite.dgl.contract.v1.account.AccountState
import io.cordite.test.utils.*
import io.cordite.test.utils.WaitForHttpEndPoint.Companion.waitForHttpEndPoint
import io.vertx.core.Vertx
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpHeaders.AUTHORIZATION
import io.vertx.core.http.HttpHeaders.CONTENT_TYPE
import io.vertx.core.json.JsonObject
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import io.vertx.kotlin.core.json.jsonArrayOf
import io.vertx.kotlin.core.json.jsonObjectOf
import net.corda.core.utilities.loggerFor
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkParameters
import net.corda.testing.node.StartedMockNode
import net.corda.testing.node.internal.findCordapp
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.lang.System.setProperty
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import kotlin.test.assertEquals

@RunWith(VertxUnitRunner::class)
class BraidServerCordaServiceTest {

  companion object {

    private val log = loggerFor<BraidServerCordaServiceTest>()
    private const val USERNAME = "sa"
    private const val PASSWORD = "admin"
    private const val accountId = "a-1"
  }

  private val braidPortHelper = BraidPortHelper()
  private lateinit var network: MockNetwork
  private lateinit var node1: StartedMockNode

  private val node1Port = braidPortHelper.portForName(proposerName.organisation)
  private val vertx = Vertx.vertx()
  private val loginRequest = LoginRequest(USERNAME, PASSWORD)

  private val httpClient = vertx.createHttpClient(
    HttpClientOptions()
      .setSsl(true)
      .setTrustAll(true)
      .setVerifyHost(false)
      .setDefaultHost("localhost")
      .setDefaultPort(node1Port)
  )

  @Before
  fun before(context: TestContext) {
    log.info("Initialising network")
    braidPortHelper.setSystemPropertiesFor(proposerName, newMemberName, anotherMemberName)

    setProperty(CORDITE_AUTH_USERNAME, USERNAME)
    setProperty(CORDITE_AUTH_PASSWORD, PASSWORD)

    val cordapps = setOf(
      findCordapp("io.cordite.dgl.contract"),
      findCordapp("io.cordite.dgl.api"),
      findCordapp("io.cordite.dao"),
      findCordapp("io.cordite.cordapp")
    )

    network = MockNetwork(MockNetworkParameters(cordappsForAllNodes = cordapps))

    node1 = network.createPartyNode(proposerName)
//    node2 = network.createPartyNode(newMemberName)
//    node3 = network.createPartyNode(anotherMemberName)
//    listOf(node1, node2, node3).forEach {
//      it.registerInitiatedFlow(CreateProposalFlowResponder::class.java)
//      it.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
//    }
    network.runNetwork()

    waitForHttpEndPoint(
      vertx = vertx,
      host = "localhost",
      port = node1Port,
      handler = context.asyncAssertSuccess<Unit>()
    )
  }

  @After
  fun after() {
    log.info("Shutting down network")
    network.stopNodes()
    vertx.close()
    log.info("Network shutdown")
  }

  @Test
  fun test() {
    log.info("starting test")
    val node1Port = braidPortHelper.portForNode(node1)
//    val node2Port = braidPortHelper.portForNode(node2)
    verifySwaggerForAuthentication()
    verifyWeCanCallRestMethodWithAuth()
    BraidClientHelper
      .braidClient(node1Port, "ledger", vertx, loginRequest)
      .use { ledgerClient ->
        val ledger = ledgerClient.bind(LedgerApi::class.java)
        val accounts = ledger.listAccounts().getOrThrow()
        assertEquals(1, accounts.size)
        assertEquals(accountId, accounts.first().address.accountId)
      }
  }

  private fun verifyWeCanCallRestMethodWithAuth() {
    val token = httpClient
      .futurePost(path = "/rest/auth/login", body = loginRequest)
      .compose {
        it.verifyNoError()
        it.body()
      }
      .map {
        it.toString()
      }.getOrThrow()
    val createAccountsPayload =
      CreateAccountsPayload(listOf(CreateAccountRequest(accountId)), network.defaultNotaryIdentity.name.toString())
    val headers = mapOf(
      AUTHORIZATION.toString() to "Bearer $token",
      CONTENT_TYPE.toString() to APPLICATION_JSON
    )
    val accountResponse = httpClient
      .futurePost(path = "/rest/ledger/accounts", body = createAccountsPayload, headers = headers)
      .compose {
        it.verifyNoError()
        it.bodyAsJsonList<AccountState>()
      }.getOrThrow()
    assertEquals(1, accountResponse.size)
    assertEquals(accountId, accountResponse.first().address.accountId)
  }

  private fun verifySwaggerForAuthentication() {
    val swagger = httpClient.futureGet("/swagger/swagger.json")
      .compose {
        it.verifyNoError()
        it.body()
      }
      .map { JsonObject(it) }
      .getOrThrow()
    val authorization = swagger.query<JsonObject>("/securityDefinitions/Authorization")
    val expectedAuthorization = jsonObjectOf(
      "type" to "apiKey",
      "name" to "authorization",
      "in" to "header"
    )
    assertEquals(expectedAuthorization, authorization)
    swagger.query<JsonObject>("/paths/~1auth~1login")
    val pathSecurity = swagger.query<JsonObject>("/paths/~1ledger~1accounts/get/security/0")
    val expectedSecurity = jsonObjectOf(
      "Authorization" to jsonArrayOf()
    )
    assertEquals(expectedSecurity, pathSecurity)
  }
}
