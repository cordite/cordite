/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.logging

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.LogEvent
import org.apache.logging.log4j.core.config.plugins.Plugin
import org.apache.logging.log4j.core.config.plugins.PluginAttribute
import org.apache.logging.log4j.core.config.plugins.PluginFactory
import org.apache.logging.log4j.core.layout.AbstractStringLayout
import java.io.PrintWriter
import java.io.StringWriter
import java.nio.charset.Charset
import java.time.*
import java.time.format.DateTimeFormatter

@Plugin(name = "CorditeJsonLayout", category = "Core", elementType = "layout", printObject = true)
class CorditeJsonLayout(ints: String, charset: Charset): AbstractStringLayout(charset) {

  private val intKeys: Set<String> = ints.split(",").map{ it.trim() }.toSet()

  companion object {
    @JvmStatic
    @PluginFactory
    fun createLayout(@PluginAttribute(value = "ints", defaultString = "") ints: String, @PluginAttribute(value = "charset", defaultString = "UTF-8") charset: Charset): CorditeJsonLayout {
      return CorditeJsonLayout(ints, charset)
    }
  }

  override fun toSerializable(logEvent: LogEvent?): String {
    return if (logEvent != null) {
      CorditeJsonMessage(logEvent, intKeys).toString()
    } else {
      ""
    }
  }

  class CorditeJsonMessage(private val logEvent: LogEvent, private val intKeys: Set<String>) {

    companion object {
      val om = ObjectMapper()
      val kvPattern = "\\[([^=^,]+)=([^=^,]+)],?".toRegex()
    }

    private val jsonMap: Map<String, Any>
    private val dispatchTable = mapOf(
                              "time" to this::time,
                              "severity" to this::severity,
                              "thread" to this::thread,
                              "logger" to this::logger,
                              "exception" to this::stack)

    init {
      jsonMap = createMapFrom(listOf("message", "time", "thread", "severity", "logger", "exception")).plus(mdc()).plus(message()).filterValues { it != "" }
    }

    private fun dispatch(token: String) : String = dispatchTable[token]?.let { it() } ?: ""
    private fun createMapFrom(list: List<String>): Map<String, String> = list.map { it to dispatch(it) }.toMap()

    private fun thread() = logEvent.threadName
    private fun logger() = logEvent.loggerName
    private fun stack() = logEvent.thrown?.getStackTraceAsString() ?: ""
    private fun mdc(): Map<String, Any> = logEvent.contextData.toMap().replaceIntKeys()

    private fun Map<String, String>.replaceIntKeys(): Map<String, Any>  {
      return this.map { (k, v) ->
        when (intKeys.contains(k)) {
          true -> k to v.toInt()
          false -> k to v
        }
      }.toMap()
    }

    private fun message(): Map<String, Any> {
      val message = logEvent.message?.formattedMessage ?: ""
      return kvPattern
          .findAll(message)
          .filter { !it.groups.isEmpty() && it.groups.size == 3}
          .map { it.groups[1]!!.value.trim() to it.groups[2]!!.value.trim() }
          .toMap()
          .plus("message" to kvPattern.replace(message, "").trim())
          .replaceIntKeys()
    }

    override fun toString(): String {
      return om.writeValueAsString(jsonMap)+"\n"
    }

    private fun severity(): String {
      return when (logEvent.level) {
        Level.WARN -> "WARNING"
        Level.TRACE -> "DEBUG"
        Level.FATAL -> "ALERT"
        Level.ALL -> "INFO"
        Level.OFF -> "INFO"
        else -> "${logEvent.level}"
      }
    }

    //%Y-%m-%dT%H:%M:%S.%NZ from https://raw.githubusercontent.com/Stackdriver/kubernetes-configs/stable/agents.yaml
    private fun time(): String {
      val time = Instant.ofEpochMilli(logEvent.timeMillis).atZone(ZoneOffset.UTC)
      return time.format(DateTimeFormatter.ISO_INSTANT)
    }
  }
}

fun Throwable.getStackTraceAsString() = StringWriter().also { printStackTrace(PrintWriter(it)) }.toString()



