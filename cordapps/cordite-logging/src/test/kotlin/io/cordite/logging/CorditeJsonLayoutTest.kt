/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.logging

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.apache.logging.log4j.Level
import org.apache.logging.log4j.core.impl.Log4jLogEvent
import org.apache.logging.log4j.message.SimpleMessage
import org.apache.logging.log4j.util.SortedArrayStringMap
import org.junit.Assert
import org.junit.Test
import java.nio.charset.Charset

// https://gitbox.apache.org/repos/asf?p=logging-log4j2.git;a=blob;f=log4j-core/src/test/java/org/apache/logging/log4j/core/layout/PatternLayoutTest.java;h=d6f3a806be2c1d068adc413497a2795ee09b265b;hb=HEAD
/**
 * testing the output as a string here, as that's what google sees, rather than round tripping in an OO way...
 */
class CorditeJsonLayoutTest {

  private val om = ObjectMapper()
  private val layout = CorditeJsonLayout.createLayout("intKey1, intKey2", Charset.defaultCharset())

  @Test
  fun `message should be in message field`() {
    val myEvent = Log4jLogEvent.newBuilder().setMessage(SimpleMessage("message")).build()
    convertAndAssert(myEvent, "message", "message")
  }

  @Test
  fun `should correctly convert the time of the event`() {
    println(System.currentTimeMillis())
    val milliTime = 1548681986845
    val myEvent = Log4jLogEvent.newBuilder().setTimeMillis(milliTime).build()
    convertAndAssert(myEvent, "time", "2019-01-28T13:26:26.845Z")
  }

  @Test
  fun `should correctly convert log levels`() {
    assertSeverityShouldBe(Level.OFF, "INFO")
    assertSeverityShouldBe(Level.FATAL, "ALERT")
    assertSeverityShouldBe(Level.ERROR, "ERROR")
    assertSeverityShouldBe(Level.WARN, "WARNING")
    assertSeverityShouldBe(Level.INFO, "INFO")
    assertSeverityShouldBe(Level.DEBUG, "DEBUG")
    assertSeverityShouldBe(Level.TRACE, "DEBUG")
    assertSeverityShouldBe(Level.ALL, "INFO")
  }

  @Test
  fun `should add MDC into json map`() {
    val map = SortedArrayStringMap(mapOf("key1" to "value1", "key2" to "value2"))
    val myEvent = Log4jLogEvent.newBuilder().setContextData(map).build()
    val result = convertMessageToObject(myEvent)
    Assert.assertEquals(result["key1"], "value1")
    Assert.assertEquals(result["key2"], "value2")
  }

  @Test
  fun `MDC can contain ints`() {
    val map = SortedArrayStringMap(mapOf("intKey1" to "1", "key2" to "value2"))
    val myEvent = Log4jLogEvent.newBuilder().setContextData(map).build()
    val result = convertMessageToObject(myEvent)
    Assert.assertEquals(result["intKey1"], 1)
    Assert.assertEquals(result["key2"], "value2")
  }

  @Test
  fun `should convert logger name`() {
    val myEvent = Log4jLogEvent.newBuilder().setLoggerName("fred the shred").build()
    convertAndAssert(myEvent, "logger", "fred the shred")
  }

  @Test
  fun `should convert thread name`() {
    val myEvent = Log4jLogEvent.newBuilder().setThreadName("fred the thread").build()
    convertAndAssert(myEvent, "thread", "fred the thread")
  }

  @Test
  fun `should convert stack trace if there is one`() {
    val re = RuntimeException("there has been a significant issue")
    val myEvent = Log4jLogEvent.newBuilder().setThrown(re).build()
    val result = convertMessageToObject(myEvent)
    Assert.assertTrue((result["exception"] as String).startsWith("java.lang.RuntimeException: there has been a significant issue"))
  }

  @Test
  fun `should convert stack trace with cause if there is one`() {
    val re = RuntimeException("there has been a significant issue", RuntimeException("cause of said issue"))
    val myEvent = Log4jLogEvent.newBuilder().setThrown(re).build()
    val result = convertMessageToObject(myEvent)
    Assert.assertTrue((result["exception"] as String).contains("Caused by: java.lang.RuntimeException: cause of said issue"))
  }

  @Test
  fun `structured pairs should be allowed to contain spaces`() {
    val myEvent = Log4jLogEvent.newBuilder().setMessage(SimpleMessage("[key1=value 1] message")).build()
    val result = convertMessageToObject(myEvent)
    Assert.assertEquals(result["key1"], "value 1")
    Assert.assertEquals(result["message"], "message")
  }

  @Test
  fun `should be able to add int key-value pairs to log message`() {
    val myEvent = Log4jLogEvent.newBuilder().setMessage(SimpleMessage("[intKey1=1] [intKey2=2] message")).build()
    val result = convertMessageToObject(myEvent)
    Assert.assertEquals(result["intKey1"], 1)
    Assert.assertEquals(result["intKey2"], 2)
  }

  @Test
  fun `multiple structured pairs should be supported`() {
    val myEvent = Log4jLogEvent.newBuilder().setMessage(SimpleMessage("[key1=value 1] [key2=value 2] message")).build()
    val result = convertMessageToObject(myEvent)
    Assert.assertEquals(result["key1"], "value 1")
    Assert.assertEquals(result["key2"], "value 2")
    Assert.assertEquals(result["message"], "message")
  }

  @Test
  fun `message can go anywhere`() {
    val myEvent = Log4jLogEvent.newBuilder().setMessage(SimpleMessage("[key1=value 1] bit1 [key2=value 2] bit2")).build()
    val result = convertMessageToObject(myEvent)
    Assert.assertEquals(result["key1"], "value 1")
    Assert.assertEquals(result["key2"], "value 2")
    Assert.assertEquals(result["message"], "bit1  bit2")
  }

  private fun assertSeverityShouldBe(level: Level, expectedLevel: String) {
    val myEvent = Log4jLogEvent.newBuilder().setLevel(level).build()
    val result = convertMessageToObject(myEvent)

    Assert.assertEquals(expectedLevel, result["severity"])
  }

  private fun convertAndAssert(myEvent: Log4jLogEvent, key: String, expectedValue: String) {
    val result = convertMessageToObject(myEvent)
    Assert.assertEquals(expectedValue, result[key])
  }

  private fun convertMessageToObject(myEvent: Log4jLogEvent): Map<String, Any> {
    val message = String(layout.toByteArray(myEvent))
    return om.readValue(message)
  }

}