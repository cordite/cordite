/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.test.utils

import io.cordite.braid.client.BraidClient
import io.cordite.braid.client.BraidClientConfig
import io.vertx.core.Vertx
import java.net.URI

object BraidClientHelper {

  fun braidClient(
    port: Int,
    serviceName: String,
    host: String,
    vertx: Vertx,
    authCredentials: Any? = null
  ): BraidClient {
    val serviceURI = URI("https://$host:$port/api/$serviceName/braid")
    return BraidClient(
      config = BraidClientConfig(
        serviceURI = serviceURI,
        trustAll = true,
        verifyHost = false,
        authCredentials = authCredentials
      ),
      vertx = vertx
    )
  }

  fun braidClient(port: Int, serviceName: String, vertx: Vertx, authCredentials: Any? = null): BraidClient {
    return braidClient(port, serviceName, "localhost", vertx, authCredentials)
  }
}