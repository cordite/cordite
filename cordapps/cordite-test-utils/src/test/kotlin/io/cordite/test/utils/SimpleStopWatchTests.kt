/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.test.utils

import org.junit.Test

class SimpleStopWatchTests {
  @Test
  fun `we can record timings without creating and instance`() {

    val elapsed1 = SimpleStopWatch.elapse { testFunc() }
    assert(elapsed1 > -1)
    val elapsed2 = SimpleStopWatch.elapse({ testFunc() }, "log this")
    assert(elapsed2 > -1)
  }

  @Test
  fun `we can run a stopwatch life cycle`() {
    val stopWatch = SimpleStopWatch()
    stopWatch.elapseAndRecord({ testFunc() }, "first")
    stopWatch.elapseAndRecord({ testFunc() }, "second")
    stopWatch.elapseAndRecord({ testFunc() }, "third")
    stopWatch.logOutTimings()
    stopWatch.clearTimings()
  }

  private fun testFunc() {
    var a = 0;
    for (i in 1..1000) {
      a += i/2
    }
    println("test func calc $a")
  }
}