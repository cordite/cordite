/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import co.paralleluniverse.fibers.Suspendable
import com.fasterxml.jackson.annotation.JsonTypeInfo
import io.cordite.dao.proposal.ProposalState
import net.corda.core.contracts.Requirements.using
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

@JsonTypeInfo(use= JsonTypeInfo.Id.CLASS, include= JsonTypeInfo.As.PROPERTY, property="@class")
@CordaSerializable
interface ModelData {
  fun verify(oldDaoState: DaoState, newDaoState: DaoState, command: DaoContract.Commands)

  @Suspendable
  fun handleAcceptanceOnCreation(inputDao: DaoState, flowLogic: FlowLogic<*>)

  @Suspendable
  fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>)

  @Suspendable
  fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>)

  @Suspendable
  fun handleEvent(event: ModelDataEvent, daoState: DaoState, flowLogic: FlowLogic<*>, notary: Party)

  @Suspendable
  fun verifyMyChange(originalModelData: ModelData?, consumedState: ProposalState<*>)
}

@CordaSerializable
open class BaseModelData: ModelData {
  override fun verify(oldDaoState: DaoState, newDaoState: DaoState, command: DaoContract.Commands) {
    // no verification
  }

  override fun handleAcceptanceOnCreation(inputDao: DaoState, flowLogic: FlowLogic<*>) {
    // nothing to do
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>) {
    // nothing to do
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>) {
    // nothing to do
  }

  @Suspendable
  override fun handleEvent(event: ModelDataEvent, daoState: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    // nothing to do
  }

  @Suspendable
  override fun verifyMyChange(originalModelData: ModelData?, consumedState: ProposalState<*>) {
    "consumed state must be ModelDataProposal" using (consumedState.proposal is ModelDataProposal)
    val modelDataProposal = consumedState.proposal as ModelDataProposal
    "i must be in the model data proposal" using (modelDataProposal.newModelData == this)
  }

}

@CordaSerializable
data class SampleModelData(val someString: String) : BaseModelData()

@CordaSerializable
interface ModelDataEvent