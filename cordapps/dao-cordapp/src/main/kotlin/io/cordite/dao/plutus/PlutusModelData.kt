/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.plutus

import co.paralleluniverse.fibers.Suspendable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import io.cordite.dao.core.*
import io.cordite.dao.data.DataHelper
import io.cordite.dao.membership.NewMemberModelDataEvent
import io.cordite.dao.proposal.ProposalState
import io.cordite.dgl.api.CreateTokenTypeRequest
import io.cordite.dgl.api.flows.account.CreateAccountFlow
import io.cordite.dgl.api.flows.account.SetAccountTagFlow
import io.cordite.dgl.api.flows.account.accountExists
import io.cordite.dgl.api.flows.token.flows.CreateTokenTypeFlow
import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.tag.Tag
import net.corda.core.contracts.Requirements.using
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import java.time.Duration
import java.time.Instant

val DAO_TAG = Tag("dao", "")

@CordaSerializable
@JsonIgnoreProperties(ignoreUnknown = true)
data class TokenTypeTemplate(val symbol: String, val exponent: Int, val issuerName: CordaX500Name)

@CordaSerializable
data class PlutusModelData(val plutusDaoOwner: CordaX500Name, val accountName: String, val issuableTokens: List<TokenTypeTemplate>, val currentMintingRate: Double, val totalIssued: Double, val minNextProposal: Instant = Instant.now(), val proposalPeriod: Duration = Duration.ofDays(28), val initialIssuance: Double = 1000000.0) : BaseModelData() {

  private val maxMintingRate = 0.14
  private val minMintingRate = 0.0

  init {
    checkValid()
  }

  @Suspendable
  private fun checkValid(){
    "minting rate must be <= max" using (currentMintingRate <= maxMintingRate)
    "minting rate must be >= min" using (currentMintingRate >= minMintingRate)
  }

  @Suspendable
  fun getBoundedRate(newRate: Double): Double {
    return when  {
      newRate > maxMintingRate -> maxMintingRate
      newRate < minMintingRate -> minMintingRate
      else -> newRate
    }
  }

  @Suspendable
  override fun handleAcceptanceOnCreation(inputDao: DaoState, flowLogic: FlowLogic<*>) {
    handleAcceptance(inputDao, flowLogic, true)
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>) {
    handleAcceptance(inputDao, flowLogic, false)
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>) {
    handleAcceptance(inputDao, flowLogic, false)
  }

  @Suspendable
  private fun handleAcceptance(inputDao: DaoState, flowLogic: FlowLogic<*>, creation: Boolean) {
    flowLogic.logger.info("handling acceptance of plutus model data")
    // log.info("handling acceptance of plutus model data") this causes all sorts of attachment issues
    val notary = DataHelper.daoStateAndRefFor(inputDao.daoKey, flowLogic.serviceHub).state.notary
    val tokenTypeTemplates = identifyNewDescriptors(inputDao, creation)

    // create dao account
    createDepositAccount(flowLogic, inputDao, notary)

    // create token type if issuer
    tokenTypeTemplates.filter { it.issuerName == flowLogic.ourIdentity.name }.forEach {
      flowLogic.logger.info("creating token type ${it.symbol} as we (${it.issuerName}) are the issuer")
      flowLogic.subFlow(CreateTokenTypeFlow(CreateTokenTypeRequest(symbol = it.symbol, exponent = it.exponent, description = "", notary = notary.name)))
    }
  }

  @Suspendable
  override fun verifyMyChange(originalModelData: ModelData?, consumedState: ProposalState<*>) {
    when (consumedState.proposal) {
      is PlutusProposal -> {
        val plutusModelData = originalModelData as PlutusModelData
        val (newMintingRate, _, newTotalIssued) = consumedState.proposal.extractNewValues(plutusModelData, consumedState.voteResult)
        "new minting rate should be correct" using (this.currentMintingRate == newMintingRate)
        "total issued should be correct" using (this.totalIssued == newTotalIssued)
        "minProposalDate should be correct" using (this.minNextProposal == minNextProposal || this.minNextProposal == plutusModelData.nextMinProposal())
        // really want to check that the issuer is a signer, but this is effectively done by confirming that all members are signers in the main contract check
        "cannot change daoAdmin" using (this.plutusDaoOwner == plutusDaoOwner)
        "cannot change accountName" using (this.accountName == accountName)
        "cannot change issuable tokens" using (this.issuableTokens == issuableTokens)
        "cannot change period" using (this.proposalPeriod == proposalPeriod)
        checkValid()
      }
      is ModelDataProposal -> {
        super.verifyMyChange(originalModelData, consumedState)
        checkValid()
      }
      else -> throw RuntimeException("cannot update plutus model data while consuming proposal: ${consumedState.proposal.key()}")
    }
  }

  @Suspendable
  private fun createDepositAccount(flowLogic: FlowLogic<*>, inputDao: DaoState, notary: Party) {
    val depositAccountName = inputDao.get(PlutusModelData::class)?.accountName ?: "depositAccount"
    val accountAddress = AccountAddress(depositAccountName, flowLogic.ourIdentity.name)
    if (!flowLogic.serviceHub.accountExists(accountAddress)) {
      flowLogic.logger.info("$depositAccountName account does not exist - creating and tagging")
      flowLogic.subFlow(CreateAccountFlow(listOf(CreateAccountFlow.Request(inputDao.name)), notary))
      flowLogic.logger.info("$depositAccountName account created, now tagging with $DAO_TAG!")
      flowLogic.subFlow(SetAccountTagFlow(inputDao.name, listOf(DAO_TAG), notary))
    }
  }

  @Suspendable
  private fun identifyNewDescriptors(previousDaoVersion: DaoState, creation: Boolean): List<TokenTypeTemplate> {
    return if (previousDaoVersion.containsModelData(PlutusModelData::class) && !creation) {
      val previousEmd = previousDaoVersion.get(PlutusModelData::class) as PlutusModelData
      issuableTokens.minus(previousEmd.issuableTokens)
    } else {
      issuableTokens
    }
  }

  @Suspendable
  override fun handleEvent(event: ModelDataEvent, daoState: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    when (event) {
      is NewMemberModelDataEvent -> addAccountIfWeAreNewMember(event, daoState, flowLogic, notary)
    }
  }

  @Suspendable
  private fun addAccountIfWeAreNewMember(event: NewMemberModelDataEvent, daoState: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    if (event.newMember == flowLogic.ourIdentity) {
      createDepositAccount(flowLogic, daoState, notary)
    }
  }

  @Suspendable
  private fun nextMinProposal(): Instant {
    return this.minNextProposal.plusSeconds(proposalPeriod.seconds)
  }

  fun copyWith(newMintingRate: Double, newTotalIssued: Double): PlutusModelData {
    return PlutusModelData(this.plutusDaoOwner, this.accountName, this.issuableTokens, newMintingRate, newTotalIssued, nextMinProposal(), proposalPeriod, initialIssuance)
  }
}

