/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.plutus

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.proposal.Proposal
import io.cordite.dao.proposal.ProposalState
import io.cordite.dao.voting.*
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

val UP = VoteType.of("UP")
val DOWN = VoteType.of("DOWN")

val UP_RESULT = VoteResult.of("UP")
val DOWN_RESULT = VoteResult.of("DOWN")

@CordaSerializable
class PlutusProposalVotingStrategy(private val quorumThreshold: Double = 0.25, private val successThreshold: Double = 0.5): VotingStrategy {

  override val voteTypes = setOf(UP, DOWN, ABSTAIN)
  override val voteResults = setOf(UP_RESULT, DOWN_RESULT, NOT_QUORUM)

  @Suspendable
  override fun voteStatus(proposalState: ProposalState<*>, daoState: DaoState): VoteResult {
    val memberCount = daoState.members.size
    return when {
      proposalState.voters.size.toDouble() / memberCount <= quorumThreshold -> NOT_QUORUM
      proposalState.votes.count{ it.voteType == UP}.toDouble() / proposalState.voters.size >= successThreshold -> UP_RESULT
      else -> DOWN_RESULT
    }
  }
}

@CordaSerializable
class PlutusMemberProposalVotingStrategy: VotingStrategy {
  // currently covered in the DIT.
  override val voteTypes = setOf(FOR, AGAINST, ABSTAIN)
  override val voteResults = setOf(PASSED, FAILED, NOT_QUORUM)

  @Suspendable
  override fun voteStatus(proposalState: ProposalState<*>, daoState: DaoState): VoteResult {
    return when (proposalState.proposal) {
      is MemberProposal -> if (checkMemberVotes(proposalState.proposal, proposalState, daoState)) PASSED else FAILED
      else -> FAILED
    }
  }

  @Suspendable
  private fun checkMemberVotes(proposal: MemberProposal, proposalState: ProposalState<*>, daoState: DaoState): Boolean {
    return when (proposal.type) {
      MemberProposal.Type.NewMember -> checkAddMemberVotes(proposal.member, proposalState.votes)
      MemberProposal.Type.RemoveMember -> checkRemoveMemberVotes(daoState, proposalState.votes)
    }
  }

  @Suspendable
  private fun checkRemoveMemberVotes(daoState: DaoState, votes: Set<Vote>): Boolean {
    if (!daoState.containsModelData(PlutusModelData::class)) {
      throw RuntimeException("You cannot get here without a PlutusModelData")
    }
    val overrideMemberName = daoState.get(PlutusModelData::class)!!.plutusDaoOwner
    return votes.filter { it.voteType == FOR }.map { it.voter.name }.contains( overrideMemberName )
  }

  @Suspendable
  private fun checkAddMemberVotes(member: Party, votes: Set<Vote>): Boolean {
    val forVotes = votes.filter { it.voteType == FOR }
    return forVotes.isNotEmpty() && forVotes.any { it.voter == member }
  }

}

@CordaSerializable
class PlutusVotingStrategySelector: VotingStrategySelector {

  private val plutusProposalVotingStrategy = PlutusProposalVotingStrategy()
  private val memberProposalVotingStrategy = PlutusMemberProposalVotingStrategy()
  private val defaultProposalVotingStrategy = DEFAULT_VOTING_STRATEGY

  @Suspendable
  override fun votingStrategyFor(proposal: Proposal): VotingStrategy {
    return when (proposal) {
      is PlutusProposal -> plutusProposalVotingStrategy
      is MemberProposal -> memberProposalVotingStrategy
      else -> defaultProposalVotingStrategy
    }
  }
}