/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.utils.contextLogger
import io.cordite.dao.core.DaoState
import io.cordite.dao.data.DataHelper
import io.cordite.dao.finality.ReceiveUtterFinalityFlow
import io.cordite.dao.finality.UtterFinalityFlow
import io.cordite.dao.voting.VoteResult
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction

@InitiatingFlow
@StartableByRPC
@StartableByService
class AcceptProposalFlow(private val proposalKey: ProposalKey) : FlowLogic<Pair<ProposalLifecycle,VoteResult>>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call(): Pair<ProposalLifecycle, VoteResult> {
    val me = serviceHub.myInfo.legalIdentities.first()

    log.info("$me proposing to accept proposal $proposalKey")

    val inputProposalStateAndRef = findHeadProposalStateFromVault()
    val inputProposalState = inputProposalStateAndRef.state.data
    val referenceDaoStateAndRef = DataHelper.daoStateAndRefFor(inputProposalState.daoKey, serviceHub)
    val inputMembershipStateAndRef = DataHelper.membershipStateAndRefFor(inputProposalState.daoKey, serviceHub)

    val (newProposalState, txBuilder) = ProposalContract.generateAcceptProposal(inputProposalStateAndRef, inputMembershipStateAndRef, referenceDaoStateAndRef)

    txBuilder.verify(serviceHub)

    val signedTx = serviceHub.signInitialTransaction(txBuilder)

    val flowSessions = newProposalState.participantSet().filter { it != me }.map { initiateFlow(it) }
    log.info("there are ${flowSessions.size} sigs to get...")

    val fullySignedUnNotarisedTx = subFlow(CollectSignaturesFlow(signedTx, flowSessions, CollectSignaturesFlow.tracker()))

    val finalTx = subFlow(UtterFinalityFlow(fullySignedUnNotarisedTx, flowSessions))
    log.info("proposal: ${proposalKey.name} created.  TxHash: $finalTx")

    // now handle the proposal's post acceptance steps
    newProposalState.proposal.handleAcceptanceOnAcceptor(newProposalState, referenceDaoStateAndRef.state.data, this, inputMembershipStateAndRef.state.notary)

    return Pair(newProposalState.lifecycleState, newProposalState.voteResult)
  }

  private fun findHeadProposalStateFromVault(): StateAndRef<ProposalState<Proposal>> {
    val vaultPage = serviceHub.vaultService.queryBy(ProposalState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(proposalKey.uniqueIdentifier)))
    if (vaultPage.states.size != 1) {
      log.warn("incorrect number of ProposalStates with name: ${proposalKey.name} found (${vaultPage.totalStatesAvailable})")
      throw IllegalStateException("there should be exactly one ProposalState called: \"${proposalKey.name}\" but there are: ${vaultPage.states.size}")
    }
    @Suppress("UNCHECKED_CAST")
    return vaultPage.states.first() as StateAndRef<ProposalState<Proposal>>
  }

}

@InitiatedBy(AcceptProposalFlow::class)
class AcceptProposalFlowResponder(val creatorSession: FlowSession) : FlowLogic<Unit>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call() {

    val signTransactionFlow = object : SignTransactionFlow(creatorSession, tracker()) {
      override fun checkTransaction(stx: SignedTransaction) = requireThat {
        // the tx has already been verified and this time we don't have to verify that all dao members are members!
        val tx = stx.toLedgerTransaction(serviceHub, false)
        val groups = tx.groupStates { it: ProposalState<*> -> it.proposal.key() }
        "should be only one group" using (groups.size == 1)
        val (_, outputs, _) = groups.first()
        "there should be one output proposal" using (outputs.size == 1)
        val outputProposal = outputs.first()
        val me = serviceHub.myInfo.legalIdentities.first()

        // give proposal the opportunity to refuse to sign this create txn
        outputProposal.proposal.createResponderCheck(outputProposal, tx, me, this@AcceptProposalFlowResponder)

        log.info("accepting proposal - it's been verified")
      }
    }

    val stx = subFlow(signTransactionFlow)

    subFlow(ReceiveUtterFinalityFlow(creatorSession, expectedTxId = stx.id))

    fireProposalHandlers(stx)
  }

  @Suspendable
  private fun fireProposalHandlers(stx: SignedTransaction) {
    val tx = stx.toLedgerTransaction(serviceHub, false) // is this safe?  should we go and find the properly signed tx?
    val daoStates = tx.referenceInputsOfType(DaoState::class.java)
    val groups = tx.groupStates { it: ProposalState<*> -> it.proposal.key() }

    groups.forEach {
      val proposalState = it.outputs.first()
      val inputDao = daoStates.first { it.daoKey == proposalState.daoKey }
      proposalState.proposal.handleAcceptanceAllButAcceptingMembers(proposalState, inputDao, this, tx.notary!!)
    }
  }
}