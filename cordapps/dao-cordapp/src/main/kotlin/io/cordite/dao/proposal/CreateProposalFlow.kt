/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.utils.contextLogger
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoKey
import io.cordite.dao.data.DataHelper
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.transactions.SignedTransaction

/*
we really only have four ways of handling this:

 1. Put _everything_ on the DaoState
    * horrific due to concurrency issues
 2. Include an unspent DaoState in every tx that creates a proposal or votes
    * this is logically equivalent to 1
    * no concurrency - but then maybe we don't care
    * on the plus side it's very deterministic and simple
 3. Same as above but don't spend it - just refer and check it's valid
    * think this uses oracles
    * not very deterministic - is it idempotent
    * not supported by corda and no plans to support
 4. Proposals just "have to have" Dao members as signers
    * if they don't, we don't care about them
    * if they do, we have some concurrency gains
    * at a cost of having to maintain the membership lists

The simplest and safest is 1 or 2, but there are concurrency issues.  The 4th is the other option.
Really easy, but quite a bit of work to do at membership change time.  It's safe work, though, as you
would do it in the same tx as making the membership change.

Going with 4/. fairly arbitrarily for the mo.
 */


@InitiatingFlow
@StartableByRPC
@StartableByService
class CreateProposalFlow<T : Proposal>(val proposal: T, private val daoKey: DaoKey): FlowLogic<ProposalState<T>>() {

    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call(): ProposalState<T> {
        log.info("creating new proposal: ${proposal.key().name}")
        val me = serviceHub.myInfo.legalIdentities.first()

        val membershipStateAndRef = DataHelper.membershipStateAndRefFor(daoKey, serviceHub)
        val daoStateAndRef = DataHelper.daoStateAndRefFor(daoKey, serviceHub)

        val notary = membershipStateAndRef.state.notary

        val (newProposalState, txBuilder) = ProposalContract.generateCreateProposal(proposal, daoKey, membershipStateAndRef, me, notary)
        DaoContract.generateReferToDao(daoStateAndRef, txBuilder)

        log.info("proposal state created with ${newProposalState.members.size} members")

        txBuilder.verify(serviceHub)

        val signedTx = serviceHub.signInitialTransaction(txBuilder)

        val flowSessions = newProposalState.participantSet().filter{ it != me }.map{ initiateFlow(it) }
        log.info("there are ${flowSessions.size} sigs to get...")

        val fullySignedUnNotarisedTx = subFlow(CollectSignaturesFlow(signedTx, flowSessions, CollectSignaturesFlow.tracker()))

        val finalTx = subFlow(FinalityFlow(fullySignedUnNotarisedTx, flowSessions))
        log.info("proposal: ${proposal.key().name} created.  TxHash: $finalTx")

        return newProposalState
    }

}

@InitiatedBy(CreateProposalFlow::class)
class CreateProposalFlowResponder(val creatorSession: FlowSession) : FlowLogic<Unit>() {

    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call() {

        val signTransactionFlow = object : SignTransactionFlow(creatorSession, tracker()) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                // the tx has already been verified and this time we don't have to verify that all dao members are members!

                val tx = stx.toLedgerTransaction(serviceHub, false)
                val groups = tx.groupStates { it: ProposalState<*> -> it.proposal.key() }
                "should be only one group" using (groups.size == 1)
                val (_, outputs, _) = groups.first()
                "there should be one output proposal" using (outputs.size == 1)
                val outputProposal = outputs.first()
                val me = serviceHub.myInfo.legalIdentities.first()

                // give proposal the opportunity to refuse to sign this create txn
                outputProposal.proposal.createResponderCheck(outputProposal, tx, me, this@CreateProposalFlowResponder)

                log.info("accepting proposal - it's been verified")
            }
        }

        val stx = subFlow(signTransactionFlow)

        subFlow(ReceiveFinalityFlow(creatorSession, expectedTxId = stx.id))
    }
}