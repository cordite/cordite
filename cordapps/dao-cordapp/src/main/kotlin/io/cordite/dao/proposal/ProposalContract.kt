/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membership.MembershipState
import io.cordite.dao.voting.Vote
import io.cordite.dao.voting.VoteType
import net.corda.core.contracts.*
import net.corda.core.identity.Party
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.TransactionBuilder

const val PROPOSAL_CONTRACT_ID = "io.cordite.dao.proposal.ProposalContract"

open class ProposalContract : Contract {

  companion object {
    // TODO: can we refactor this a lot??!! https://gitlab.com/cordite/cordite/issues/280

    fun <T : Proposal> generateCreateProposal(proposal: T, daoKey: DaoKey, referenceMembershipStateAndRef: StateAndRef<MembershipState>, proposer: Party, notary: Party): Pair<ProposalState<T>, TransactionBuilder> {
      val initialVotes = proposal.initialVotes(proposer)
      val membershipState = referenceMembershipStateAndRef.state.data
      val participants = proposal.initialParticipants(membershipState.members, proposer)

      val newProposalState = ProposalState(proposal, proposer, initialVotes, participants, daoKey)
      val newProposalContractAndState = StateAndContract(newProposalState, PROPOSAL_CONTRACT_ID)
      val cmd = Command(Commands.CreateProposal(), participants.map { it.owningKey }.toList())

      val txBuilder = TransactionBuilder(notary = notary)
      MembershipContract.generateReferToMembershipState(referenceMembershipStateAndRef, txBuilder)
      txBuilder.withItems(newProposalContractAndState, cmd)

      return Pair(newProposalState, txBuilder)
    }

    fun generateVoteForProposal(inputProposalStateTxState: StateAndRef<ProposalState<Proposal>>, newVoter: Party, voteType: VoteType, referenceMembershipStateAndRef: StateAndRef<MembershipState>, notary: Party): Pair<ProposalState<Proposal>, TransactionBuilder> {
      val newProposalState = inputProposalStateTxState.state.data.copyWithNewVoter(Vote(newVoter, voteType))
      val newProposalContractAndState = StateAndContract(newProposalState, PROPOSAL_CONTRACT_ID)
      val cmd = Command(Commands.VoteForProposal(newVoter), newProposalState.participants.map { it.owningKey })

      val txBuilder = TransactionBuilder(notary = notary)
      MembershipContract.generateReferToMembershipState(referenceMembershipStateAndRef, txBuilder)
      txBuilder.withItems(inputProposalStateTxState, newProposalContractAndState, cmd)

      return Pair(newProposalState, txBuilder)
    }

    fun generateAcceptProposal(inputProposalStateTxState: StateAndRef<ProposalState<Proposal>>, inputMembershipStateAndRef: StateAndRef<MembershipState>, referenceDaoStateAndRef: StateAndRef<DaoState>): Pair<ProposalState<Proposal>, TransactionBuilder> {
      val inputProposal = inputProposalStateTxState.state.data
      val voteResult = referenceDaoStateAndRef.state.data.votingStrategyFor(inputProposal.proposal).voteStatus(inputProposal, referenceDaoStateAndRef.state.data)
      val newProposalState = inputProposal.copyWithNewLifecycleState(ProposalLifecycle.ACCEPTED, voteResult)
      val newProposalContractAndState = StateAndContract(newProposalState, PROPOSAL_CONTRACT_ID)
      val cmd = Command(Commands.AcceptProposal(), newProposalState.participants.map { it.owningKey })

      val txBuilder = TransactionBuilder(referenceDaoStateAndRef.state.notary)
      MembershipContract.generateReferToMembershipState(inputMembershipStateAndRef, txBuilder) // this is because we need access to the current set of members
      DaoContract.generateReferToDao(referenceDaoStateAndRef, txBuilder) // this is for the membership model data params

      txBuilder.withItems(inputProposalStateTxState, newProposalContractAndState, cmd)
      newProposalState.proposal.extendTxn(txBuilder, referenceDaoStateAndRef.state.data)

      return Pair(newProposalState, txBuilder)
    }

    fun generateMemberConsistencyProposal(inputProposalStateTxStates: Set<StateAndRef<ProposalState<*>>>, membershipState: MembershipState, notary: Party): TransactionBuilder {
      val newProposalStates = inputProposalStateTxStates.map { it.state.data.copyReplacingMembers(membershipState.members) }
      val txBuilder = TransactionBuilder(notary = notary)
      // only dao members need to sign these
      val cmd = Command(Commands.DaoMemberConsistencyUpdate(), membershipState.members.map { it.owningKey })
      txBuilder.withItems(*inputProposalStateTxStates.toTypedArray(), cmd)
      newProposalStates.forEach {
        if (it.members != membershipState.members) {
          throw IllegalStateException("proposal $it has incorrect members...")
        }
        txBuilder.addOutputState(it, PROPOSAL_CONTRACT_ID)
      }

      return txBuilder
    }

  }

  override fun verify(tx: LedgerTransaction) {
    @Suppress("UNCHECKED_CAST") val proposalCommands = tx.commands.filter { it.value is Commands } as List<CommandWithParties<Commands>>

    requireThat {
      "there should be exactly one proposal command" using (proposalCommands.size == 1)
    }

    val command = proposalCommands.first()
    val groups = tx.groupStates { it: ProposalState<*> -> it.proposal.key() }

    for ((inputs, outputs, _) in groups) {

      when (command.value) {
        is Commands.CreateProposal -> {
          requireThat {
            val daoStates = tx.referenceInputsOfType(DaoState::class.java)
            "there must be a reference DaoState" using (daoStates.size == 1)  // NB this is implicitly the proposal has a dao

            val daoState = daoStates.first()

            "no inputs should be consumed when creating a proposal" using (inputs.isEmpty())
            "there should be one output proposal" using (outputs.size == 1)
            val outputProposal = outputs.first()
            outputProposal.proposal.verifyCreate(outputProposal, daoState)

            val membershipStates = tx.referenceInputsOfType(MembershipState::class.java)
            "there must be a corresponding MembershipState" using (membershipStates.size == 1)  // NB this is implicitly the proposal has a dao

            val membershipState = membershipStates.first()

            outputProposal.checkSignersForProposal(command, membershipState)
          }
        }

        is Commands.VoteForProposal -> {
          requireThat {
            val voteForProposal = command.value as Commands.VoteForProposal

            "there should be one input proposal" using (inputs.size == 1)
            "there should be one output proposal" using (outputs.size == 1)

            val inputProposal = inputs.first()
            val outputProposal = outputs.first()

            "proposers should be the same" using (inputProposal.proposer == outputProposal.proposer)
            "no members should be removed" using (outputProposal.members.containsAll(inputProposal.members))
            "voters should be old set plus new voter" using (outputProposal.voters == inputProposal.voters + voteForProposal.newSupporter)
            "proposals should be the same" using (inputProposal.proposal == outputProposal.proposal)
            "one member, one vote" using (outputProposal.votes.groupingBy { it.voter }.eachCount().filter { it.value > 1 }.isEmpty())

            val membershipStates = tx.referenceInputsOfType(MembershipState::class.java)
            "there must be a corresponding MembershipState" using (membershipStates.size == 1)  // NB this is implicitly the proposal has a dao

            val membershipState = membershipStates.first()

            outputProposal.checkSignersForProposal(command, membershipState)
          }
        }

        is Commands.AcceptProposal -> {
          requireThat {
            "there should be one input proposal" using (inputs.size == 1)
            "there should be one output proposal" using (outputs.size == 1)

            val inputProposal = inputs.first()
            val outputProposal = outputs.first()

            "proposers should be the same" using (inputProposal.proposer == outputProposal.proposer)
            "proposals should be the same" using (inputProposal.proposal == outputProposal.proposal)
            "no members should be removed" using (outputProposal.members.containsAll(inputProposal.members))

            val membershipStates = tx.referenceInputsOfType(MembershipState::class.java)
            "there must be a corresponding MembershipState" using (membershipStates.size == 1)  // NB this is implicitly the proposal has a dao

            val membershipState = membershipStates.first()

            outputProposal.checkSignersForProposal(command, membershipState)

            val daoStates = tx.referenceInputsOfType(DaoState::class.java)
            "there must be a reference DaoState" using (daoStates.size == 1)  // NB this is implicitly the proposal has a dao

            val daoState = daoStates.first()

            outputProposal.proposal.verifyAccept(daoState, outputProposal, tx)

            val votingStrategy = daoState.votingStrategyFor(outputProposal.proposal)
            "the proposal must be accepted" using (outputProposal.lifecycleState == ProposalLifecycle.ACCEPTED)
            "the proposal must have correct result" using (votingStrategy.voteStatus(outputProposal, daoState) == outputProposal.voteResult)

            val membershipModelData = daoState.membershipModelData()
            if (membershipModelData.strictMode && inputProposal.proposal !is MemberProposal) {
              "cannot accept non member proposals unless dao has min number of members in strict mode" using (daoState.membershipModelData().hasMinNumberOfMembers)
            }
          }
        }

      // currently only relevant for NewMemberProposals
        is Commands.ConsumeProposal -> {
          requireThat {
            "there should be one input proposal" using (inputs.size == 1)
            "there should be no output proposals" using (outputs.isEmpty())

            val inputProposal = inputs.first()
            "inputProposal must be accepted" using (inputProposal.lifecycleState == ProposalLifecycle.ACCEPTED)

            // for membership proposal acceptance we can have a output rather than a reference state
            val membershipStates = if (tx.inputsOfType(MembershipState::class.java).isNotEmpty()) tx.inputsOfType(MembershipState::class.java) else tx.referenceInputsOfType(MembershipState::class.java)
            "there must be a corresponding MembershipState" using (membershipStates.size == 1)  // NB this is implicitly the proposal has a dao

            val membershipState = membershipStates.first()
            inputProposal.checkSignersForProposal(command, membershipState)
          }
        }

        is Commands.DaoMemberConsistencyUpdate -> {
          requireThat {
            "there should be one input proposal" using (inputs.size == 1)
            "there should be one output proposal" using (outputs.size == 1)

            val inputProposal = inputs.first()
            val outputProposal = outputs.first()

            val originalMembers = inputProposal.members
            val outputMembers = outputProposal.members

            val removedMembers = originalMembers - outputMembers

            "output members and input members must overlap" using (outputMembers.intersect(originalMembers).isNotEmpty())
            "only the dao members must be signers" using (command.signers.containsAll(outputMembers.map { it.owningKey }) && command.signers.size == outputMembers.size)

            "proposers should be the same" using (inputProposal.proposer == outputProposal.proposer)
            "voters should be correct" using (outputProposal.voters == inputProposal.voters - removedMembers)
          }
        }

        else -> throw IllegalArgumentException("unrecognised command: ${command.value}")
      }
    }
  }

  // Used to indicate the transaction's intent.
  interface Commands : CommandData {
    class CreateProposal : TypeOnlyCommandData(), Commands
    class VoteForProposal(val newSupporter: Party) : Commands
    class AcceptProposal : TypeOnlyCommandData(), Commands
    class DaoMemberConsistencyUpdate : TypeOnlyCommandData(), Commands
    class ConsumeProposal : TypeOnlyCommandData(), Commands
  }

}