/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import co.paralleluniverse.fibers.Suspendable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonTypeInfo
import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MembershipState
import io.cordite.dao.voting.FOR
import io.cordite.dao.voting.VOTE_STILL_OPEN
import io.cordite.dao.voting.Vote
import io.cordite.dao.voting.VoteResult
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.CommandWithParties
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.Requirements.using
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.TransactionBuilder
import java.util.*

@CordaSerializable
data class ProposalKey(val name: String, val uuid: UUID = UUID.randomUUID()) {
  val uniqueIdentifier: UniqueIdentifier = UniqueIdentifier(name, uuid)
}

@CordaSerializable
enum class ProposalLifecycle {
  OPEN, ACCEPTED // NB accepted really means that the voting result has been accepted.  your call how to interpret it
}

@JsonIgnoreProperties("linearId", "participants", "name")
@CordaSerializable
@BelongsToContract(ProposalContract::class)
data class ProposalState<T : Proposal>(val proposal: T, val proposer: Party, val votes: Set<Vote>, val members: Set<Party>, val daoKey: DaoKey, val lifecycleState: ProposalLifecycle = ProposalLifecycle.OPEN, val voteResult: VoteResult = VOTE_STILL_OPEN) : LinearState {

  val name = proposal.key().name
  val voters = votes.map { it.voter }.toSet()

  override val participants: List<AbstractParty> = (members + voters).toList()

  override val linearId: UniqueIdentifier
    get() = proposal.key().uniqueIdentifier

  fun copyWithNewVoter(newVote: Vote): ProposalState<T> {
    // remove existing vote for member if there...
    val newVotes = votes.filter { it.voter != newVote.voter }.plus(newVote).toSet()
    return ProposalState(proposal, proposer, newVotes, members, daoKey, lifecycleState)
  }

  fun copyWithoutSupporter(noLongerSupporter: Party): ProposalState<T> {
    return ProposalState(proposal, proposer, votes.filter{ it.voter != noLongerSupporter}.toSet() , members, daoKey, lifecycleState)
  }

  fun copyWithNewMember(newMember: Party): ProposalState<T> {
    return ProposalState(proposal, proposer, votes, members + newMember, daoKey, lifecycleState)
  }

  fun copyReplacingMembers(newMembers: Set<Party>): ProposalState<T> {
    return ProposalState(proposal, proposer, votes.filter{ newMembers.contains(it.voter) }.toSet(), newMembers, daoKey, lifecycleState)
  }

  fun copyWithNewLifecycleState(lifecycleState: ProposalLifecycle, voteResult: VoteResult): ProposalState<T> {
    return ProposalState(proposal, proposer, votes, members, daoKey, lifecycleState, voteResult)
  }

  fun isValid(percentage: Double): Boolean {
    return votes.size.toDouble() / members.size >= percentage
  }

  // TODO: wondering whether these checks really belong here? could split up and have the tests here and the require that bits in the contract? https://gitlab.com/cordite/cordite/issues/279
  fun checkSignersForProposal(command: CommandWithParties<ProposalContract.Commands>, membershipState: MembershipState) {
    "proposer must be signer" using (command.signers.contains(proposer.owningKey))
    proposal.verify(this, membershipState)
    "members must be signers" using (command.signers.containsAll(members.map { it.owningKey }))
  }

  fun participantSet() = participants.toSet()

}

@JsonTypeInfo(use= JsonTypeInfo.Id.CLASS, include= JsonTypeInfo.As.PROPERTY, property="@class")
@CordaSerializable
interface Proposal {
  fun key(): ProposalKey
  fun verifyCreate(state: ProposalState<*>, daoState: DaoState)
  fun verify(state: ProposalState<*>, membershipState: MembershipState) {
    "the proposal state members must be the same as the membership state's members" using (state.members == membershipState.members)
  }
  @Suspendable
  fun initialVotes(proposer: Party): Set<Vote>
  @Suspendable
  fun initialParticipants(currentMembers: Set<Party>, proposer: Party): Set<Party> {
    val initialVotes = initialVotes(proposer)
    return currentMembers + initialVotes.map { it.voter }
  }
  @Suspendable
  fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party)
  @Suspendable
  fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party)
  fun verifyAccept(daoState: DaoState, proposalState: ProposalState<*>, tx: LedgerTransaction)
  fun extendTxn(txBuilder: TransactionBuilder, dao: DaoState)

  @Suspendable
  fun createResponderCheck(proposalState: ProposalState<*>, tx: LedgerTransaction, me: Party, flowLogic: FlowLogic<*>) {
    // most people won't need this
  }

  @Suspendable
  fun acceptResponderCheck(proposalState: ProposalState<*>, tx: LedgerTransaction, me: Party, flowLogic: FlowLogic<*>) {
    // most people won't need this
  }
}

@CordaSerializable
data class NormalProposal(val name: String, val description: String, val proposalKey: ProposalKey = ProposalKey(name)) : Proposal {

  override fun key() = proposalKey

  override fun verifyCreate(state: ProposalState<*>, daoState: DaoState) {
    "proposer must be only initial voter" using (state.voters == setOf(state.proposer))
  }

  override fun verify(state: ProposalState<*>, membershipState: MembershipState) {
    super.verify(state, membershipState)
    "supporters must be members" using (state.members.containsAll(state.voters))
  }

  override fun initialVotes(proposer: Party): Set<Vote> {
    return setOf(Vote(proposer, FOR))
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    // not needed...
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    // not needed...
  }

  override fun verifyAccept(daoState: DaoState, proposalState: ProposalState<*>, tx: LedgerTransaction) {
    // not needed
  }

  override fun extendTxn(txBuilder: TransactionBuilder, dao: DaoState) {
    // not needed
  }


}



