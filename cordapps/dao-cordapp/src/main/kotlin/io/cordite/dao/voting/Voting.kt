/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.voting

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.core.DaoState
import io.cordite.dao.proposal.ProposalState
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

// need to come back with more time and make this much cleaner and more extensible

@CordaSerializable
data class VoteType(val type: String) {
  companion object {
    fun of(type: String): VoteType = VoteType(type)
  }
}

@CordaSerializable
data class VoteResult(val result: String) {
  companion object {
    fun of(result: String): VoteResult = VoteResult(result)
  }
}

@CordaSerializable
data class Vote(val voter: Party, val voteType: VoteType)

val FOR = VoteType.of("FOR")
val AGAINST = VoteType.of("AGAINST")
val ABSTAIN = VoteType.of("ABSTAIN")

val PASSED = VoteResult.of("PASSED")
val FAILED = VoteResult.of("FAILED")
val NOT_QUORUM = VoteResult.of("NOT_QUORUM")
val VOTE_STILL_OPEN = VoteResult.of("VOTE_STILL_OPEN")

interface VotingStrategy {

  val voteTypes: Set<VoteType>
  val voteResults: Set<VoteResult>

  @Suspendable
  fun voteStatus(proposalState: ProposalState<*>, daoState: DaoState): VoteResult

  @Suspendable
  fun isValid(type: VoteType): Boolean = voteTypes.contains(type)

  @Suspendable
  fun isValid(result: VoteResult): Boolean = voteResults.contains(result)
}

@CordaSerializable
class FirstPastPostVotingStrategy(private val quorumThreshold: Double, private val successThreshold: Double = 0.5): VotingStrategy {

  override val voteTypes = setOf(FOR, AGAINST, ABSTAIN)
  override val voteResults = setOf(PASSED, FAILED, NOT_QUORUM)

  @Suspendable
  override fun voteStatus(proposalState: ProposalState<*>, daoState: DaoState): VoteResult {
    val memberCount = proposalState.members.size
    return when {
      proposalState.voters.size.toDouble() / memberCount < quorumThreshold -> NOT_QUORUM
      proposalState.votes.count{ it.voteType == FOR}.toDouble() / proposalState.voters.size >= successThreshold -> PASSED
      else -> FAILED
    }
  }
}

