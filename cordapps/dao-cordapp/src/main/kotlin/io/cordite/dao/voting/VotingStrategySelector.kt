/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.voting

import co.paralleluniverse.fibers.Suspendable
import com.fasterxml.jackson.annotation.JsonTypeInfo
import io.cordite.dao.core.BaseModelData
import io.cordite.dao.proposal.Proposal
import net.corda.core.serialization.CordaSerializable

val DEFAULT_VOTING_STRATEGY = FirstPastPostVotingStrategy(0.0, 0.5)

@JsonTypeInfo(use= JsonTypeInfo.Id.CLASS, include= JsonTypeInfo.As.PROPERTY, property="@class")
@CordaSerializable
interface VotingStrategySelector {
  @Suspendable
  fun votingStrategyFor(proposal: Proposal): VotingStrategy
}

@CordaSerializable
class DefaultVotingStrategySelector: VotingStrategySelector {
  @Suspendable
  override fun votingStrategyFor(proposal: Proposal): VotingStrategy {
    return DEFAULT_VOTING_STRATEGY
  }
}

@CordaSerializable
class VotingModelData(val votingStrategySelector: VotingStrategySelector = DefaultVotingStrategySelector()): BaseModelData(), VotingStrategySelector {

  @Suspendable
  override fun votingStrategyFor(proposal: Proposal): VotingStrategy {
    return votingStrategySelector.votingStrategyFor(proposal)
  }

}