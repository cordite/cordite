/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.membership

import io.cordite.dao.core.DAO_CONTRACT_ID
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MEMBERSHIP_CONTRACT_ID
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membershipState
import io.cordite.dao.proposal.NormalProposal
import io.cordite.dao.proposal.PROPOSAL_CONTRACT_ID
import io.cordite.dao.proposal.ProposalContract
import io.cordite.dao.proposal.ProposalState
import io.cordite.dao.proposalState
import io.cordite.dao.voting.FOR
import io.cordite.dao.voting.Vote
import io.cordite.test.utils.*
import net.corda.core.contracts.CommandData
import net.corda.core.crypto.entropyToKeyPair
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.contracts.DummyState
import net.corda.testing.node.ledger
import org.junit.Before
import org.junit.Test
import java.math.BigInteger
import java.security.KeyPair

class AddMemberProposalTest {

  private val newMember1Key: KeyPair by lazy { entropyToKeyPair(BigInteger.valueOf(315)) }
  private val newMember1Name = CordaX500Name("m1","NewMember", "London", "GB") // has same O as newMember
  private val newMember1Party : Party get() =  Party(newMember1Name, newMember1Key.public)

  private val membershipState = membershipState(members = setOf(proposerParty))
  private val membershipModelData = MembershipModelData(MembershipKey("dao"), 1, hasMinNumberOfMembers = true, oneMemberPerOrg = true, strictMode = false)
  private val daoState = DaoState("dao", setOf(proposerParty, newMemberParty, anotherMemberParty)).copyWith(membershipModelData)

  private val membershipProposal = MemberProposal(newMember1Party, daoState.name)
  private val membershipProposalState = ProposalState(membershipProposal, proposerParty, setOf(Vote(newMember1Party, FOR)), daoState.members, daoState.daoKey)

  @Test
  fun `should not be able to add member with same org as existing member if flag set`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "daoState", daoState)
        command(daoState.members.map{ it.owningKey }, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(proposerKey.public, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        reference("daoState")
        reference("membershipState")
        output(PROPOSAL_CONTRACT_ID, membershipProposalState)
        command(proposerKey.public, ProposalContract.Commands.CreateProposal())
        `fails with`("new member must not belong to existing organisation")
      }
    }
  }
}