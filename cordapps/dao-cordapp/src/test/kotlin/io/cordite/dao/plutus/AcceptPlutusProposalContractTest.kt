/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.plutus

import io.cordite.dao.core.DAO_CONTRACT_ID
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MEMBERSHIP_CONTRACT_ID
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.membershipState
import io.cordite.dao.proposal.PROPOSAL_CONTRACT_ID
import io.cordite.dao.proposal.ProposalContract
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import io.cordite.dao.voting.Vote
import io.cordite.dao.voting.VotingModelData
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.test.utils.ledgerServices
import io.cordite.test.utils.newMemberParty
import io.cordite.test.utils.proposerKey
import io.cordite.test.utils.proposerParty
import net.corda.core.contracts.TimeWindow
import net.corda.testing.node.ledger
import org.junit.Test
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.math.sign

class AcceptPlutusProposalContractTest {

  private val tokenDescriptor = TokenDescriptor("XCD", proposerParty.name)
  private val tokenTypeTemplate = TokenTypeTemplate("XCD", 2, proposerParty.name)

  private val daoKey = DaoKey("theDao")
  private val members = setOf(proposerParty, newMemberParty)
  private val signers = members.map{it.owningKey}

  private val plutusProposalState = ProposalState(PlutusProposal(tokenDescriptor, 20), proposerParty, setOf(Vote(proposerParty, UP)), members, daoKey)
  private val outputProposalState = plutusProposalState.copyWithNewLifecycleState(ProposalLifecycle.ACCEPTED, UP_RESULT)
  private val plutusModelData = PlutusModelData(proposerParty.name,"account", listOf(tokenTypeTemplate), 0.14, 1000.0, Instant.now().plus(1, ChronoUnit.DAYS))
  private val votingModelData = VotingModelData(PlutusVotingStrategySelector())
  private val membershipModelData = MembershipModelData(MembershipKey(daoKey.name), 1, true, strictMode = false)
  private val dao = DaoState("theDao", members, daoKey, mapOf(PlutusModelData::class.qualifiedName!! to plutusModelData, VotingModelData::class.qualifiedName!! to votingModelData, MembershipModelData::class.qualifiedName!! to membershipModelData))

  private val membershipState = membershipState(members)

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", plutusProposalState)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        input("initialProposal")
        reference("membershipState")
        reference("dao")
        timeWindow(TimeWindow.fromOnly(plutusModelData.minNextProposal))
        command(signers, ProposalContract.Commands.AcceptProposal())
        verifies()
      }
    }
  }

  @Test
  fun `must have plutus model data`() {
    val newModelDataMap = dao.modelDataMap.filter { it.value !is PlutusModelData }
    val daoWithNoPmd = DaoState(dao.name, dao.members, daoKey, newModelDataMap)

    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", daoWithNoPmd)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", plutusProposalState)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        input("initialProposal")
        reference("membershipState")
        reference("dao")
        timeWindow(TimeWindow.fromOnly(plutusModelData.minNextProposal))
        command(signers, ProposalContract.Commands.AcceptProposal())
        failsWith("there must be a plutus model data")
      }
    }
  }

  @Test
  fun `transaction must have a time window`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", plutusProposalState)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        input("initialProposal")
        reference("membershipState")
        reference("dao")
        command(signers, ProposalContract.Commands.AcceptProposal())
        failsWith("there must be a time window")
      }
    }
  }

  @Test
  fun `time window start instant must be same as min model data one`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", plutusProposalState)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        input("initialProposal")
        reference("membershipState")
        reference("dao")
        timeWindow(TimeWindow.fromOnly(plutusModelData.minNextProposal.plusSeconds(1)))
        command(signers, ProposalContract.Commands.AcceptProposal())
        failsWith("time window start date should be same as modeldata min time")
      }
    }
  }

  @Test
  fun `time window must be open ended`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", plutusProposalState)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        input("initialProposal")
        reference("membershipState")
        reference("dao")
        timeWindow(TimeWindow.between(plutusModelData.minNextProposal.plusSeconds(1), plutusModelData.minNextProposal.plusSeconds(2)))
        command(signers, ProposalContract.Commands.AcceptProposal())
        failsWith("time window should be open ended")
      }
    }
  }

  @Test
  fun `only dao owner can accept plutus proposal`() {
    val plutusProposalState = ProposalState(PlutusProposal(tokenDescriptor, 20), newMemberParty, setOf(Vote(newMemberParty, UP)), members, daoKey)
    val outputProposalState = plutusProposalState.copyWithNewLifecycleState(ProposalLifecycle.ACCEPTED, UP_RESULT)

    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", plutusProposalState)
        reference("dao")
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        input("initialProposal")
        reference("membershipState")
        reference("dao")
        timeWindow(TimeWindow.fromOnly(plutusModelData.minNextProposal))
        command(signers, ProposalContract.Commands.AcceptProposal())
        failsWith("only admin can accept plutus proposals")
      }
    }
  }
}