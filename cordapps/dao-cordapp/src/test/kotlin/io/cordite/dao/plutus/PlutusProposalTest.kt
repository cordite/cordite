/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.plutus

import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.test.utils.proposerParty
import org.junit.Assert
import org.junit.Test
import java.time.Instant
import java.time.temporal.ChronoUnit

class PlutusProposalTest {
  private val tokenDescriptor = TokenDescriptor("XCD", proposerParty.name)
  private val tokenTypeTemplate = TokenTypeTemplate("XCD", 2, proposerParty.name)
  private val initialPlutusModelData = PlutusModelData(proposerParty.name,"account", listOf(tokenTypeTemplate), 0.14, 0.0, Instant.now().plus(1, ChronoUnit.DAYS))
  private val oldPlutusModelData = PlutusModelData(proposerParty.name,"account", listOf(tokenTypeTemplate), 0.005, 100.0, Instant.now().plus(1, ChronoUnit.DAYS))

  @Test
  fun `initial values should be correct`() {
    val plutusProposal = PlutusProposal(tokenDescriptor, 50)
    val (newMintingRate, amountToIssue, newTotalIssued) = plutusProposal.extractNewValues(initialPlutusModelData, UP_RESULT)
    Assert.assertEquals("new minting rate should be correct", 0.14, newMintingRate, 0.00)
    Assert.assertEquals("amount to issue should be correct", 1000000.0, amountToIssue, 0.00)
    Assert.assertEquals("amount now issued should be correct", 1000000.0, newTotalIssued, 0.00)
  }

  @Test
  fun `up issuance should be correct`() {
    val plutusProposal = PlutusProposal(tokenDescriptor, 50)
    val (newMintingRate, amountToIssue, newTotalIssued) = plutusProposal.extractNewValues(oldPlutusModelData, UP_RESULT)
    Assert.assertEquals("new minting rate should be correct", 0.01, newMintingRate, 0.00)
    Assert.assertEquals("amount to issue should be correct", 1.0, amountToIssue, 0.00)
    Assert.assertEquals("amount now issued should be correct", 101.0, newTotalIssued, 0.00)
  }

  @Test
  fun `up issuance should not be able to go above max`() {
    val oldPlutusModelData = PlutusModelData(proposerParty.name,"account", listOf(tokenTypeTemplate), 0.14, 100.0, Instant.now().plus(1, ChronoUnit.DAYS))
    val plutusProposal = PlutusProposal(tokenDescriptor, 50)
    val (newMintingRate, amountToIssue, newTotalIssued) = plutusProposal.extractNewValues(oldPlutusModelData, UP_RESULT)
    Assert.assertEquals("new minting rate should be correct", 0.14, newMintingRate, 0.00)
    Assert.assertEquals("amount to issue should be correct", 14.0, amountToIssue, 0.00)
    Assert.assertEquals("amount now issued should be correct", 114.0, newTotalIssued, 0.00)
  }

  @Test
  fun `down issuance should not be able to go above max`() {
    val oldPlutusModelData = PlutusModelData(proposerParty.name,"account", listOf(tokenTypeTemplate), 0.0, 100.0, Instant.now().plus(1, ChronoUnit.DAYS))
    val plutusProposal = PlutusProposal(tokenDescriptor, 50)
    val (newMintingRate, amountToIssue, newTotalIssued) = plutusProposal.extractNewValues(oldPlutusModelData, DOWN_RESULT)
    Assert.assertEquals("new minting rate should be correct", 0.0, newMintingRate, 0.00)
    Assert.assertEquals("amount to issue should be correct", 0.0, amountToIssue, 0.00)
    Assert.assertEquals("amount now issued should be correct", 100.0, newTotalIssued, 0.00)
  }

  @Test(expected = IllegalArgumentException::class)
  fun `should not be able to create a plutus proposal with more then 100 basis points`() {
    PlutusProposal(tokenDescriptor, 101)
  }

  @Test(expected = IllegalArgumentException::class)
  fun `should not be able to create a plutus proposal with less than 0 basis points`() {
    PlutusProposal(tokenDescriptor, -2)
  }

}