/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal


import io.cordite.dao.core.DAO_CONTRACT_ID
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MEMBERSHIP_CONTRACT_ID
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.membershipState
import io.cordite.dao.plutus.PlutusModelData
import io.cordite.dao.proposal.ProposalContract
import io.cordite.dao.proposalCopy
import io.cordite.dao.proposalState
import io.cordite.dao.voting.AGAINST
import io.cordite.dao.voting.FOR
import io.cordite.dao.voting.Vote
import io.cordite.dao.voting.VotingModelData
import io.cordite.test.utils.*
import net.corda.testing.node.ledger
import org.junit.Ignore
import org.junit.Test

// NB we cannot really check that all the DaoState members are participants in the current scheme, so members
// must check this in the responder core - discussion in CreateProposalFlow
class VoteForProposalContractTest {

  private val initialProposal = proposalState().copyWithNewMember(voterParty)
  private val outputProposalState = initialProposal.copyWithNewVoter(Vote(voterParty, FOR))

  private val daoKey = DaoKey("theDao")
  private val members = setOf(proposerParty, voterParty)
  private val signers = members.map{it.owningKey}

  private val membershipModelData = MembershipModelData(MembershipKey(daoKey.name), 1, true, strictMode = false)
  private val dao = DaoState("theDao", members, daoKey)

  private val membershipState = membershipState(members)

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        verifies()
      }
    }
  }

  @Test
  fun `there should be one input state`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("There should be one input proposal")
      }
    }
  }

  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("There should be one output proposal")
      }
    }
  }

  @Test
  fun `proposers should be the same`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(initialProposal, proposer = daoParty))
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("proposers should be the same")
      }
    }
  }

  @Test
  fun `no members should be removed`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(initialProposal, members = setOf(proposerParty)))
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("no members should be removed")
      }
    }
  }

  @Test
  fun `supporters should be old set plus new supporter`() {
    ledgerServices.ledger {
      transaction {
        unverifiedTransaction {
          output(DAO_CONTRACT_ID, "dao", dao)
          command(signers, DaoContract.Commands.CreateDao())
        }

        unverifiedTransaction {
          output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
          command(signers, ProposalContract.Commands.CreateProposal())
        }

        unverifiedTransaction {
          output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
          command(signers, MembershipContract.Commands.CreateMembershipState())
        }

        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, initialProposal)
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("voters should be old set plus new voter")
      }
    }
  }

  @Test
  fun `proposer must be signer`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        reference("membershipState")
        reference("dao")
        command(voterKey.public, ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("proposer must be signer")
      }
    }
  }

  @Test
  fun `supporters must be members`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        val inputProp = proposalState()
        input(PROPOSAL_CONTRACT_ID, inputProp)
        output(PROPOSAL_CONTRACT_ID, inputProp.copyWithNewVoter(Vote(voterParty, FOR)))
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("the proposal state members must be the same as the membership state's members")
      }
    }
  }

  @Test
  fun `all members must be signers`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        reference("membershipState")
        reference("dao")
        command(proposerKey.public, ProposalContract.Commands.VoteForProposal(voterParty))
        `fails with`("members must be signers")
      }
    }
  }

  @Test
  fun `one member one vote`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        val outputProposal = ProposalState(initialProposal.proposal, initialProposal.proposer, initialProposal.votes + Vote(proposerParty, AGAINST) , members, daoKey, initialProposal.lifecycleState)
        output(PROPOSAL_CONTRACT_ID, outputProposal)
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(proposerParty))
        `fails with`("one member, one vote")
      }
    }
  }

  @Test
  fun `member can change vote`() {
    ledgerServices.ledger {
      unverifiedTransaction {
        output(DAO_CONTRACT_ID, "dao", dao)
        command(signers, DaoContract.Commands.CreateDao())
      }

      unverifiedTransaction {
        output(PROPOSAL_CONTRACT_ID, "initialProposal", initialProposal)
        command(signers, ProposalContract.Commands.CreateProposal())
      }

      unverifiedTransaction {
        output(MEMBERSHIP_CONTRACT_ID, "membershipState", membershipState)
        command(signers, MembershipContract.Commands.CreateMembershipState())
      }

      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, initialProposal.copyWithNewVoter(Vote(proposerParty, AGAINST)))
        reference("membershipState")
        reference("dao")
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.VoteForProposal(proposerParty))
        verifies()
      }
    }
  }


}