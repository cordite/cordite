/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.commons

import net.corda.core.crypto.Crypto
import net.corda.core.crypto.DigitalSignature
import net.corda.core.crypto.sign
import net.corda.core.node.ServiceHub
import net.corda.core.serialization.CordaSerializable
import java.security.PrivateKey
import java.security.PublicKey

/**
 * General purpose [content] + [signature] class
 */
@CordaSerializable
data class SignedPayload(
  override val content: ByteArray,
  override val signature: DigitalSignature
) : Signed {

  companion object {

    fun sign(content: ByteArray, privateKey: PrivateKey): DigitalSignature {
      return privateKey.sign(content)
    }

    fun sign(
      content: ByteArray,
      publicKey: PublicKey,
      serviceHub: ServiceHub
    ): DigitalSignature {
      return serviceHub.keyManagementService.sign(content, publicKey)
    }
  }

  constructor(content: ByteArray, publicKey: PublicKey, serviceHub: ServiceHub) :
    this(content, sign(content, publicKey, serviceHub))

  override fun isSignedBy(publicKey: PublicKey): Boolean {
    return Crypto.isValid(publicKey, signature.bytes, content)
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is SignedPayload) return false

    if (!content.contentEquals(other.content)) return false
    if (signature != other.signature) return false

    return true
  }

  override fun hashCode(): Int {
    var result = content.contentHashCode()
    result = 31 * result + signature.hashCode()
    return result
  }
}
