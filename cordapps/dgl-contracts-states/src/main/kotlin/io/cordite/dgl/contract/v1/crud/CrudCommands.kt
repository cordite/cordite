/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.crud

import io.cordite.dgl.contract.v1.commons.SignedPayload
import net.corda.core.contracts.CommandData
import net.corda.core.identity.AbstractParty
import net.corda.core.node.ServiceHub
import java.security.PublicKey

interface CrudCommands : CommandData {

  val signedPayload: SignedPayload?

  companion object {

    private fun sign(identity: AbstractParty, serviceHub: ServiceHub): SignedPayload {
      return SignedPayload(identity.owningKey.encoded, identity.owningKey, serviceHub)
    }

    fun create(identity: AbstractParty, serviceHub: ServiceHub) =
      Create(sign(identity, serviceHub))

    fun update(identity: AbstractParty, serviceHub: ServiceHub) =
      Update(sign(identity, serviceHub))

    fun delete(identity: AbstractParty, serviceHub: ServiceHub) =
      Delete(sign(identity, serviceHub))
  }

  fun isSignedBy(publicKey: PublicKey): Boolean {
    return signedPayload?.isSignedBy(publicKey) ?: false
  }

  class Create(override val signedPayload: SignedPayload? = null) : CrudCommands

  class Update(override val signedPayload: SignedPayload? = null) : CrudCommands

  class Delete(override val signedPayload: SignedPayload? = null) : CrudCommands
}
