/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.dstate

import io.cordite.dgl.contract.v1.tag.Tag
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import java.util.*
import javax.persistence.*

@BelongsToContract(DynamicStateContract::class)
data class DynamicState(
  val tags: Set<Tag> = emptySet(),
  val data: Any? = null,
  val creator: Party,
  val updaters: List<Party>,
  val readers: List<Party> = (updaters + creator).distinct(),
  override val linearId: UniqueIdentifier = UniqueIdentifier(id = UUID.randomUUID())
) : LinearState, QueryableState {

  override val participants: List<Party>
    get() = (updaters + creator + readers).distinct()

  val id: UUID = linearId.id

  override fun generateMappedObject(schema: MappedSchema): PersistentState {
    if (schema !is DStateSchemaV1)
      throw RuntimeException("unknown schema ${schema.name}, version ${schema.version}")

    val dState = DStateSchemaV1.DState()
    dState.tags = tags.map { tag ->
      DStateSchemaV1.DStateTags(0, "${tag.category}:${tag.value}", dState)
    }.toMutableSet()

    return dState
  }

  override fun supportedSchemas(): Iterable<MappedSchema> = listOf(DStateSchemaV1)
}

object DStateSchema
object DStateSchemaV1 : MappedSchema(
  DStateSchema::class.java,
  1,
  listOf(DState::class.java, DStateTags::class.java)
) {

  @Entity
  @Table(name = "CORDITE_D_STATE")
  class DState(

    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.PERSIST])
    @JoinColumns(
      JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
      JoinColumn(name = "output_index", referencedColumnName = "output_index")
    )
    @OrderColumn
    var tags: MutableSet<DStateTags> = mutableSetOf()
  ) : PersistentState()

  @Entity
  @Table(
    name = "CORDITE_D_STATE_TAGS", indexes = [Index(
      name = "cordite_d_state_tags_index", columnList = "categoryAndValue", unique = false
    )]
  )
  class DStateTags(
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long = 0,
    @Column(name = "categoryAndValue") val categoryAnValue: String,
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns(
      JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"),
      JoinColumn(name = "output_index", referencedColumnName = "output_index")
    )
    val dState: DState
  )
}