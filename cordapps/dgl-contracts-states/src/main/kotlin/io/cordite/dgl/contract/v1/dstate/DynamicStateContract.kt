/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.dstate

import io.cordite.dgl.contract.v1.crud.CrudCommands
import io.cordite.dgl.contract.v1.crud.CrudContract
import net.corda.core.contracts.Contract
import net.corda.core.contracts.ContractClassName
import net.corda.core.contracts.Requirements.using
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.transactions.LedgerTransaction

class DynamicStateContract : CrudContract<DynamicState>(DynamicState::class), Contract {
  companion object {

    val CONTRACT_ID: ContractClassName = DynamicStateContract::class.java.name
  }

  override fun onVerifyCreate(tx: LedgerTransaction) {
    val createCommand = tx.commands.requireSingleCommand<CrudCommands>()
    tx.commands
    "There should be a single output of type DynamicState"
      .using(tx.outputStates.size == 1 && tx.outputStates.single() is DynamicState)
    val dStateOutput = tx.outputsOfType<DynamicState>().single()

    "Create command origin signature is not signed by the creator"
      .using(createCommand.value.isSignedBy(dStateOutput.creator.owningKey))

    verifyAtLeastOneUpdater(dStateOutput)
    verifyNoDuplicateUpdaters(dStateOutput)
    readersShouldContainAtLeastUpdatersAndCreator(dStateOutput)

    val creatorAndUpdatersOwningKeys =
      (dStateOutput.updaters.map { it.owningKey } + dStateOutput.creator.owningKey).distinct()
    "Create command signers should be the creator and updaters" using
      (createCommand.signers.size == creatorAndUpdatersOwningKeys.size
        && createCommand.signers.containsAll(creatorAndUpdatersOwningKeys))
    
    verifyNoDuplicateTagsCategories(dStateOutput)
    verifyCreatorAndUpdatersAndReadersAreInParticipants(dStateOutput)
  }

  override fun onVerifyUpdate(tx: LedgerTransaction) {
    val updateCommand = tx.commands.requireSingleCommand<CrudCommands>()

    "There should be a single input of type DynamicState"
      .using(tx.inputStates.size == 1 && tx.inputStates.single() is DynamicState)
    "There should be a single output of type DynamicState"
      .using(tx.outputStates.size == 1 && tx.outputStates.single() is DynamicState)
    val dStateInput = tx.inputsOfType<DynamicState>().single()
    val dStateOutput = tx.outputsOfType<DynamicState>().single()

    "Can't change the dynamic state creator" using
      (dStateInput.creator == dStateOutput.creator)

    "Can't change the dynamic state updaters" using
      (dStateInput.updaters.toSet() == dStateOutput.updaters.toSet())

    "Can't change the dynamic state readers" using
      (dStateInput.readers.toSet() == dStateOutput.readers.toSet())

    "Update command origin signature is not signed by any party in updaters list"
      .using(
        dStateInput.updaters
          .map { updateCommand.value.isSignedBy(it.owningKey) }
          .find { it } == true
      )
    
    verifyNoDuplicateTagsCategories(dStateOutput)

    verifyAtLeastOneUpdater(dStateOutput)
    verifyNoDuplicateUpdaters(dStateOutput)
    readersShouldContainAtLeastUpdatersAndCreator(dStateOutput)
    verifyCreatorAndUpdatersAndReadersAreInParticipants(dStateOutput)
  }

  override fun onVerifyDelete(tx: LedgerTransaction) {
    val deleteCommand = tx.commands.requireSingleCommand<CrudCommands>()
    "There should be a single input of type DynamicState"
      .using(tx.inputStates.size == 1 && tx.inputStates.single() is DynamicState)
    val dStateInput = tx.inputsOfType<DynamicState>().single()
    "There should no output state" using (tx.outputStates.isEmpty())

    "Delete command origin signature is not signed by any updater"
      .using(
        dStateInput.updaters
          .map { deleteCommand.value.isSignedBy(it.owningKey) }
          .find { it } == true
      )

    val inputCreatorAndUpdatersOwningKeys = (
      dStateInput.updaters.map { it.owningKey } +
        dStateInput.creator.owningKey
      ).distinct()
    "Delete command signers should be the creator and updaters of it's input" using
      (deleteCommand.signers.size == inputCreatorAndUpdatersOwningKeys.size
        && deleteCommand.signers.containsAll(inputCreatorAndUpdatersOwningKeys))
  }

  private fun verifyAtLeastOneUpdater(dState: DynamicState) {
    "Dynamic state should have at least one updater".using(dState.updaters.isNotEmpty())
  }

  private fun verifyNoDuplicateUpdaters(dState: DynamicState) {
    if (dState.updaters.size <= 1) return
    "Dynamic state has duplicate parties in updaters" using
      (dState.updaters.map { it.owningKey }.toSet().size == dState.updaters.size)
  }

  private fun verifyNoDuplicateTagsCategories(dState: DynamicState) {
    if (dState.tags.size <= 1) return
    "Dynamic state has duplicate category names in tags" using
      (dState.tags.map { it.category }.toSet().size == dState.tags.size)
  }

  private fun verifyCreatorAndUpdatersAndReadersAreInParticipants(dState: DynamicState) {
    val creatorUpdatersReadersParties =
      (dState.updaters + dState.creator + dState.readers).distinct()
    "Dynamic state participants should be updaters, readers and creator"
      .using(
        dState.participants.size == creatorUpdatersReadersParties.size
          && dState.participants.containsAll(creatorUpdatersReadersParties)
      )
  }

  private fun readersShouldContainAtLeastUpdatersAndCreator(dState: DynamicState) {
    "In the readers list there should be at least the creator and the updaters" using
      (dState.readers.containsAll(dState.updaters + dState.creator))
  }

}


