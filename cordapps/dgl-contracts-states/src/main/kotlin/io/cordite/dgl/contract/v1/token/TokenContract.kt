/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.token

import net.corda.core.contracts.*
import net.corda.core.identity.AbstractParty
import net.corda.core.node.ServiceHub
import net.corda.core.transactions.LedgerTransaction
import java.math.BigDecimal

class TokenContract : Contract {
  companion object {
    val CONTRACT_ID: ContractClassName = TokenContract::class.java.name

    /**
     * @param amount is a string representation of a numeric value. This routine will reject numbers that have a greater exponent than allowed by the [tokenTypeRef]
     */
    fun generateIssuance(services: ServiceHub,
                         amount: String,
                         tokenTypeRef: StateAndRef<TokenTypeState>,
                         accountId: String,
                         owner: AbstractParty = tokenTypeRef.state.data.issuer): TokenState {
      check(services.myInfo.legalIdentities.contains(tokenTypeRef.state.data.issuer)) { "token type issuer is not in the list of identities for this node" }
      val bdAmount = amount.toBigDecimalAmount(tokenTypeRef.state.data)
      check(bdAmount.compareTo(BigDecimal.ZERO) != 0) { "amount must not be zero" }

      val tokenType = tokenTypeRef.state.data
      val issuer = tokenType.issuer
      return TokenState(
        accountId = accountId,
        amount = bdAmount,
        tokenTypePointer = LinearPointer(tokenType.linearId, TokenTypeState::class.java),
        issuer = issuer,
        owner = owner
      )
    }
  }

  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<Command>()
    val groups = tx.groupStates(TokenState::class.java) { it.tokenTypePointer }
    for ((inputs, outputs, tokenTypePointer) in groups) {
      val tokenType = tx.findReference<TokenTypeState> { it.linearId == tokenTypePointer.pointer }
      when (command.value) {
        is Command.Issue -> {
          requireThat {
            "there should be no inputs" using (inputs.isEmpty())
            "there are one or more outputs" using (outputs.isNotEmpty())
            outputs.forEach {
              "issuer of the token is the same as the TokenType owner" using
                (it.issuer.nameOrNull() == tokenType.descriptor.issuerName)
              "exponent matches" using (it.amount.quantity.scale() == tokenType.exponent)
            }
          }
        }
        is Command.Move -> {
          requireThat {
            "there are one or more inputs " using (inputs.isNotEmpty())
            "total input amount equals total output amount" using (inputs.map { it.amount.quantity }.sum() == outputs.map { it.amount.quantity }.sum())
            outputs.forEach {
              "issuer of the token is the same as the TokenType owner" using
                (it.issuer.nameOrNull() == tokenType.descriptor.issuerName)
              "exponent matches" using (it.amount.quantity.scale() == tokenType.exponent)
            }
          }
        }
      }
    }
  }

  interface Command : CommandData {
    object Issue : TypeOnlyCommandData(), Command
    data class Move(override val contract: Class<out Contract>? = TokenContract::class.java) : MoveCommand, Command
  }
}

fun Iterable<BigDecimal>.sum(): BigDecimal {
  return this.reduce { acc, item -> acc + item }
}

