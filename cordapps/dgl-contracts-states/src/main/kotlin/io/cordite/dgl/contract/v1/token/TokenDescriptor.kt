/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.token

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import net.corda.core.identity.CordaX500Name
import net.corda.core.serialization.CordaSerializable

@JsonIgnoreProperties(ignoreUnknown = true)
@CordaSerializable
data class TokenDescriptor(
    val symbol: String,
    val issuerName: CordaX500Name
) {
  companion object {
    const val SEPARATOR = ':'
    fun parse(uri: String) : TokenDescriptor {
      val parts = uri.split(SEPARATOR)
      if (parts.size != 2) throw RuntimeException("invalid token type descriptor $uri")
      return TokenDescriptor(parts[0], CordaX500Name.parse(parts[1]))
    }
  }
  init {
    if (symbol.contains(SEPARATOR)) {
      throw RuntimeException("token descriptor cannot contain $SEPARATOR")
    }
  }

  val uri : String get() = "$symbol$SEPARATOR$issuerName"
}