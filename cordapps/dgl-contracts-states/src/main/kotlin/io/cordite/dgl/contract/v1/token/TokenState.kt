/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.token

import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.token.TokenTypeState.Companion.AMOUNT_MAX_EXPONENT
import io.cordite.dgl.contract.v1.token.TokenTypeState.Companion.AMOUNT_PRECISION
import net.corda.core.contracts.*
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.utilities.toBase58String
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Index
import javax.persistence.Table

@BelongsToContract(TokenContract::class)
data class TokenState(
  val accountId: String,
  val amount: BigDecimalAmount<TokenDescriptor>,
  val tokenTypePointer: LinearPointer<TokenTypeState>,
  val issuer: Party,
  override val owner: AbstractParty = issuer
) : OwnableState, QueryableState {

  override val participants : List<AbstractParty> = listOf(owner)
  val contractId: ContractClassName get() = TokenContract::class.java.name
  val accountAddress: AccountAddress = AccountAddress(accountId, owner.nameOrNull()!!)

  override fun withNewOwner(newOwner: AbstractParty)
      = CommandAndState(TokenContract.Command.Move(), copy(owner = newOwner))

  override fun generateMappedObject(schema: MappedSchema): PersistentState {
    return when (schema) {
      is TokenSchemaV1 -> {
        TokenSchemaV1.PersistedToken(
          tokenTypeSymbol = amount.amountType.symbol,
          accountId = accountId,
          amount = amount.quantity,
          issuer = issuer.toString(),
          issuerKey = issuer.owningKey.toBase58String()
        )
      }
      else -> {
        throw IllegalArgumentException("Unrecognised schema $schema")
      }
    }
  }

  override fun supportedSchemas(): Iterable<MappedSchema> = listOf(TokenSchemaV1)

  object TokenSchema
  object TokenSchemaV1 : MappedSchema(TokenSchema::class.java, 1, setOf(TokenSchemaV1.PersistedToken::class.java)
  ) {
    @Entity
    @Table(name = "CORDITE_TOKEN", indexes = [
      Index(name = "cordite_token_account_idx", columnList = "account_id"),
      Index(name = "cordite_token_symbol_idx", columnList = "symbol"),
      Index(name = "cordite_token_issuer_idx", columnList = "issuer"),
      Index(name = "cordite_token_amount_idx", columnList = "amount"),
      Index(name = "cordite_token_issuer_key_idx", columnList = "issuer_key")
    ])
    class PersistedToken(
        @Column(name = "symbol")
        val tokenTypeSymbol: String,
        @Column(name = "amount", precision = AMOUNT_PRECISION, scale= AMOUNT_MAX_EXPONENT)
        val amount: BigDecimal,
        @Column(name = "account_id")
        val accountId: String,
        @Column(name = "issuer")
        val issuer: String,
        @Column(name = "issuer_key")
        val issuerKey: String
    ) : PersistentState()
  }
}