/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.token

import io.cordite.dgl.contract.v1.crud.CrudCommands
import io.cordite.dgl.contract.v1.crud.CrudContract
import net.corda.core.contracts.Contract
import net.corda.core.contracts.ContractClassName
import net.corda.core.contracts.Requirements.using
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.transactions.LedgerTransaction

class TokenTypeContract : CrudContract<TokenTypeState>(TokenTypeState::class), Contract {
  companion object {
    val CONTRACT_ID : ContractClassName = TokenTypeContract::class.java.name
  }

  final override fun onVerifyCreate(tx: LedgerTransaction) {
    tx.commands.requireSingleCommand<CrudCommands>()
    "There should be a single output" using 
        (tx.outputStates.size == 1 && tx.outputStates.single() is TokenTypeState)
    val tokenType = tx.outputsOfType<TokenTypeState>().single()
    "Token issuer should be the only participant" using 
        (tokenType.participants.size == 1 && tokenType.participants.contains(tokenType.issuer))
  }

  final override fun onVerifyUpdate(tx: LedgerTransaction) {
    "There should be a single input" using 
        (tx.inputStates.size == 1 && tx.inputStates.single() is TokenTypeState)
    "There should be a single output" using 
        (tx.outputStates.size == 1 && tx.outputStates.single() is TokenTypeState)
    val tokenType = tx.outputsOfType<TokenTypeState>().single()
    "Token issuer should be the only participant" using 
        (tokenType.participants.size == 1 && tokenType.participants.contains(tokenType.issuer))
  }
}


