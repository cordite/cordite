/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.token

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import io.cordite.dgl.contract.v1.token.TokenTypeSchemaV1.PersistedTokenType
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.contracts.requireThat
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Index
import javax.persistence.Table

/**
 * Definition of a token type. Contains a
 * @param symbol - usually 3 or 4 capitalised letters
 * @param exponent - representing the number of fractional decimal places
 * @param issuer
 *
 */
@BelongsToContract(TokenTypeContract::class)
@JsonIgnoreProperties(ignoreUnknown = true)
data class TokenTypeState(
  val symbol: String,
  val exponent: Int,
  val description: String,
  val issuer: Party,
  override val linearId: UniqueIdentifier = UniqueIdentifier(externalId = symbol),
  val metaData: Any? = null,
  val settlements: List<Any> = emptyList()
) : LinearState, QueryableState {
  companion object {
    const val AMOUNT_MAX_EXPONENT = 20
    const val AMOUNT_PRECISION = 38
  }
  init {
    requireThat {
      "exponent must be within the range (0..$AMOUNT_MAX_EXPONENT)" using ((0..AMOUNT_MAX_EXPONENT).contains(exponent))
    }
  }
  val descriptor: TokenDescriptor by lazy { TokenDescriptor(symbol, issuer.name)}
  override val participants: List<AbstractParty> = listOf(issuer)
  override fun generateMappedObject(schema: MappedSchema): PersistentState {
    return when (schema) {
      is TokenTypeSchemaV1 -> {
        PersistedTokenType(
          symbol = symbol,
          issuer = issuer.name.toString(),
          exponent = exponent,
          description = description
        )
      }
      else -> {
        throw RuntimeException("unknown schema version ${schema.version}")
      }
    }
  }

  override fun supportedSchemas() = listOf(TokenTypeSchemaV1)
}

object TokenTypeSchema
object TokenTypeSchemaV1 : MappedSchema(
  TokenTypeSchema::class.java, 1, listOf(PersistedTokenType::class.java)
) {
  @Entity
  @Table(name = "CORDITE_TOKEN_TYPE", indexes = [
    Index(name = "cordite_token_type_symbol_idx", columnList = "symbol", unique = false),
    Index(name = "cordite_token_type_issuer_idx", columnList = "issuer", unique = false),
    Index(name = "cordite_token_type_exponent_idx", columnList = "exponent", unique = false)
  ])
  class PersistedTokenType(
    @Column(name = "symbol")
    val symbol: String,
    @Column(name = "issuer")
    val issuer: String,
    @Column(name = "exponent")
    val exponent: Int,
    @Column(name = "description")
    val description: String
  ) : PersistentState()
}
