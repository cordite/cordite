/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.dstate

import io.cordite.dgl.contract.v1.crud.CrudCommands
import io.cordite.dgl.contract.v1.tag.Tag
import net.corda.core.identity.CordaX500Name
import net.corda.core.internal.packageName_
import net.corda.testing.core.TestIdentity
import net.corda.testing.node.MockServices
import net.corda.testing.node.ledger
import net.corda.testing.node.makeTestIdentityService
import org.junit.Test

class DynamicStateContractCreateTest {

  private val node1 = CordaX500Name("node-1", "Bristol", "GB")
  private val node2 = CordaX500Name("node-2", "London", "GB")
  private val node3 = CordaX500Name("node-3", "London", "GB")

  private val identity1 = TestIdentity(node1)
  private val identity2 = TestIdentity(node2)
  private val identity3 = TestIdentity(node3)

  private val ledgerServices = MockServices.makeTestDatabaseAndMockServices(
    cordappPackages = listOf(this.javaClass.packageName_),
    identityService =
    makeTestIdentityService(identity1.identity, identity2.identity, identity3.identity),
    initialIdentity = identity1,
    moreKeys = *arrayOf(identity1.keyPair, identity2.keyPair, identity3.keyPair)
  ).second

  @Test
  fun `can create valid dState`() {
    val dStateOutput = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity2.party,
      updaters = listOf(identity1.party),
      readers = listOf(identity1.party, identity2.party, identity3.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(identity2.publicKey, identity1.publicKey),
          CrudCommands.create(identity2.party, ledgerServices)
        )
        verifies()
      }
    }
  }

  @Test
  fun `can't create more than one dynamic state outputs`() {
    val dStateOutput1 = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity2.party,
      updaters = listOf(identity1.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput1)
        output(DynamicStateContract.CONTRACT_ID, dStateOutput1)
        command(
          listOf(identity2.publicKey, identity1.publicKey),
          CrudCommands.create(identity2.party, ledgerServices)
        )
        failsWith("There should be a single output of type DynamicState")
      }
    }
  }

  @Test
  fun `can create dState with empty tags`() {
    val dStateOutput = DynamicState(
      tags = setOf(),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity2.party,
      updaters = listOf(identity1.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(identity2.publicKey, identity1.publicKey),
          CrudCommands.create(identity2.party, ledgerServices)
        )
        verifies()
      }
    }
  }

  @Test
  fun `can't create dState with duplicate tags`() {
    val dStateOutput = DynamicState(
      tags = setOf(Tag("type", "issue"), Tag("type", "issue2")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity2.party,
      updaters = listOf(identity1.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(identity2.publicKey, identity1.publicKey),
          CrudCommands.create(identity2.party, ledgerServices)
        )
        failsWith("Dynamic state has duplicate category names in tags")
      }
    }
  }

  @Test
  fun `can't create dState with empty updaters`() {
    val dStateOutput = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity2.party,
      updaters = listOf()
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(identity2.publicKey, CrudCommands.create(identity2.party, ledgerServices))
        failsWith("Dynamic state should have at least one updater")
      }
    }
  }

  @Test
  fun `can't create dState with duplicate updaters`() {
    val dStateOutput = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity2.party,
      updaters = listOf(identity1.party, identity1.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(identity2.publicKey, identity1.publicKey),
          CrudCommands.create(identity2.party, ledgerServices)
        )
        failsWith("Dynamic state has duplicate parties in updaters")
      }
    }
  }

  @Test
  fun `can't create dState when command origin signature is not signed by the creator`() {
    val dStateOutput = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity2.party,
      updaters = listOf(identity1.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(identity2.publicKey, identity1.publicKey),
          CrudCommands.create(identity1.party, ledgerServices)
        )
        failsWith("Create command origin signature is not signed by the creator")
      }
    }
  }

  @Test
  fun `can't create dState when the creator is not a command signer`() {
    val dStateOutput = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity1.party,
      updaters = listOf(identity2.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(identity1.publicKey),
          CrudCommands.create(identity1.party, ledgerServices)
        )
        failsWith("Create command signers should be the creator and updaters")
      }
    }
  }

  @Test
  fun `can't create dState when a party other than creator or updater exists as command signer`() {
    val dStateOutput = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity1.party,
      updaters = listOf(identity2.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(identity1.publicKey, identity2.publicKey, identity2.publicKey),
          CrudCommands.create(identity1.party, ledgerServices)
        )
        failsWith("Create command signers should be the creator and updaters")
      }
    }
  }

  @Test
  fun `can't create dState when the readers does not contain at least the updaters and the creator`() {
    val dStateOutput = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity1.party,
      updaters = listOf(identity2.party),
      readers = listOf(identity1.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(identity1.publicKey, identity2.publicKey),
          CrudCommands.create(identity1.party, ledgerServices)
        )
        failsWith("In the readers list there should be at least the creator and the updaters")
      }
    }
  }

}