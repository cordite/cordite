/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.dstate

import io.cordite.dgl.contract.v1.crud.CrudCommands
import io.cordite.dgl.contract.v1.tag.Tag
import net.corda.core.identity.CordaX500Name
import net.corda.core.internal.packageName_
import net.corda.testing.core.TestIdentity
import net.corda.testing.node.MockServices
import net.corda.testing.node.ledger
import net.corda.testing.node.makeTestIdentityService
import org.junit.Test

class DynamicStateContractUpdateTest {

  private val node1 = CordaX500Name("node-1", "Bristol", "GB")
  private val node2 = CordaX500Name("node-2", "London", "GB")
  private val node3 = CordaX500Name("node-3", "London", "GB")

  private val creatorIdentity = TestIdentity(node2)
  private val updaterIdentity1 = TestIdentity(node1)
  private val updaterIdentity2 = TestIdentity(node3)

  private val ledgerServices = MockServices.makeTestDatabaseAndMockServices(
    cordappPackages = listOf(this.javaClass.packageName_),
    identityService =
    makeTestIdentityService(
      updaterIdentity1.identity,
      creatorIdentity.identity,
      updaterIdentity2.identity
    ),
    initialIdentity = updaterIdentity1,
    moreKeys = *arrayOf(
      updaterIdentity1.keyPair,
      creatorIdentity.keyPair,
      updaterIdentity2.keyPair
    )
  ).second

  val dStateInput = DynamicState(
    tags = setOf(Tag("type", "issue")),
    data = mapOf("key1" to "value1", "key2" to "value2"),
    creator = creatorIdentity.party,
    updaters = listOf(updaterIdentity1.party)
  )

  @Test
  fun `can update dState body`() {
    val dStateOutput =
      dStateInput.copy(data = mapOf("key1" to "value2", "key2" to "value3"))
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity1.party, ledgerServices)
        )
        verifies()
      }
    }
  }

  @Test
  fun `can update dState tags`() {
    val dStateOutput = dStateInput.copy(tags = setOf(Tag("type", "issue")))
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity1.party, ledgerServices)
        )
        verifies()
      }
    }
  }

  @Test
  fun `can update dState with empty tags`() {
    val dStateOutput = dStateInput.copy(tags = setOf())
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity1.party, ledgerServices)
        )
        verifies()
      }
    }
  }

  @Test
  fun `can't update dState with duplicate tags`() {
    val dStateOutput = dStateInput.copy(
      tags = setOf(Tag("type", "issue"), Tag("type", "issue2"))
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity1.party, ledgerServices)
        )
        failsWith("Dynamic state has duplicate category names in tags")
      }
    }
  }

  @Test
  fun `can't update dState why duplicate updaters`() {
    val dStateOutput = dStateInput.copy(
      updaters = listOf(updaterIdentity1.party, updaterIdentity1.party),
      readers = listOf(creatorIdentity.party, updaterIdentity1.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity1.party, ledgerServices)
        )
        failsWith("Dynamic state has duplicate parties in updaters")
      }
    }
  }

  @Test
  fun `can't update dState when command origin signature is not signed by any party in updaters list`() {
    val dStateOutput = dStateInput.copy(tags = setOf(Tag("type", "issue")))
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity2.party, ledgerServices)
        )
        failsWith("Update command origin signature is not signed by any party in updaters list")
      }
    }
  }

  @Test
  fun `can't change the dState creator`() {
    val dStateOutput = dStateInput.copy(creator = updaterIdentity1.party)
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity1.party, ledgerServices)
        )
        failsWith("Can't change the dynamic state creator")
      }
    }
  }

  @Test
  fun `can't change the dState updaters`() {
    val dStateOutput = dStateInput.copy(
      updaters = listOf(updaterIdentity1.party, updaterIdentity2.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity1.party, ledgerServices)
        )
        failsWith("Can't change the dynamic state updaters")
      }
    }
  }

  @Test
  fun `can't change the dState readers`() {
    val dStateOutput = dStateInput.copy(
      readers = listOf(updaterIdentity1.party)
    )
    ledgerServices.ledger {
      transaction {
        output(DynamicStateContract.CONTRACT_ID, "dStateInput", dStateInput)
        command(
          listOf(updaterIdentity1.publicKey, creatorIdentity.publicKey),
          CrudCommands.create(creatorIdentity.party, ledgerServices)
        )
        verifies()
      }
      transaction {
        input("dStateInput")
        output(DynamicStateContract.CONTRACT_ID, dStateOutput)
        command(
          listOf(creatorIdentity.publicKey),
          CrudCommands.update(updaterIdentity1.party, ledgerServices)
        )
        failsWith("Can't change the dynamic state readers")
      }
    }
  }

}