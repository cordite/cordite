/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.contract.v1.dstate

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import io.cordite.braid.corda.serialisation.CordaX500NameDeserializer
import io.cordite.braid.corda.serialisation.CordaX500NameSerializer
import io.cordite.braid.corda.serialisation.PublicKeyDeserializer
import io.cordite.braid.corda.serialisation.PublicKeySerializer
import io.cordite.dgl.contract.v1.tag.Tag
import net.corda.core.identity.CordaX500Name
import net.corda.testing.core.TestIdentity
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import java.security.PublicKey
import kotlin.test.assertEquals

class JacksonTest {

  private val node1 = CordaX500Name("node-1", "Bristol", "GB")
  private val node2 = CordaX500Name("node-2", "London", "GB")
  private val node3 = CordaX500Name("node-3", "London", "GB")

  private val identity1 = TestIdentity(node1)
  private val identity2 = TestIdentity(node2)
  private val identity3 = TestIdentity(node3)

  companion object {

    private val mapper: ObjectMapper = ObjectMapper()

    @BeforeClass
    @JvmStatic
    fun setup() {
      mapper.registerModule(KotlinModule())
      val cordaModule = SimpleModule()
      cordaModule.addDeserializer(CordaX500Name::class.java, CordaX500NameDeserializer())
      cordaModule.addDeserializer(PublicKey::class.java, PublicKeyDeserializer())

      cordaModule.addSerializer(CordaX500Name::class.java, CordaX500NameSerializer())
      cordaModule.addSerializer(PublicKey::class.java, PublicKeySerializer())

      mapper.registerModule(cordaModule)
    }
  }

  @Test
  @Ignore
  fun `it can be deserialized`() {
    val json = """
      {
        "tags": [
          {
            "category": "status",
            "value": "pending"
          },
          {
            "category": "type",
            "value": "settlement"
          }
        ],
        "value": {
          "accountFrom": "account1",
          "amount": 1000
        },
        "creator": {
          "name": "OU=Sable, O=Sable Group, L=London, C=GB",
          "owningKey": "GfHq2tTVk9z4eXgyEnTkhqPTcXZyt8kXjP2ZZJRoHhdxaRLpPMMeyD3CZvEC"
        },
        "updaters": [
          {
            "name": "OU=Sable, O=Sable Group, L=London, C=GB",
            "owningKey": "GfHq2tTVk9z4eXgyEnTkhqPTcXZyt8kXjP2ZZJRoHhdxaRLpPMMeyD3CZvEC"
          },
          {
            "name": "OU=Nielsen, O=Nielsen Group, L=London, C=GB",
            "owningKey": "GfHq2tTVk9z4eXgyXBy8LacV87jSVEQsUvn6FNr716XncpnBLNe2BLuiPbit"
          }
        ],
        "readers": [
          {
            "name": "OU=Sable, O=Sable Group, L=London, C=GB",
            "owningKey": "GfHq2tTVk9z4eXgyEnTkhqPTcXZyt8kXjP2ZZJRoHhdxaRLpPMMeyD3CZvEC"
          },
          {
            "name": "OU=Nielsen, O=Nielsen Group, L=London, C=GB",
            "owningKey": "GfHq2tTVk9z4eXgyXBy8LacV87jSVEQsUvn6FNr716XncpnBLNe2BLuiPbit"
          }
        ],
        "linearId": {
          "externalId": null,
          "id": "fd7871a3-7fd9-45be-a227-0ebbd67334b8"
        },
        "id": "fd7871a3-7fd9-45be-a227-0ebbd67334b8",
        "participants": [
          {
            "name": "OU=Sable, O=Sable Group, L=London, C=GB",
            "owningKey": "GfHq2tTVk9z4eXgyEnTkhqPTcXZyt8kXjP2ZZJRoHhdxaRLpPMMeyD3CZvEC"
          },
          {
            "name": "OU=Nielsen, O=Nielsen Group, L=London, C=GB",
            "owningKey": "GfHq2tTVk9z4eXgyXBy8LacV87jSVEQsUvn6FNr716XncpnBLNe2BLuiPbit"
          }
        ]
      }
    """

    val dState = mapper.readValue<DynamicState>(json)
    println(dState)
    assertEquals(dState.id.toString(), "fd7871a3-7fd9-45be-a227-0ebbd67334b8")
    assertEquals(dState.participants.size, 2)
  }

  @Test
  @Ignore
  fun `it can be serialized`() {
    val dState = DynamicState(
      tags = setOf(Tag("type", "issue")),
      data = mapOf("key1" to "value1", "key2" to "value2"),
      creator = identity2.party,
      updaters = listOf(identity1.party),
      readers = listOf(identity1.party, identity2.party, identity3.party)
    )
    val json = mapper.writeValueAsString(dState)
    println(json)
    Assert.assertThat(json, CoreMatchers.containsString("\"id\":"))
    Assert.assertThat(json, CoreMatchers.containsString("\"participants\":"))
  }
}