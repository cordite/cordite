<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Module `dgl-cordapp`

## 1. API

Primary external API is [LedgerApi](src/main/kotlin/io/cordite/dgl/api/LedgerAPI.kt). 
This is implemented by [LedgerAPIImp](src/main/kotlin/io/cordite/dgl/api/impl/LedgerAPIImpl.kt),
which in turn calls a set of flow declared in [io.cordite.dgl.flows](src/main/kotlin/io/cordite/dgl/api/flows).

## 2. State Model

The core state model is:

* [AccountState](../dgl-contracts-states/src/main/kotlin/io/cordite/dgl/contract/v1/account/AccountState.kt) - defines an account together with meta data called `tags` that are indexed for fast queries.
* [TokenTypeState](../dgl-contracts-states/src/main/kotlin/io/cordite/dgl/contract/v1/token/TokenTypeState.kt) - definition of a Token type indicating parameters such as `symbol` and `exponent`. TokenStates are shared as reference data with any node that receives `TokenState`s, and dynamically update if the token definition changes.
* [TokenState](../dgl-contracts-states/src/main/kotlin/io/cordite/dgl/contract/v1/token/TokenState.kt) - holds the token data such as [accountId] and [amount]. References a respective `TokenType` as a `LinearPointer`.
* [TokenTransactionSummary](../dgl-contracts-states/src/main/kotlin/io/cordite/dgl/contract/v1/token/TokenTransactionSummary.kt) - additional transaction meta data that sums up the debits and credits to each respective account for a single transaction.
* [TokenDescriptor](../dgl-contracts-states/src/main/kotlin/io/cordite/dgl/contract/v1/token/TokenDescriptor.kt) - a string-serialisable unique identifier for a `TokenType`. This is used by the `LedgerAPI` to reference a `TokenType` when executing a transaction.

### Regarding `BigDecimalAmount`

All of these rely on a replacement of Corda's `Amount` type with 
[BigDecimalAmount](../dgl-contracts-states/src/main/kotlin/io/cordite/dgl/contract/v1/token/BigDecimalAmount.kt).
This type exists because many users of Cordite are interested in representing assets that have a higher precision than 
that offered by a `Long` as is the case with Corda's `Amount` type. A classic example of such an asset are the tokens 
created in the `Ethereum` network. The precision and max exponent of amounts is defined in 
[TokenTypeState](../dgl-contracts-states/src/main/kotlin/io/cordite/dgl/contract/v1/token/TokenTypeState.kt) as 
`AMOUNT_PRECISION` and `AMOUNT_MAX_EXPONENT`.

## 3. Flows
The implementation for `LedgerAPI` calls into a set of public [flows](src/main/kotlin/io/cordite/dgl/api/flows):

* [AccountFlows.kt](src/main/kotlin/io/cordite/dgl/api/flows/account/AccountFlows.kt) - flows for managing accounts
* [CreateTokenTypeFlow](src/main/kotlin/io/cordite/dgl/api/flows/token/flows/CreateTokenTypeFlow.kt) - creates token types. This will be complemented with amendment routines.
* [IssueTokensFlow](src/main/kotlin/io/cordite/dgl/api/flows/token/flows/IssueTokensFlow.kt) - issues tokens to accounts on the initiating node.
* [MultiAccountTokenTransferFlow](src/main/kotlin/io/cordite/dgl/api/flows/token/flows/MultiAccountTokenTransferFlow.kt) - multi-account transfers.

## 4. Token Selection

The implementation for `MultiAccountTokenTransferFlow` uses bespoke implementations of 
[AbstractTokenSelection](src/main/kotlin/io/cordite/dgl/api/flows/token/selection/AbstractTokenSelection.kt) for  `H2`, 
`PostgreSQL`, and `SQL Server` databases. Additional implementations can be added using Java 
[`ServiceLoader`](https://docs.oracle.com/javase/8/docs/api/java/util/ServiceLoader.html) providers defined in the 
respective [`META-INF` file](src/main/resources/META-INF/services/io.cordite.dgl.api.flows.token.selection.AbstractTokenSelection) 
on the classpath.

## 5. `TokenTypeState` and Data Distribution Groups

The `DGL` will synchronise updates to `TokenTypeState` using Cordite's [Data Distribution Groups](../cordite-commons/src/main/kotlin/io/cordite/commons/distribution/README.md).