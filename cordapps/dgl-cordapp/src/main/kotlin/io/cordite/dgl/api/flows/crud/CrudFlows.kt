/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.crud

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.api.flows.token.checkSignAndFinalise
import io.cordite.dgl.api.flows.token.execute
import io.cordite.dgl.contract.v1.crud.CrudCommands
import net.corda.core.contracts.ContractClassName
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.StateAndRef
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.FlowSession
import net.corda.core.identity.Party
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import java.security.PublicKey

class CrudCreateFlow<T : LinearState>(
  private val clazz: Class<T>,
  private val states: List<T>,
  private val contractClassName: ContractClassName,
  private val notary: Party,
  private val signers: List<PublicKey> =
    states.flatMap { it.participants }.map { it.owningKey }.distinct()
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    val ids =
      states.filter { it.linearId.externalId != null }.map { it.linearId.externalId!! }
    val qc = QueryCriteria.LinearStateQueryCriteria(externalId = ids)
    serviceHub.vaultService.queryBy(clazz, qc).apply {
      if (states.isNotEmpty()) {
        val mgs = "cannot create the following $contractClassName because they exist: " +
          states.map { it.state.data.linearId.externalId }.joinToString(", ")
        throw IllegalArgumentException(mgs)
      }
    }
    val txb = TransactionBuilder(notary = notary, serviceHub = serviceHub).apply {
      addCommand(CrudCommands.create(ourIdentity, serviceHub), signers)
      states.forEach { addOutputState(it, contractClassName) }
    }
    return execute(txb)
  }
}

class CrudCreateReceiverFlow<T : LinearState>(
  private val otherPartySession: FlowSession
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    return checkSignAndFinalise(otherPartySession)
  }
}

class CrudUpdateFlow<T : LinearState>(
  private val inputStates: List<StateAndRef<T>>,
  private val states: List<T>,
  private val contractClassName: ContractClassName,
  private val notary: Party,
  private val signers: List<PublicKey> =
    states.flatMap { it.participants }.map { it.owningKey }.distinct()
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    val txb = TransactionBuilder(notary = notary, serviceHub = serviceHub).apply {
      addCommand(CrudCommands.update(ourIdentity, serviceHub), signers)
      inputStates.forEach { addInputState(it) }
      states.forEach { addOutputState(it, contractClassName) }
    }
    return execute(txb)
  }
}

class CrudUpdateReceiverFlow<T : LinearState>(
  private val otherPartySession: FlowSession
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    return checkSignAndFinalise(otherPartySession)
  }
}

class CrudDeleteFlow<T : LinearState>(
  private val inputStates: List<StateAndRef<T>>,
  private val notary: Party,
  private val signers: List<PublicKey> =
    inputStates.flatMap { it.state.data.participants }.map { it.owningKey }.distinct()
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    val txb = TransactionBuilder(notary = notary, serviceHub = serviceHub).apply {
      addCommand(CrudCommands.delete(ourIdentity, serviceHub), signers)
      inputStates.forEach { addInputState(it) }
    }
    return execute(txb)
  }
}

class CrudDeleteReceiverFlow<T : LinearState>(
  private val otherPartySession: FlowSession
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    return checkSignAndFinalise(otherPartySession)
  }
}