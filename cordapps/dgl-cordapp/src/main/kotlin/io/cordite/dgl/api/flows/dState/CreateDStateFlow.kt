/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.dState

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.api.flows.crud.CrudCreateFlow
import io.cordite.dgl.api.flows.crud.CrudCreateReceiverFlow
import io.cordite.dgl.contract.v1.dstate.DynamicState
import io.cordite.dgl.contract.v1.dstate.DynamicStateContract
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction

class CreateDStateFlow(
  private val dState: DynamicState,
  private val notary: Party
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    return subFlow(
      CrudCreateFlow(
        clazz = DynamicState::class.java,
        states = listOf(dState),
        contractClassName = DynamicStateContract.CONTRACT_ID,
        notary = notary,
        signers = (dState.updaters.map { it.owningKey } + dState.creator.owningKey).distinct()
      )
    )
  }

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class Sender(
    private val dState: DynamicState,
    private val notary: Party
  ) : FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call(): SignedTransaction {
      return subFlow(CreateDStateFlow(dState, notary))
    }
  }
}

class CreateDStateFlowReceiver(
  private val otherPartySession: FlowSession
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    return subFlow(CrudCreateReceiverFlow<DynamicState>(otherPartySession))
  }

  @InitiatedBy(CreateDStateFlow.Sender::class)
  class Receiver(private val otherPartySession: FlowSession) :
    FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call(): SignedTransaction {
      return subFlow(CreateDStateFlowReceiver(otherPartySession))
    }
  }
}
