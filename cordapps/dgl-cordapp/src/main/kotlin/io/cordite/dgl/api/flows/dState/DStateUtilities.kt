/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.dState

import io.cordite.commons.utils.transaction
import io.cordite.dgl.contract.v1.dstate.DynamicState
import io.cordite.dgl.contract.v1.dstate.DynamicStateContract
import io.cordite.dgl.contract.v1.tag.Tag
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.StateRef
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.utilities.loggerFor
import java.util.*

private val log = loggerFor<DynamicStateContract>()

fun ServiceHub.getDynamicState(id: UUID): StateAndRef<DynamicState> {
  val linearId = UniqueIdentifier(id = id)
  val linearStateCriteria =
    QueryCriteria.LinearStateQueryCriteria(linearId = listOf(linearId))
  val foundDynamicState =
    vaultService.queryBy(DynamicState::class.java, linearStateCriteria)

  return when (foundDynamicState.states.size) {
    1 -> foundDynamicState.states.first()
    0 -> throw IllegalArgumentException("Dynamic state with id $id not found")
    else -> {
      log.error("Found more than one dynamic states with same id: $id")
      error("Unexpected error")
    }
  }
}

fun ServiceHub.filterDynamicStatesByAllTags(tags: List<Tag>): List<DynamicState> {
  val stringOfCommaSeparatedQuestionMarks = "? ,".repeat(tags.size).dropLast(2)
  val paramSize = tags.size
  val query = """
      SELECT CORDITE_D_STATE.*
      FROM CORDITE_D_STATE 
        INNER JOIN (
            SELECT CORDITE_D_STATE_TAGS.TRANSACTION_ID,
                   CORDITE_D_STATE_TAGS.OUTPUT_INDEX,
                   COUNT(CORDITE_D_STATE_TAGS.CATEGORYANDVALUE)
            FROM CORDITE_D_STATE_TAGS 
              INNER JOIN VAULT_STATES ON (
                (CORDITE_D_STATE_TAGS.TRANSACTION_ID = VAULT_STATES.TRANSACTION_ID) AND
                (CORDITE_D_STATE_TAGS.OUTPUT_INDEX = VAULT_STATES.OUTPUT_INDEX)
              )
            WHERE (
              VAULT_STATES.STATE_STATUS = 0 AND
              CORDITE_D_STATE_TAGS.CATEGORYANDVALUE IN ($stringOfCommaSeparatedQuestionMarks)
            )
            GROUP BY CORDITE_D_STATE_TAGS.TRANSACTION_ID, CORDITE_D_STATE_TAGS.OUTPUT_INDEX
            HAVING COUNT(CORDITE_D_STATE_TAGS.CATEGORYANDVALUE) = $paramSize
        ) as tempTable ON (
        (CORDITE_D_STATE.TRANSACTION_ID = tempTable.TRANSACTION_ID) AND
        (CORDITE_D_STATE.OUTPUT_INDEX = tempTable.OUTPUT_INDEX)
      )
    """.trimIndent()
  return transaction {
    val preparedStatement = jdbcSession().prepareStatement(query)
    for (indexedTag in tags.withIndex()) {
      val index = indexedTag.index + 1 // preparedStatement.setXXX starts from 1 not 0
      val tagSearchTerm = "${indexedTag.value.category}:${indexedTag.value.value}"
      preparedStatement.setString(index, tagSearchTerm)
    }

    val rs = preparedStatement.executeQuery()

    val stateRefs = mutableListOf<StateRef>()
    while (rs.next()) {
      val sh = SecureHash.parse(rs.getString("TRANSACTION_ID"))
      val i = rs.getInt("OUTPUT_INDEX")
      stateRefs.add(StateRef(sh, i))
    }

    vaultService.queryBy(
      contractStateType = DynamicState::class.java,
      criteria = QueryCriteria.VaultQueryCriteria(stateRefs = stateRefs)
    ).states.map { it.state.data }
  }

}