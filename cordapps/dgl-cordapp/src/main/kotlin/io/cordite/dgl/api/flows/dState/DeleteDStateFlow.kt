/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.dState

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.api.flows.crud.CrudUpdateReceiverFlow
import io.cordite.dgl.api.flows.token.collectSignatures
import io.cordite.dgl.api.flows.token.createSessions
import io.cordite.dgl.api.flows.token.toWellKnownParties
import io.cordite.dgl.contract.v1.crud.CrudCommands
import io.cordite.dgl.contract.v1.dstate.DynamicState
import net.corda.core.contracts.StateAndRef
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

class DeleteDStateFlow(
  private val inputDStateAndRef: StateAndRef<DynamicState>,
  private val notary: Party
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    val inputState = inputDStateAndRef.state.data
    val signers =
      (inputState.updaters.map { it.owningKey } + inputState.creator.owningKey).distinct()
    val txb = TransactionBuilder(notary = notary, serviceHub = serviceHub).apply {
      addCommand(CrudCommands.delete(ourIdentity, serviceHub), signers)
      addInputState(inputDStateAndRef)
    }
    val participants = inputState.participants
    val wellKnownParties = participants.toWellKnownParties(serviceHub)
    val sessions = createSessions(wellKnownParties)
    val signedTransaction = collectSignatures(txb, sessions)
    return subFlow(FinalityFlow(signedTransaction, sessions))
  }

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class Sender(
    private val inputDStateAndRef: StateAndRef<DynamicState>,
    private val notary: Party
  ) : FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call(): SignedTransaction {
      return subFlow(DeleteDStateFlow(inputDStateAndRef, notary))
    }
  }
}

class DeleteDStateFlowReceiver(
  private val otherPartySession: FlowSession
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    return subFlow(CrudUpdateReceiverFlow<DynamicState>(otherPartySession))
  }

  @InitiatedBy(DeleteDStateFlow.Sender::class)
  class Receiver(private val otherPartySession: FlowSession) :
    FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call(): SignedTransaction {
      return subFlow(DeleteDStateFlowReceiver(otherPartySession))
    }
  }
}
