/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.dState

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.api.flows.crud.CrudUpdateFlow
import io.cordite.dgl.api.flows.crud.CrudUpdateReceiverFlow
import io.cordite.dgl.contract.v1.dstate.DynamicState
import io.cordite.dgl.contract.v1.dstate.DynamicStateContract
import net.corda.core.contracts.StateAndRef
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction

class UpdateDStateFlow(
  private val inputDStateAndRef: StateAndRef<DynamicState>,
  private val outputDState: DynamicState,
  private val notary: Party
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    val inputState = inputDStateAndRef.state.data

    val currentParty = serviceHub.myInfo.legalIdentities.first()
    if (!inputState.updaters.contains(currentParty))
      throw RuntimeException("Cannot update dynamic state, current node is not in updaters")

    return subFlow(
      CrudUpdateFlow(
        inputStates = listOf(inputDStateAndRef),
        states = listOf(outputDState),
        contractClassName = DynamicStateContract.CONTRACT_ID,
        notary = notary,
        signers = (
          outputDState.updaters.map { it.owningKey } +
            outputDState.creator.owningKey +
            inputState.updaters.map { it.owningKey } +
            inputState.creator.owningKey
          ).distinct()
      )
    )
  }

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class Sender(
    private val inputDStateAndRef: StateAndRef<DynamicState>,
    private val outputDState: DynamicState,
    private val notary: Party
  ) : FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call(): SignedTransaction {
      return subFlow(UpdateDStateFlow(inputDStateAndRef, outputDState, notary))
    }
  }
}

class UpdateDStateFlowReceiver(
  private val otherPartySession: FlowSession
) : FlowLogic<SignedTransaction>() {

  @Suspendable
  override fun call(): SignedTransaction {
    return subFlow(CrudUpdateReceiverFlow<DynamicState>(otherPartySession))
  }

  @InitiatedBy(UpdateDStateFlow.Sender::class)
  class Receiver(private val otherPartySession: FlowSession) :
    FlowLogic<SignedTransaction>() {

    @Suspendable
    override fun call(): SignedTransaction {
      return subFlow(UpdateDStateFlowReceiver(otherPartySession))
    }
  }
}
