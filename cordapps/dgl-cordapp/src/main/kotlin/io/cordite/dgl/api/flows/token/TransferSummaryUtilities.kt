/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token

import io.cordite.commons.utils.transaction
import io.cordite.dgl.contract.v1.tag.Tag
import io.cordite.dgl.contract.v1.token.TokenTransactionSummary
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.SecureHash
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.QueryCriteria

fun ServiceHub.filterTokenTransactionSummariesByAllTags(
  tags: List<Tag>
): List<TokenTransactionSummary.State> {
  val stringOfCommaSeparatedQuestionMarks = "? ,".repeat(tags.size).dropLast(2)
  val paramSize = tags.size
  val query = """
    SELECT STATE.*
    FROM CORDITE_TOKEN_TRANSACTION as STATE
      INNER JOIN (
        SELECT TAGS.TRANSACTION_ID,
               TAGS.OUTPUT_INDEX,
               COUNT(TAGS.CATEGORYANDVALUE)
        FROM CORDITE_TOKEN_TRANSACTION_TAGS as TAGS 
          INNER JOIN VAULT_STATES ON (
            (TAGS.TRANSACTION_ID = VAULT_STATES.TRANSACTION_ID) AND
            (TAGS.OUTPUT_INDEX = VAULT_STATES.OUTPUT_INDEX)
          )
        WHERE (
          VAULT_STATES.STATE_STATUS = 0 AND
          TAGS.CATEGORYANDVALUE IN ($stringOfCommaSeparatedQuestionMarks)
        )
        GROUP BY TAGS.TRANSACTION_ID, TAGS.OUTPUT_INDEX
        HAVING COUNT(TAGS.CATEGORYANDVALUE) = $paramSize
      ) as tempTable ON (
        (STATE.TRANSACTION_ID = tempTable.TRANSACTION_ID) AND
        (STATE.OUTPUT_INDEX = tempTable.OUTPUT_INDEX)
      )
  """.trimIndent()
  return transaction {
    val preparedStatement = jdbcSession().prepareStatement(query)
    for (indexedTag in tags.withIndex()) {
      val index = indexedTag.index + 1 // preparedStatement.setXXX starts from 1 not 0
      val tagSearchTerm = "${indexedTag.value.category}:${indexedTag.value.value}"
      preparedStatement.setString(index, tagSearchTerm)
    }

    val rs = preparedStatement.executeQuery()

    val stateRefs = mutableListOf<StateRef>()
    while (rs.next()) {
      val sh = SecureHash.parse(rs.getString("TRANSACTION_ID"))
      val i = rs.getInt("OUTPUT_INDEX")
      stateRefs.add(StateRef(sh, i))
    }

    vaultService.queryBy(
      contractStateType = TokenTransactionSummary.State::class.java,
      criteria = QueryCriteria.VaultQueryCriteria(stateRefs = stateRefs)
    ).states.map { it.state.data.copy(transactionId = it.ref.txhash) }
  }

}