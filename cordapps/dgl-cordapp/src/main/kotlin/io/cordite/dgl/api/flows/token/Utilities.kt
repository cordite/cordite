/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.*
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.IdentityService
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.unwrap

@Suspendable
fun <T> FlowLogic<T>.execute(transactionBuilder: TransactionBuilder): SignedTransaction {
  val participants = transactionBuilder.outputStates().flatMap { it.data.participants }
  val wellKnownParties = participants.toWellKnownParties(serviceHub)
  val sessions = createSessions(wellKnownParties)
  val signedTransaction = collectSignatures(transactionBuilder, sessions)
  return subFlow(FinalityFlow(signedTransaction, sessions))
}

@Suspendable
fun <T> FlowLogic<T>.collectSignatures(
  transactionBuilder: TransactionBuilder,
  sessions: List<FlowSession>
): SignedTransaction {
  val signingKeys = transactionBuilder.commands().flatMap { it.signers }.toSet()
  val ourKeys = serviceHub.keyManagementService.filterMyKeys(signingKeys)
  val initialTransaction = serviceHub.signInitialTransaction(transactionBuilder, ourKeys)
  val signingSessions = sessions.filter {
    cordaCheck(it.counterparty !in serviceHub.myInfo.legalIdentities) {
      "As the initiator, did not expect a session to myself"
    }
    it.counterparty.owningKey in signingKeys
  }
  sessions.forEach { session ->
    val transactionContext =
      TransactionContext(session in signingSessions, initialTransaction.id)
    session.send(transactionContext)
  }
  return when {
    signingSessions.isEmpty() -> initialTransaction
    else -> subFlow(CollectSignaturesFlow(initialTransaction, signingSessions))
  }
}

@Suspendable
fun <T> FlowLogic<T>.createSessions(wellKnownParties: List<Party>): List<FlowSession> {
  val otherWellKnownParties =
    wellKnownParties.filter { !serviceHub.myInfo.isLegalIdentity(it) }
  return when {
    otherWellKnownParties.isEmpty() -> emptyList()
    else -> sessionsForParties(otherWellKnownParties)
  }
}

@Suspendable
fun List<AbstractParty>.toWellKnownParties(services: ServiceHub): List<Party> {
  return map(services.identityService::requireKnownConfidentialIdentity)
}

@Suspendable
fun FlowLogic<*>.sessionsForParties(parties: List<AbstractParty>): List<FlowSession> {
  val wellKnownParties = parties.toWellKnownParties(serviceHub)
  return wellKnownParties.map(::initiateFlow)
}

// Extension function that has nicer error message than the default one from [IdentityService::requireWellKnownPartyFromAnonymous].
@Suspendable
fun IdentityService.requireKnownConfidentialIdentity(party: AbstractParty): Party {
  return wellKnownPartyFromAnonymous(party)
    ?: throw IllegalArgumentException(
      "Called flow with anonymous party that node doesn't know about. " +
        "Make sure that RequestConfidentialIdentity flow is called before."
    )
}

/**
 * Equivalent to Kotlin's check but throws [FlowException]
 * @throws FlowException
 */
public inline fun cordaCheck(value: Boolean, lazyMessage: () -> Any): Unit {
  if (!value) {
    val message = lazyMessage()
    throw FlowException(message.toString())
  }
}

@Suspendable
fun FlowLogic<*>.checkSignAndFinalise(
  otherSession: FlowSession,
  checkTransaction: (SignedTransaction) -> Unit = {}
): SignedTransaction {
  val transactionContext = otherSession.receive<TransactionContext>().unwrap { it }

  if (transactionContext.isSignatureRequired) {
    val signTransactionFlow = object : SignTransactionFlow(otherSession) {
      override fun checkTransaction(stx: SignedTransaction) {
        checkTransaction(stx)
      }
    }
    subFlow(signTransactionFlow)
  }
  return subFlow(ReceiveFinalityFlow(otherSession, transactionContext.transactionId))
}

@CordaSerializable
data class TransactionContext(
  val isSignatureRequired: Boolean,
  val transactionId: SecureHash
)
