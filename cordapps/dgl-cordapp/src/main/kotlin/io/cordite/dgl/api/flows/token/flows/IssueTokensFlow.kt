/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.api.flows.account.verifyAccountsExist
import io.cordite.dgl.api.flows.token.flows.TokenTransactionSummaryFunctions.addTokenTransactionSummary
import io.cordite.dgl.contract.v1.token.TokenContract
import io.cordite.dgl.contract.v1.token.TokenState
import io.cordite.dgl.contract.v1.token.TokenTransactionSummary
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

@InitiatingFlow
@StartableByRPC
@StartableByService
class IssueTokensFlow(private val tokens: List<TokenState>,
                      private val notary: Party,
                      private val description: String)
  : FlowLogic<SignedTransaction>() {

  constructor(token: TokenState, notary: Party, description: String) : this(listOf(token), notary, description)

  @Suspendable
  override fun call(): SignedTransaction {
    serviceHub.verifyAccountsExist(tokens.map(TokenState::accountAddress))
    val txb = TransactionBuilder(notary)
    val stx = serviceHub.signInitialTransaction(txb.apply {
      addCommand(TokenContract.Command.Issue, ourIdentity.owningKey)
      tokens.forEach { addOutputState(it, it.contractId) }
      addTokenTransactionSummary(TokenContract.Command.Issue, ourIdentity, description, listOf(), nettedAccountAmounts(tokens))
    })
    val secureHash = subFlow(FinalityFlow(stx, emptyList())).id
    return waitForLedgerCommit(secureHash)
  }

  private fun nettedAccountAmounts(tokens: List<TokenState>) = tokens.map { token ->
    TokenTransactionSummary.NettedAccountAmount(token.accountAddress, token.amount)
  }
}