/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token.selection

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.contract.v1.token.BigDecimalAmount
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.dgl.contract.v1.token.TokenState
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.SecureHash
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.internal.uncheckedCast
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.StatesNotAvailableException
import net.corda.core.utilities.contextLogger
import net.corda.core.utilities.millis
import net.corda.core.utilities.toNonEmptySet
import net.corda.core.utilities.trace
import java.math.BigDecimal
import java.sql.Connection
import java.sql.DatabaseMetaData
import java.sql.ResultSet
import java.util.*
import java.util.concurrent.atomic.AtomicReference

/**
 * Pluggable interface to allow for different token selection provider implementations
 * Default implementation in finance workflow module uses H2 database and a custom function within H2 to perform aggregation.
 * Custom implementations must implement this interface and declare their implementation in
 * `META-INF/services/io.cordite.dgl.api.flows.token.selection.AbstractTokenSelection`.
 */
// TODO: make parameters configurable when we get CorDapp configuration.
abstract class AbstractTokenSelection(private val maxRetries: Int = 8, private val retrySleep: Int = 100,
                                      private val retryCap: Int = 2000) {
  companion object {
    private val instance = AtomicReference<AbstractTokenSelection>()

    fun getInstance(metadata: () -> DatabaseMetaData): AbstractTokenSelection {
      return instance.get() ?: {
        val metadataLocal = metadata()
        val tokenSelectionAlgos = ServiceLoader.load(AbstractTokenSelection::class.java, this::class.java.classLoader).toList()
        val tokenSelectionAlgo = tokenSelectionAlgos.firstOrNull { it.isCompatible(metadataLocal) }
        tokenSelectionAlgo?.let {
          instance.set(tokenSelectionAlgo)
          tokenSelectionAlgo
        }
          ?: throw ClassNotFoundException("\nUnable to load compatible token selection algorithm implementation for JDBC driver name '${metadataLocal.driverName}'." +
            "\nPlease specify an implementation in META-INF/services/${AbstractTokenSelection::class.qualifiedName}." +
            "\nAvailable implementations: $tokenSelectionAlgos")
      }.invoke()
    }

    private val log = contextLogger()
  }

  /**
   * Upon dynamically loading configured Token Selection algorithms declared in META-INF/services
   * this method determines whether the loaded implementation is compatible and usable with the currently
   * loaded JDBC driver.
   * Note: the first loaded implementation to pass this check will be used at run-time.
   */
  protected abstract fun isCompatible(metadata: DatabaseMetaData): Boolean

  /**
   * A vendor specific query(ies) to gather Token states that are available.
   * @param amount The amount of currency desired (ignoring issues, but specifying the currency)
   * @param lockId The FlowLogic.runId.uuid of the flow, which is used to soft reserve the states.
   * Also, previous outputs of the flow will be eligible as they are implicitly locked with this id until the flow completes.
   * @param notary If null the notary source is ignored, if specified then only states marked
   * with this notary are included.
   * @param withResultSet Function that contains the business logic. The JDBC ResultSet with the matching states that were found. If sufficient funds were found these will be locked,
   * otherwise what is available is returned unlocked for informational purposes.
   * @return The result of the withResultSet function
   */
  protected abstract fun executeQuery(
    connection: Connection,
    amount: BigDecimalAmount<TokenDescriptor>,
    accountId: String,
    lockId: UUID,
    notary: Party?,
    withResultSet: (ResultSet) -> Boolean
  ): Boolean

  abstract override fun toString(): String

  /**
   * Query to gather Token states that are available and retry if they are temporarily unavailable.
   * @param services The service hub to allow access to the database session
   * @param amount The amount of currency desired (ignoring issues, but specifying the currency)
   * @param notary If null the notary source is ignored, if specified then only states marked
   * with this notary are included.
   * @param lockId The FlowLogic.runId.uuid of the flow, which is used to soft reserve the states.
   * Also, previous outputs of the flow will be eligible as they are implicitly locked with this id until the flow completes.
   * @return The matching states that were found. If sufficient funds were found these will be locked,
   * otherwise what is available is returned unlocked for informational purposes.
   */
  @Suspendable
  fun unconsumedTokenStatesForSpending(
    services: ServiceHub,
    amount: BigDecimalAmount<TokenDescriptor>,
    accountId: String,
    notary: Party? = null,
    lockId: UUID
  ): Set<StateAndRef<TokenState>> {
    val stateAndRefs = mutableSetOf<StateAndRef<TokenState>>()

    for (retryCount in 1..maxRetries) {
      if (!attemptSpend(services, amount, accountId, lockId, notary, stateAndRefs)) {
        log.warn("Coin selection failed on attempt $retryCount")
        // TODO: revisit the back off strategy for contended spending.
        if (retryCount != maxRetries) {
          stateAndRefs.clear()
          val durationMillis = (minOf(retrySleep.shl(retryCount), retryCap / 2) * (1.0 + Math.random())).toInt()
          FlowLogic.sleep(durationMillis.millis)
        } else {
          log.warn("Insufficient spendable states identified for $amount")
        }
      } else {
        break
      }
    }
    return stateAndRefs
  }

  private fun attemptSpend(
    services: ServiceHub,
    amount: BigDecimalAmount<TokenDescriptor>,
    accountId: String,
    lockId: UUID,
    notary: Party?,
    stateAndRefs: MutableSet<StateAndRef<TokenState>>
  ): Boolean {
    val connection = services.jdbcSession()
    try {
      // we select spendable states irrespective of lock but prioritised by unlocked ones (Eg. null)
      // the softLockReserve update will detect whether we try to lock states locked by others
      return executeQuery(connection, amount, accountId, lockId, notary) { rs ->
        stateAndRefs.clear()

        var totalAmount = BigDecimal.ZERO
        val stateRefs = mutableSetOf<StateRef>()
        while (rs.next()) {
          val txHash = SecureHash.parse(rs.getString(1))
          val index = rs.getInt(2)
          val quantity = rs.getBigDecimal(3)
          totalAmount = rs.getBigDecimal(4)
          val rowLockId = rs.getString(5)
          stateRefs.add(StateRef(txHash, index))
          log.trace { "ROW: $rowLockId ($lockId): ${StateRef(txHash, index)} : $quantity ($totalAmount)" }
        }

        if (stateRefs.isNotEmpty()) {
          // TODO: future implementation to retrieve contract states from a Vault BLOB store
          stateAndRefs.addAll(uncheckedCast(services.loadStates(stateRefs)))
        }

        val success = stateAndRefs.isNotEmpty() && totalAmount >= amount.quantity
        if (success) {
          // we should have a minimum number of states to satisfy our selection `amount` criteria
          log.trace("Coin selection for $amount retrieved ${stateAndRefs.count()} states totalling $totalAmount pennies: $stateAndRefs")

          // With the current single threaded state machine available states are guaranteed to lock.
          // TODO However, we will have to revisit these methods in the multi-threaded future.
          services.vaultService.softLockReserve(lockId, (stateAndRefs.map { it.ref }).toNonEmptySet())
        } else {
          log.trace("Coin selection requested $amount but retrieved $totalAmount pennies with state refs: ${stateAndRefs.map { it.ref }}")
        }
        success
      }

      // retry as more states may become available
    } catch (e: StatesNotAvailableException) { // Should never happen with single threaded state machine
      log.warn(e.message)
      // retry only if there are locked states that may become available again (or consumed with change)
    }
    return false
  }
}
