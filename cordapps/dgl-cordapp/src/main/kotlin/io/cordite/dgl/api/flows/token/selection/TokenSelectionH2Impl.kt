/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token.selection

import io.cordite.dgl.contract.v1.token.BigDecimalAmount
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import net.corda.core.identity.Party
import net.corda.core.utilities.contextLogger
import net.corda.core.utilities.debug
import java.sql.Connection
import java.sql.DatabaseMetaData
import java.sql.ResultSet
import java.util.*

class TokenSelectionH2Impl : AbstractTokenSelection() {
    companion object {
        const val JDBC_DRIVER_NAME = "H2 JDBC Driver"
        private val log = contextLogger()
    }

    override fun isCompatible(metadata: DatabaseMetaData): Boolean {
        return metadata.driverName == JDBC_DRIVER_NAME
    }

    override fun toString() = "${this::class.qualifiedName} for '$JDBC_DRIVER_NAME'"

    //       We are using an H2 specific means of selecting a minimum set of rows that match a request amount of coins:
    //       1) There is no standard SQL mechanism of calculating a cumulative total on a field and restricting row selection on the
    //          running total of such an accumulator
    //       2) H2 uses session variables to perform this accumulator function:
    //          http://www.h2database.com/html/functions.html#set
    //       3) H2 does not support JOIN's in FOR UPDATE (hence we are forced to execute 2 queries)
    override fun executeQuery(
      connection: Connection,
      amount: BigDecimalAmount<TokenDescriptor>,
      accountId: String,
      lockId: UUID,
      notary: Party?,
      withResultSet: (ResultSet) -> Boolean
    ): Boolean {
        connection.createStatement().use { it.execute("CALL SET(@t, CAST(0 AS DECIMAL));") }

        // state_status = 0 -> UNCONSUMED.
        // is_relevant = 0 -> RELEVANT.
        val selectJoin = """
                    SELECT vs.transaction_id, vs.output_index, ct.amount, SET(@t, ifnull(@t,0)+ct.amount) total_amount, vs.lock_id
                    FROM vault_states AS vs, cordite_token AS ct
                    WHERE vs.transaction_id = ct.transaction_id AND vs.output_index = ct.output_index
                    AND vs.state_status = 0
                    AND vs.relevancy_status = 0
                    AND ct.account_id = ?
                    AND ct.symbol = ?
                    AND ct.issuer = ?
                    AND @t < ?
                    AND (vs.lock_id = ? OR vs.lock_id is null)
                    """ +
                (if (notary != null) " AND vs.notary_name = ?" else "")

        // Use prepared statement for protection against SQL Injection (http://www.h2database.com/html/advanced.html#sql_injection)
        connection.prepareStatement(selectJoin).use { psSelectJoin ->
            var pIndex = 0
            psSelectJoin.setString(++pIndex, accountId)
            psSelectJoin.setString(++pIndex, amount.amountType.symbol)
            psSelectJoin.setString(++pIndex, amount.amountType.issuerName.toString())

            psSelectJoin.setBigDecimal(++pIndex, amount.quantity)
            psSelectJoin.setString(++pIndex, lockId.toString())
            if (notary != null)
                psSelectJoin.setString(++pIndex, notary.name.toString())
            log.debug { psSelectJoin.toString() }
            psSelectJoin.executeQuery().use { rs ->
                return withResultSet(rs)
            }
        }
    }
}