/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token.selection

import io.cordite.dgl.contract.v1.token.BigDecimalAmount
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import net.corda.core.identity.Party
import net.corda.core.utilities.contextLogger
import net.corda.core.utilities.debug
import java.sql.Connection
import java.sql.DatabaseMetaData
import java.sql.ResultSet
import java.util.*

class TokenSelectionPostgreSQLImpl : AbstractTokenSelection() {

    companion object {
        const val JDBC_DRIVER_NAME = "PostgreSQL JDBC Driver"
        private val log = contextLogger()
    }

    override fun isCompatible(metadata: DatabaseMetaData): Boolean {
        return metadata.driverName == JDBC_DRIVER_NAME
    }

    override fun toString() = "${this::class.qualifiedName} for '$JDBC_DRIVER_NAME'"

    //       This is using PostgreSQL window functions for selecting a minimum set of rows that match a request amount of coins:
    //       1) This may also be possible with user-defined functions (e.g. using PL/pgSQL)
    //       2) The window function accumulated column (`total`) does not include the current row (starts from 0) and cannot
    //          appear in the WHERE clause, hence restricting row selection and adjusting the returned total in the outer query.
    //       3) Currently (version 9.6), FOR UPDATE cannot be specified with window functions
    override fun executeQuery(connection: Connection, amount: BigDecimalAmount<TokenDescriptor>, accountId: String, lockId: UUID, notary: Party?, withResultSet: (ResultSet) -> Boolean): Boolean {
        // state_status = 0 -> UNCONSUMED.
        // is_relevant = 0 -> RELEVANT.
        val selectJoin = """SELECT nested.transaction_id, nested.output_index, nested.amount,
                        nested.total+nested.amount as total_amount, nested.lock_id
                       FROM
                       (SELECT vs.transaction_id, vs.output_index, ct.amount,
                       coalesce((SUM(ct.amount) OVER (PARTITION BY 1 ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING)), 0)
                       AS total, vs.lock_id
                        FROM vault_states AS vs, cordite_token AS ct
                        WHERE vs.transaction_id = ct.transaction_id AND vs.output_index = ct.output_index
                        AND vs.state_status = 0
                        AND vs.relevancy_status = 0
                        AND ct.account_id = ?
                        AND ct.symbol = ?
                        AND ct.issuer = ?
                        AND (vs.lock_id = ? OR vs.lock_id is null)
                        """ +
                (if (notary != null)
                    " AND vs.notary_name = ?" else "") +
                """)
                        nested WHERE nested.total < ?
                     """

        var pIndex = 0
        connection.prepareStatement(selectJoin).use { statement ->
            statement.setString(++pIndex, accountId)
            statement.setString(++pIndex, amount.amountType.symbol)
            statement.setString(++pIndex, amount.amountType.issuerName.toString())
            statement.setString(++pIndex, lockId.toString())
            if (notary != null) {
                statement.setString(++pIndex, notary.name.toString())
            }
            statement.setBigDecimal(++pIndex, amount.quantity)
            log.debug { statement.toString() }

            statement.executeQuery().use { rs ->
                return withResultSet(rs)
            }
        }
    }
}
