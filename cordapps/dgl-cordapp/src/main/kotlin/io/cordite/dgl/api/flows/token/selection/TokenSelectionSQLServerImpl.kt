/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.token.selection

import io.cordite.dgl.contract.v1.token.BigDecimalAmount
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import net.corda.core.identity.Party
import net.corda.core.utilities.contextLogger
import net.corda.core.utilities.debug
import java.sql.Connection
import java.sql.DatabaseMetaData
import java.sql.ResultSet
import java.util.*

class TokenSelectionSQLServerImpl : AbstractTokenSelection() {
    companion object {
        val JDBC_DRIVER_NAME_REGEX = """Microsoft JDBC Driver (\w+.\w+) for SQL Server""".toRegex()
        private val log = contextLogger()
    }

    override fun isCompatible(metadata: DatabaseMetaData): Boolean {
        return  JDBC_DRIVER_NAME_REGEX.matches(metadata.driverName)
    }

    override fun toString() = "${this::class.qualifiedName} for '$JDBC_DRIVER_NAME_REGEX'"

    //      This is one MSSQL implementation of the query to select just enough token states to meet the desired amount.
    //      We select the token states with smaller amounts first so that as the result, we minimize the numbers of
    //      unspent token states remaining in the vault.
    //
    //      If there is not enough token, the query will return an empty resultset, which should signal to the caller
    //      of an exception, since the desired amount is assumed to always > 0.
    //      NOTE: The other two implementations, H2 and PostgresSQL, behave differently in this case - they return
    //      all in the vault instead of nothing. That seems to give the caller an extra burden to verify total returned
    //      >= amount.
    //      In addition, extra data fetched results in unnecessary I/O.
    //      Nevertheless, if so desired, we can achieve the same by changing the last FROM clause to
    //          FROM CTE LEFT JOIN Boundary AS B ON 1 = 1
    //          WHERE B.seqNo IS NULL OR CTE.seqNo <= B.seqNo
    //
    //      Common Table Expression and Windowed functions help make the query more readable.
    //      Query plan does index scan on amount_idx, which may be unavoidable due to the nature of the query.
    override fun executeQuery(
      connection: Connection,
      amount: BigDecimalAmount<TokenDescriptor>,
      accountId: String,
      lockId: UUID,
      notary: Party?,
      withResultSet: (ResultSet) -> Boolean
    ): Boolean {
        val sb = StringBuilder()
        // state_status = 0 -> UNCONSUMED.
        // is_relevant = 0 -> RELEVANT.
        sb.append( """
            ;WITH CTE AS
            (
            SELECT
              vs.transaction_id,
              vs.output_index,
              ct.amount,
              vs.lock_id,
              total_amount = SUM(ct.amount) OVER (ORDER BY ct.amount),
              seqNo = ROW_NUMBER() OVER (ORDER BY ct.amount)
            FROM vault_states AS vs INNER JOIN cordite_token AS ct
                ON vs.transaction_id = ct.transaction_id AND vs.output_index = ct.output_index
            WHERE
              vs.state_status = 0
              AND vs.relevancy_status = 0
              AND ct.account_id = ?
              AND ct.symbol = ?
              AND ct.issuer = ?
              AND (vs.lock_id = ? OR vs.lock_id IS NULL)
            """
        )
        if (notary != null)
            sb.append("""
              AND vs.notary_name = ?
            """)
        sb.append(
            """
            ),
            Boundary AS
            (
              SELECT TOP (1) * FROM  CTE WHERE total_amount >= ? ORDER BY seqNo
            )
            SELECT CTE.transaction_id, CTE.output_index, CTE.amount, CTE.total_amount, CTE.lock_id
              FROM CTE INNER JOIN Boundary AS B ON CTE.seqNo <= B.seqNo
            ;
            """
        )
        val selectJoin = sb.toString()
        log.debug { selectJoin }
        connection.prepareStatement(selectJoin).use { psSelectJoin ->
            var pIndex = 0
          psSelectJoin.setString(++pIndex, accountId)
          psSelectJoin.setString(++pIndex, amount.amountType.symbol)
          psSelectJoin.setString(++pIndex, amount.amountType.issuerName.toString())
          psSelectJoin.setString(++pIndex, lockId.toString())
            if (notary != null)
                psSelectJoin.setString(++pIndex, notary.name.toString())
            psSelectJoin.setBigDecimal(++pIndex, amount.quantity)

            log.debug { psSelectJoin.toString() }

            psSelectJoin.executeQuery().use { rs ->
                return withResultSet(rs)
            }
        }
    }
}