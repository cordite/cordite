/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api.flows.tokentypes

import io.cordite.commons.utils.transaction
import io.cordite.dgl.contract.v1.token.TokenDescriptor
import io.cordite.dgl.contract.v1.token.TokenTypeSchemaV1
import io.cordite.dgl.contract.v1.token.TokenTypeState
import net.corda.core.contracts.StateAndRef
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder

fun ServiceHub.listAllTokenTypes(page: Int, pageSize: Int): List<TokenTypeState> {
  return transaction {
    val res = vaultService
      .queryBy<TokenTypeState>(QueryCriteria.VaultQueryCriteria(
        contractStateTypes = setOf(TokenTypeState::class.java)),
        paging = PageSpecification(pageNumber = page, pageSize = pageSize)
      )
      .states.map { it.state.data }
    res
  }
}

fun ServiceHub.findTokenTypesIssuesByMe(symbol: String): StateAndRef<TokenTypeState> {
  val identities = myInfo.legalIdentities.map { it.name.toString() }
  val criteria = builder {
    QueryCriteria.VaultCustomQueryCriteria(TokenTypeSchemaV1.PersistedTokenType::symbol.equal(symbol)).and(
      QueryCriteria.VaultCustomQueryCriteria(TokenTypeSchemaV1.PersistedTokenType::issuer.`in`(identities)))
  }
  return transaction {
    vaultService.queryBy<TokenTypeState>(criteria).states.firstOrNull()
      ?: error("unknown token type $symbol")
  }
}

fun ServiceHub.findTokenType(
  tokenDescriptor: TokenDescriptor
) : StateAndRef<TokenTypeState>? {
  return findTokenType(tokenDescriptor.symbol, tokenDescriptor.issuerName)
}

fun ServiceHub.findTokenType(
  symbol: String,
  issuerName: CordaX500Name
): StateAndRef<TokenTypeState>? {
  val criteria = builder {
    QueryCriteria.VaultCustomQueryCriteria(TokenTypeSchemaV1.PersistedTokenType::symbol.equal(symbol)).and(
      QueryCriteria.VaultCustomQueryCriteria(TokenTypeSchemaV1.PersistedTokenType::issuer.equal(issuerName.toString())))
  }
  return transaction {
    vaultService.queryBy<TokenTypeState>(criteria).states.firstOrNull()
  }
}
