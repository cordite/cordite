/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api

import io.cordite.braid.corda.BraidConfig
import io.cordite.commons.braid.BraidCordaService
import io.cordite.commons.braid.bootstrapAndLog
import io.cordite.commons.utils.jackson.CorditeJacksonInit
import io.cordite.dgl.api.flows.token.flows.IssueTokensFlow
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.loggerFor

@CordaService
class LedgerTestBraidServer(private val serviceHub: AppServiceHub) : SingletonSerializeAsToken() {

  companion object {
    private val log = loggerFor<LedgerTestBraidServer>()

    init {
      CorditeJacksonInit.init()
    }
  }

  private val org = serviceHub.myInfo.legalIdentities.first().name.organisation.replace(" ", "")
  private val portProperty = "braid.$org.port"

  init {
    val port = getBraidPort()
    when {
      port > 0 -> {
        log.info("starting $org braid on port $port")
        BraidConfig().withPort(port)
          .withFlow(IssueTokensFlow::class.java)
          .apply { startUpBraid(this) }
      }
      else -> log.info("braid port not provided in property $portProperty. not starting braid")
    }
  }

  private fun startUpBraid(config: BraidConfig) {
    log.info("starting $org braid on port ${config.port}")
    val finalConfig = BraidCordaService.startAllBraidServicesAndUpdateConfig(config, serviceHub)
    finalConfig.bootstrapAndLog(serviceHub)
  }


  private fun getBraidPort(): Int {
    return System.getProperty(portProperty)?.toInt() ?: 0
  }
}