/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api

import io.cordite.braid.corda.BraidConfig
import io.cordite.commons.braid.BraidCordaService
import io.cordite.commons.braid.auth.BasicAuthProvider
import io.cordite.commons.braid.bootstrapAndLog

const val port = 8081
val dummyAppServiceHub = DummyAppServiceHub("io.cordite.commons", "io.cordite.dgl.api")
val baseBraidConfig = BraidConfig(port = port)
  .withAuthConstructor { BasicAuthProvider("sa", "admin") } // this will get swapped out for an JWTAuthProvider that wraps this provider

fun main(args: Array<String>) {
  BraidCordaService.startAllBraidServicesAndUpdateConfig(baseBraidConfig, dummyAppServiceHub).bootstrapAndLog(dummyAppServiceHub)
}
