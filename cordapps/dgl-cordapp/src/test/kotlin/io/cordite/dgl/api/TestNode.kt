/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.api

import io.cordite.braid.client.BraidClient
import io.cordite.braid.core.async.getOrThrow
import io.cordite.test.utils.BraidClientHelper
import io.cordite.test.utils.BraidPortHelper
import io.cordite.test.utils.WaitForHttpEndPoint
import io.vertx.core.Future
import io.vertx.core.Promise
import io.vertx.core.Vertx
import net.corda.core.identity.Party
import net.corda.core.utilities.loggerFor
import net.corda.testing.node.StartedMockNode

class TestNode(val node: StartedMockNode, braidPortHelper: BraidPortHelper) {
  companion object {
    private val log = loggerFor<TestNode>()
  }
  private val braidClient: BraidClient
  private val vertx: Vertx = Vertx.vertx()

  val party: Party = node.info.legalIdentities.first()
  val ledgerService: LedgerApi

  init {
    val succeeded = Promise.promise<Unit>()
    val port = braidPortHelper.portForParty(party)
    WaitForHttpEndPoint.waitForHttpEndPoint(vertx = vertx, port = port, handler = succeeded, path = "/api/")
    succeeded.future().getOrThrow()
    log.info("attempting to bind to test service on $port")
    braidClient = BraidClientHelper.braidClient(port, "ledger", vertx)
    ledgerService = braidClient.bind(LedgerApi::class.java)
    log.info("bound to test service on $port")
  }

  fun shutdown() {
    braidClient.close()
    vertx.close()
  }
}