/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.contract

import io.cordite.dgl.contract.v1.token.TokenType
import io.cordite.metering.schema.MeteringTermsAndConditionsSchemaV1
import net.corda.core.contracts.BelongsToContract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import java.time.Instant
import java.util.*

/**
 * Please note: we are using the concept of ownable state here, without using ownable state as we don't want the move command...
 * we can't arbitrarily move the MeteringTermsAndConditions between parties - the flow is strict for which party can do what
 */
@BelongsToContract(MeteringTermsAndConditionsContract::class)
data class MeteringTermsAndConditionsState(val meteringTermsAndConditionsProperties: MeteringTermsAndConditionsProperties, val owner: AbstractParty) : QueryableState, LinearState {

  override val participants: List<AbstractParty> = setOf(
      owner,
      meteringTermsAndConditionsProperties.meteringParty,
      meteringTermsAndConditionsProperties.meteredParty
  ).asSequence().filterNotNull().toList()
  override val linearId: UniqueIdentifier = UniqueIdentifier(meteringTermsAndConditionsProperties.meteringTermsAndConditionsId)

  override fun generateMappedObject(schema: MappedSchema): PersistentState {
    return when (schema) {
      is MeteringTermsAndConditionsSchemaV1 -> MeteringTermsAndConditionsSchemaV1.PersistentMeteringTermsAndConditions(
          meteringTermsAndConditionsId = meteringTermsAndConditionsProperties.meteringTermsAndConditionsId,
          meteringParty = meteringTermsAndConditionsProperties.meteringParty.name.toString(),
          meteredParty = meteringTermsAndConditionsProperties.meteredParty.name.toString(),
          billingToken = meteringTermsAndConditionsProperties.billingToken.uri,
          billingType = meteringTermsAndConditionsProperties.billingType.toString(), //TODO this is a place holder we need a KV sub table for this (probably)
          payParty = meteringTermsAndConditionsProperties.payParty.name.toString(),
          payAccountId = meteringTermsAndConditionsProperties.payAccountId,
          guardianNotaryParty = meteringTermsAndConditionsProperties.guardianNotaryParty.name.toString(),
          createdDateTime = meteringTermsAndConditionsProperties.createdDateTime,
          meteringTermsAndConditionsStatus = meteringTermsAndConditionsProperties.status
      )
      else -> throw IllegalArgumentException("Unrecognised schema $schema")
    }
  }

  override fun supportedSchemas(): Iterable<MappedSchema> = listOf(MeteringTermsAndConditionsSchemaV1)
}

@CordaSerializable
data class MeteringTermsAndConditionsProperties(
    val meteringTermsAndConditionsId: String = UUID.randomUUID().toString(),
    val meteringParty: Party,
    val meteredParty: Party,
    val billingToken: TokenType.Descriptor,
    val billingType: MeteringPerTransactionBillingType, // If this is MeteringBillingType the APIs using this get a not implemented exception.
    val payParty: Party,
    val guardianNotaryParty: Party,
    val payAccountId: String,
    val createdDateTime: Instant,
    val status: MeteringTermsAndConditionsStatus
)

@CordaSerializable
interface MeteringBillingType

@CordaSerializable
data class MeteringPerTransactionBillingType(
    val transactionCost: Long,
    val transactionCreditLimit: Long,
    val freeTransactions: Long
) : MeteringBillingType

@CordaSerializable
enum class MeteringTermsAndConditionsStatus {
  ISSUED,
  PROPOSED,
  ACCEPTED,
  REJECTED,
}
