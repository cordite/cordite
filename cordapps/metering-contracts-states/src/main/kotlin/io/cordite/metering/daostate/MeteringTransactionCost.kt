/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.daostate

import io.cordite.dgl.contract.v1.token.TokenType
import net.corda.core.serialization.CordaSerializable

//For the First Iteration Metering Transaction cost will be global and set by the Dao.

//For the future, notaries will issue their own transaction cost as:
    // Some Notaries will have high specs for fast transactions and be non validating notaries
    // Some Notaties will be highly trusted validating notaries for large transactions - say Mortgages
    // Some notaries might have very low cost - i.e. non validation, with low hardware foot print
//So eventually the class will look like this
//data class MeteringTransactionCosts(val transactionCosts: Map<Party,MeteringTransactionCost>)

@CordaSerializable
data class MeteringTransactionCost(val meteringTransactionCost: Int, val meteringTransactionTokenDescriptor: TokenType.Descriptor)


