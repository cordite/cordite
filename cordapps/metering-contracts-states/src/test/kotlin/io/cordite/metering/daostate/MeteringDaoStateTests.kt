/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.daostate

import io.cordite.metering.tesutils.TestUtils.Companion.TestTokenDescriptor
import net.corda.core.crypto.generateKeyPair
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import org.junit.Ignore
import org.junit.Test

@Ignore
class MeteringDaoStateTests {

  private val daoHoldingAccount ="dao-holding-account"
  private val daoFoundationAccount="dao-foundation-account"

  private val meteringNotaryKeys = generateKeyPair()
  private val meteringNotaryParty = Party(CordaX500Name("MeteringNotary", "MeteringNotary", "Sector 1", "GB"), meteringNotaryKeys.public)
  private val guardianNotaryKeys = generateKeyPair()
  private val guardianNotaryParty = Party(CordaX500Name("GuardianNotary", "GuardianNotary", "Sector 1", "GB"), guardianNotaryKeys.public)

  val daoNodeKeys = generateKeyPair()
  val daoNodeParty = Party(CordaX500Name("Cordite dao", "Cordite dao", "Sector 1", "GB"), daoNodeKeys.public)

  private val someOtherMeteringNotaryKeys = generateKeyPair()
  private val someOtherMeteringNotaryParty = Party(CordaX500Name("SomeOtherMeteringNotary", "SomeOtherMeteringNotary", "Sector 1", "GB"), someOtherMeteringNotaryKeys.public)

  private val meteringNotaryMember = MeteringNotaryMember(meteringNotaryParty, null, "meteringFeeAccount", "I am a cool metering notary", MeteringNotaryType.METERER)
  private val guardianNotaryMember = MeteringNotaryMember(guardianNotaryParty, null, "meteringFeeAccount", "I am a guardian of the galaxy", MeteringNotaryType.GUARDIAN)
  private val someOtherMeteringNotaryMember = MeteringNotaryMember(someOtherMeteringNotaryParty, null, "meteringFeeAccount", "I am just some other dude who fancies metering transactions for doughnuts", MeteringNotaryType.METERER)

  private val meteringFeeAllocation = MeteringFeeAllocation(daoHoldingAccount, 50, 40, 10, daoFoundationAccount)

  private val meteringTransactionCost = MeteringTransactionCost(10, TestTokenDescriptor(daoNodeParty))
  private val meteringNotaryMembers = mapOf(meteringNotaryMember.notaryParty.name.toString() to meteringNotaryMember, guardianNotaryMember.notaryParty.name.toString() to guardianNotaryMember)

  @Test
  fun `we can create a metering dao state`() {
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    assert(meteringModelData.meteringNotaryMembers.count() == 2)
    assert(meteringModelData.meteringFeeAllocation.daoHoldingAccountId == daoHoldingAccount)
    assert(meteringModelData.meteringFeeAllocation.daoFoundationAccount == daoFoundationAccount)
    assert(meteringModelData.meteringTransactionCost.meteringTransactionCost == 10)
  }

  @Test
  fun `we can update the transaction costs`() {
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    val newMeteringTransactionCosts = MeteringTransactionCost(12,  TestTokenDescriptor(daoNodeParty, "BRT"))
    val newMeteringModelData = meteringModelData.cloneWithNewMeteringTransactionCost(newMeteringTransactionCosts)
    assert(newMeteringModelData.meteringTransactionCost.meteringTransactionCost == 12)
    assert(newMeteringModelData.meteringTransactionCost.meteringTransactionTokenDescriptor.symbol == "BRT")
    assert(meteringModelData.meteringFeeAllocation.daoHoldingAccountId == daoHoldingAccount)
    assert(meteringModelData.meteringFeeAllocation.daoFoundationAccount == daoFoundationAccount)
  }

  @Test(expected = MeteringModelDataException::class)
  fun `we cant update the transaction costs if we dont change anything`() {
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    val newMeteringTransactionCosts = MeteringTransactionCost(10, TestTokenDescriptor(daoNodeParty))
    meteringModelData.cloneWithNewMeteringTransactionCost(newMeteringTransactionCosts)
  }

  @Test
  fun `we can update the metering fee allocation`() {
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    val newMeteringFeeAllocation = MeteringFeeAllocation(daoHoldingAccount, 30, 30, 40, daoFoundationAccount)
    val newMeteringDataModel = meteringModelData.cloneWithNewMeteringFeeAllocation(newMeteringFeeAllocation)
    assert(newMeteringDataModel.meteringFeeAllocation.daoHoldingAccountId == daoHoldingAccount)
    assert(newMeteringDataModel.meteringFeeAllocation.daoFoundationAllocation == 30)
    assert(newMeteringDataModel.meteringFeeAllocation.meterNotaryAllocation == 30)
    assert(newMeteringDataModel.meteringFeeAllocation.guardianNotaryAllocation == 40)
    assert(meteringModelData.meteringFeeAllocation.daoFoundationAccount == daoFoundationAccount)
  }

  @Test(expected = MeteringModelDataException::class)
  fun `we cant update the metering fee allocation if we change nothing`() {
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    val newMeteringFeeAllocation = MeteringFeeAllocation(daoHoldingAccount, 50, 40, 10, daoFoundationAccount)
    meteringModelData.cloneWithNewMeteringFeeAllocation(newMeteringFeeAllocation)
  }

  @Test
  fun `We can add a new notary member`() {
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    val newMeteringModelData = meteringModelData.cloneWithNewMeteringNotaryMember(someOtherMeteringNotaryMember)
    assert(newMeteringModelData.meteringNotaryMembers[someOtherMeteringNotaryMember.notaryParty.name.toString()] == someOtherMeteringNotaryMember)
  }

  @Test(expected = MeteringModelDataException::class)
  fun `We cant add the same notary member`() {
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    meteringModelData.cloneWithNewMeteringNotaryMember(meteringNotaryMember)
  }

  @Test
  fun `we can update an existing notary member`(){
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    val updatedMeteringNotaryMember = meteringNotaryMember.copy( accountId = "newFeeAccountName" )
    val newMeteringModelData = meteringModelData.cloneWithUpdatedMeteringNotaryMember(updatedMeteringNotaryMember)
    assert(newMeteringModelData.meteringNotaryMembers[meteringNotaryParty.name.toString()]?.accountId=="newFeeAccountName")
    assert(newMeteringModelData.meteringNotaryMembers[meteringNotaryParty.name.toString()]?.meteringNotaryType==MeteringNotaryType.METERER)
  }

  @Test(expected = MeteringModelDataException::class)
  fun `you can try and update a notary member with the same details, but you will fail miserably`(){
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    meteringModelData.cloneWithUpdatedMeteringNotaryMember(meteringNotaryMember)
  }

  @Test
  fun `you can delete a notary member`(){
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    val newMeteringDataModel = meteringModelData.cloneWithRemovedMeteringNotaryMember(meteringNotaryParty)
    assert(!newMeteringDataModel.meteringNotaryMembers.containsKey(meteringNotaryParty.name.toString()))
  }

  @Test(expected = MeteringModelDataException::class)
  fun `you cant delete a notary member if it does not exist`(){
    val meteringModelData = MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
    meteringModelData.cloneWithRemovedMeteringNotaryMember(someOtherMeteringNotaryParty)
  }
}