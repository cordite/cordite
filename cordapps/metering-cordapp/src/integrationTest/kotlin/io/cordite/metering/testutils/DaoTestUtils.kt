/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.testutils

import io.cordite.braid.core.async.getOrThrow
import io.cordite.dao.core.DaoState
import io.cordite.dao.proposal.NormalProposal
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import io.cordite.metering.NetworkRefresh
import io.cordite.test.utils.forTheLoveOfGodIgnoreThisBit
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.utilities.loggerFor
import org.junit.Assert

//TODO - move this to a shared dao test utils model - it might even come in handy for all those cordite fans out there ! //https://gitlab.com/cordite/cordite/issues/270
class DaoTestUtils {

  companion object {

    private val log = loggerFor<DaoTestUtils>()

    fun createDaoWithName(daoName: String, daoNode: TestNode, notaryName: CordaX500Name, dontCareIfTheyAlreadyExist: Boolean=false): DaoState {

      val currentDaos = daoNode.daoApi.daoInfo(daoName)

      var expectedDaoState = currentDaos.find { it.name ==daoName }

      if(expectedDaoState == null){
        expectedDaoState = NetworkRefresh.runWithRefresh { daoNode.daoApi.createDao(daoName, notaryName) }
        forTheLoveOfGodIgnoreThisBit()
      }
      else {
        if (!dontCareIfTheyAlreadyExist) {
          Assert.fail("there should be no dao called $daoName at the beginning")
        }
      }
      NetworkRefresh.runNetwork()

      assertDaoStateContainsMembers(getDaoWithRetry(daoNode, daoName,20), daoNode.party)
      return expectedDaoState
    }

    fun getDaoWithRetry(testNode: TestNode, daoName: String, retries : Int = 10): List<DaoState> {
      (1..retries).forEach {
        val daos = testNode.daoApi.daoInfo(daoName)
        if (daos.isNotEmpty()) {
          return daos
        }
        Thread.sleep(1000)
        NetworkRefresh.runNetwork()
      }
      throw RuntimeException("Unable to get daos from node - are you sure this is a race condition?")
    }

    fun assertDaoStateContainsMembers(daoStates: List<DaoState>, vararg parties: Party) {

      parties.forEach {
        Assert.assertTrue("members should include party: $it", daoStates.first().members.contains(it))
      }
    }

    fun DoDaoStatesContainMembers(daoStates: List<DaoState>, vararg parties: Party) : Boolean {

      val allMembers = daoStates.flatMap { it.members }.distinct()

      parties.forEach {
        if(!allMembers.contains(it)) return false
      }
      return true
    }

    fun addMemberToDao(newMemberNode: TestNode, proposerNode: TestNode, daoName: String, dontCareIfTheyAlreadyExist: Boolean, vararg extraSigners: TestNode) {

      if(!dontCareIfTheyAlreadyExist){
        addMemberToDao(newMemberNode, daoName, proposerNode, extraSigners)
      }
      else{
        if(!DoDaoStatesContainMembers(getDaoWithRetry(proposerNode,daoName),newMemberNode.party)) {
          addMemberToDao(newMemberNode, daoName, proposerNode, extraSigners)
        }
      }
      assertDaoStateContainsMembers(getDaoWithRetry(newMemberNode, daoName,50), proposerNode.party, newMemberNode.party, *extraSigners.map { it.party }.toTypedArray())
    }

    private fun addMemberToDao(newMemberNode: TestNode, daoName: String, proposerNode: TestNode, extraSigners: Array<out TestNode>) {
      val proposal = NetworkRefresh.runWithRefresh { newMemberNode.daoApi.createNewMemberProposal(daoName, proposerNode.party.name) }

      Assert.assertEquals("should be 1 supporters", 1, proposal.supporters.size)
      Assert.assertTrue("member should be a supporter", proposal.supporters.contains(newMemberNode.party))

      var numberOfSupporters = 1
      extraSigners.forEach {
        NetworkRefresh.runWithRefresh { it.daoApi.voteForProposal(proposal.proposal.key()) }
        val postVote = it.daoApi.proposalFor(proposal.proposal.key())
        Assert.assertEquals("there should be three supporters", ++numberOfSupporters, postVote.supporters.size)
      }

      // accept member proposal
      val acceptedProposalLifecycle = NetworkRefresh.runWithRefresh { newMemberNode.daoApi.sponsorAcceptProposal(proposal.proposal.key(), proposal.daoKey, proposerNode.party.name) }
      Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposalLifecycle)
    }

    /*
<<<<<<< HEAD
=======

    fun createAndAcceptProposalWithName(proposalName: String, proposalProposer: RemoteTestNode, daoState: DaoState, vararg supporters: RemoteTestNode) {
      val proposal = createNormalProposal(proposalName, proposalProposer, daoState)

      supporters.forEach {
        it.daoApi.voteForNormalProposal(proposal.proposal.proposalKey).getOrThrow()
      }

      val proposals = proposalProposer.daoApi.normalProposalsFor(daoState.daoKey)

      Assert.assertEquals("there should only be one proposal", 1, proposals.size)
      Assert.assertEquals("the proposal should have ${supporters.size + 1} supporters", supporters.size + 1, proposals.first().supporters.size)
      Assert.assertTrue("the proposal should have all supporters", proposals.first().supporters.containsAll(setOf(proposalProposer.party, *supporters.map { it.party }.toTypedArray())))

      val acceptedProposal = proposalProposer.daoApi.acceptNormalProposal(proposal.proposal.proposalKey).getOrThrow()
      Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)
    }

>>>>>>> master
    */
    fun createProposal(proposalName: String, proposalProposer: TestNode, daoState: DaoState): ProposalState<NormalProposal> {
      val proposal = proposalProposer.daoApi.createNormalProposal(proposalName, "some description", daoState.daoKey).getOrThrow()

      val origProposals = proposalProposer.daoApi.normalProposalsFor(daoState.daoKey)
      Assert.assertEquals("there should only be one proposal", 1, origProposals.size)
      Assert.assertEquals("the proposal should have one supporter", 1, origProposals.first().supporters.size)
      Assert.assertTrue("proposer should be supporter", origProposals.first().supporters.containsAll(setOf(proposalProposer.party)))
      Assert.assertEquals("keys should be the same", proposal.proposal.proposalKey, origProposals.first().proposal.proposalKey)
      return proposal
    }
  }
}