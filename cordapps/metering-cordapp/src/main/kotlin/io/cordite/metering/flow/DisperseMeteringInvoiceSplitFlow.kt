/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.token.TokenType
import io.cordite.dgl.flows.token.flows.TransferTokenSenderFunctions
import io.cordite.metering.contract.*
import io.cordite.metering.schema.MeteringInvoiceSplitSchemaV1
import net.corda.core.contracts.*
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.security.PublicKey

object DisperseMeteringInvoiceSplitFlow {

  private val log: Logger = LoggerFactory.getLogger(DisperseMeteringInvoiceSplitFlow.javaClass)


  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class MeteringInvoiceSplitDisperser(val meteringInvoiceSplitState: MeteringInvoiceSplitState, val sourceAccount: String) : FlowLogic<SignedTransaction>() {
    companion object {
      object BUILD_METERING_INVOICE_TRANSACTION : Step("Build the metering invoice transaction")
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
      BUILD_METERING_INVOICE_TRANSACTION,
      VERIFYING_TRANSACTION,
      SIGNING_TRANSACTION,
      GATHERING_SIGS,
      FINALISING_TRANSACTION
    )

    @Suspendable
    override fun call(): SignedTransaction {

      val originalMeteringInvoiceSplit = getOriginalMeteringInvoiceSplit()
      val txBuilder = buildTransaction(originalMeteringInvoiceSplit)
      val tokenSiginingKeys = addPaymentTransaction(originalMeteringInvoiceSplit.state.data.meteringInvoiceSplitProperties, txBuilder)

      val partiallySignedTx = verifyAndSignTransaction(txBuilder, tokenSiginingKeys + serviceHub.myInfo.legalIdentities.first().owningKey)
      val signatureParties = getSignatureParties(originalMeteringInvoiceSplit)
      val fullySignedTx = getSignatureFromReceiver(partiallySignedTx, signatureParties)
      val finalisedTx = finaliseTransaction(fullySignedTx)
      return finalisedTx
    }

    fun buildTransaction(originalMeteringInvoiceSplitStateAndRef: StateAndRef<MeteringInvoiceSplitState>): TransactionBuilder {
      progressTracker.currentStep = BUILD_METERING_INVOICE_TRANSACTION

      val originalMeteringInvoiceSplitProperties = originalMeteringInvoiceSplitStateAndRef.state.data.meteringInvoiceSplitProperties

      val newMeteringInvoiceSplitProperties = originalMeteringInvoiceSplitProperties.copy(meteringSplitState = MeteringSplitState.SPLIT_DISPERSED)

      val notary = originalMeteringInvoiceSplitStateAndRef.state.notary
      val newMeteringInvoiceStateSplit = MeteringInvoiceSplitState(newMeteringInvoiceSplitProperties, newMeteringInvoiceSplitProperties.finalParty)
      val txCommand = Command(MeteringInvoiceCommands.DisperseFunds(), newMeteringInvoiceStateSplit.participants.map { it.owningKey })

      val txBuilder = TransactionBuilder(notary).withItems(StateAndContract(newMeteringInvoiceStateSplit, MeteringInvoiceContract.METERING_CONTRACT_ID), txCommand)
      txBuilder.addInputState(originalMeteringInvoiceSplitStateAndRef)

      return txBuilder
    }

    fun addPaymentTransaction(meteringInvoiceSplitProperties: MeteringInvoiceSplitProperties, txBuilder: TransactionBuilder): List<PublicKey> {
      val fromAddress = AccountAddress(sourceAccount, meteringInvoiceSplitProperties.daoParty.name)
      val toAddress = AccountAddress(meteringInvoiceSplitProperties.finalAccountId, meteringInvoiceSplitProperties.finalParty.name)
      val tokenType: TokenType.Descriptor = meteringInvoiceSplitProperties.tokenDescriptor;

      val amountInTokenType = Amount.fromDecimal(BigDecimal(meteringInvoiceSplitProperties.amount), tokenType)
      val tokenSigingKeys = TransferTokenSenderFunctions.prepareTokenMoveWithSummary(txBuilder, fromAddress, toAddress, amountInTokenType, serviceHub, ourIdentity, "metering split flow payment")
      return tokenSigingKeys
    }

    fun getSignatureParties(originalMeteringInvoiceSplit: StateAndRef<MeteringInvoiceSplitState>): List<Party> {
      return listOf(originalMeteringInvoiceSplit.state.data.meteringInvoiceSplitProperties.finalParty)
    }

    private fun getOriginalMeteringInvoiceSplit(): StateAndRef<MeteringInvoiceSplitState> {
      val result = builder {
        val expression = MeteringInvoiceSplitSchemaV1.PersistentMeteringInvoiceSplit::splitInvoiceId.equal(meteringInvoiceSplitState.meteringInvoiceSplitProperties.splitInvoiceId)
        val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
        val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
        val fullCriteria = generalCriteria.and(customCriteria)
        serviceHub.vaultService.queryBy<MeteringInvoiceSplitState>(fullCriteria).states.last()
      }
      return result
    }

    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder, signingPubKeys: Iterable<PublicKey>): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      val partiallySignedTx = serviceHub.signInitialTransaction(txBuilder, signingPubKeys.distinct())
      return partiallySignedTx
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction, parties: List<Party>): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS

      val flowSessions = parties.map { initiateFlow(it) }

      val fullySignedTx = subFlow(CollectSignaturesFlow(partiallySignedTx, flowSessions.toSet(), GATHERING_SIGS.childProgressTracker()))
      return fullySignedTx
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      val finalisedTx = subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
      return finalisedTx
    }
  }

  @InitiatedBy(MeteringInvoiceSplitDisperser::class)
  class MeteringInvoiceSplitDispersee(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        override fun checkTransaction(stx: SignedTransaction) = requireThat {
          //TODO - add check for payment and that it matches split amount https://gitlab.com/cordite/cordite/issues/275
          val output = stx.tx.outputs.filter { it.data is MeteringInvoiceSplitState }.single().data
          val meteringInvoiceSplitState = output as MeteringInvoiceSplitState
          "check that this this is a valid transaction we are being metered for" using (MeteringInvoiceFlowChecks.isTheMeteredTransactionValid(meteringInvoiceSplitState, serviceHub, log))
        }
      }
      return subFlow(signTransactionFlow)
    }
  }
}
