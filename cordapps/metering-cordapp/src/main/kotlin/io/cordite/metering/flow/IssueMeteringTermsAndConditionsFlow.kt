/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.metering.contract.MeteringTermsAndConditionsCommands
import io.cordite.metering.contract.MeteringTermsAndConditionsContract
import io.cordite.metering.contract.MeteringTermsAndConditionsProperties
import io.cordite.metering.contract.MeteringTermsAndConditionsState
import net.corda.core.contracts.Command
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import java.security.PublicKey

object IssueMeteringTermsAndConditionsFlow {
  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class TermsAndConditionsIssuer(val termsAndConditionsProperties: MeteringTermsAndConditionsProperties, val doNotSend: Boolean = false) : FlowLogic<SignedTransaction>() {

    companion object {
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
        VERIFYING_TRANSACTION,
        SIGNING_TRANSACTION,
        GATHERING_SIGS,
        FINALISING_TRANSACTION
    )

    @Suspendable
    override fun call(): SignedTransaction {
      if (!doNotSend && termsAndConditionsProperties.guardianNotaryParty == termsAndConditionsProperties.meteredParty) {
        throw FlowException("Cannot issue a Terms and Conditions to a guardian notary that you're using to notarise the issuance.")
      }
      if (serviceHub.networkMapCache.getNodeByLegalIdentity(termsAndConditionsProperties.meteredParty) == null) {
        throw FlowException("Cannot issue a Terms and Conditions to a node that we can't find in the network map.")
      }

      val termsAndConditionsProposal = createTermsAndConditionsProposal(termsAndConditionsProperties)
      val termsAndConditionsTxBuilder = issuanceTxBuilder(termsAndConditionsProposal)
      val partiallySignedTransaction = verifyAndSignTransaction(termsAndConditionsTxBuilder, listOf(serviceHub.myInfo.legalIdentities.first().owningKey))

      return if (doNotSend) {
        serviceHub.recordTransactions(listOf(partiallySignedTransaction))
        partiallySignedTransaction
      } else {
        val fullySignedTransaction = getSignatureFromReceiver(partiallySignedTransaction, getSignatureParties(termsAndConditionsProposal))
        finaliseTransaction(fullySignedTransaction)
      }
    }

    private fun createTermsAndConditionsProposal(termsAndConditionsProperties: MeteringTermsAndConditionsProperties): MeteringTermsAndConditionsState {
      return MeteringTermsAndConditionsState(termsAndConditionsProperties, termsAndConditionsProperties.meteredParty)
    }

    private fun issuanceTxBuilder(issuance: MeteringTermsAndConditionsState): TransactionBuilder {
      val txCommand = Command(MeteringTermsAndConditionsCommands.Issue(), getSignatureParties(issuance).map { it.owningKey })

      return TransactionBuilder(notary = issuance.meteringTermsAndConditionsProperties.guardianNotaryParty)
          .withItems(txCommand)
          .addOutputState(issuance, MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)
    }

    private fun getSignatureParties(issuance: MeteringTermsAndConditionsState): List<Party> {
      return issuance.participants
          .filter { it !in serviceHub.myInfo.legalIdentities }
          .map { serviceHub.identityService.requireWellKnownPartyFromAnonymous(it) }
    }

    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder, signingPubKeys: Iterable<PublicKey>): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      return serviceHub.signInitialTransaction(txBuilder, signingPubKeys.distinct())
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction, parties: List<AbstractParty>): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS

      val flowSessions = parties.map { initiateFlow(serviceHub.identityService.requireWellKnownPartyFromAnonymous(it)) }
      return subFlow(CollectSignaturesFlow(partiallySignedTx, flowSessions.toSet(), GATHERING_SIGS.childProgressTracker()))
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      return subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
    }
  }

  @InitiatedBy(TermsAndConditionsIssuer::class)
  class TermsAndConditionsIssuerReceiver(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        override fun checkTransaction(stx: SignedTransaction): Unit = requireThat {
          val output = stx.tx.outputs.single().data
          "This must be a MeteringTermsAndConditions Transaction." using (output is MeteringTermsAndConditionsState)
          val meteringTermsAndConditionsState = output as MeteringTermsAndConditionsState
          "Check that the notary sending this to me is the one actually named as the meterer." using (meteringTermsAndConditionsState.meteringTermsAndConditionsProperties.meteringParty == otherPartyFlow.counterparty)
          "The counter party must actually be a notary" using (otherPartyFlow.counterparty in serviceHub.networkMapCache.notaryIdentities)
        }
      }
      return subFlow(signTransactionFlow)
    }
  }
}
