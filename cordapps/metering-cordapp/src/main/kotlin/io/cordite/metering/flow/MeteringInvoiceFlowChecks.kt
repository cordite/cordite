/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import io.cordite.metering.contract.MeteringInvoiceSplitState
import io.cordite.metering.contract.MeteringInvoiceState
import net.corda.core.contracts.requireThat
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import org.slf4j.Logger

class MeteringInvoiceFlowChecks {

  companion object {
    fun isTheMeteredTransactionValid(meteringInvoiceState: MeteringInvoiceState, serviceHub: ServiceHub, log: Logger): Boolean {
      requireThat {
        "transaction id has been populated" using (meteringInvoiceState.meteringInvoiceProperties.meteredTransactionId.isNotBlank())
      }
      //lookup transaction in database to see if it's actually ours
      log.info("metering invoice state: $meteringInvoiceState")

      // if this invoice is not for me (e.g. I'm involved in this flow as the Dao), just ignore this test and return true
      if (!serviceHub.myInfo.legalIdentities.contains(meteringInvoiceState.meteringInvoiceProperties.invoicedParty)) {
        log.info("ignoring metering invoice is not me. ")
        return true;
      }

      // lookup the metered transation in my database to see if it's actually mine
      val txId: SecureHash = SecureHash.parse(meteringInvoiceState.meteringInvoiceProperties.meteredTransactionId)
      val tx = serviceHub.validatedTransactions.getTransaction(txId)
      log.info("Original metered transaction was " + if (tx != null) "found" else "not found")
      val notary: Party? = tx?.notary
      log.info("Original metered transaction was notarised by: " + if (notary != null) notary else "unknown")

      // check that the notary on the orginal transaction is the same as the notary that's invoicing me for for it!
      return (notary ?: "").equals(meteringInvoiceState.meteringInvoiceProperties.invoicingNotary)
    }

    fun isTheMeteredTransactionValid(meteringInvoiceSplitState: MeteringInvoiceSplitState, serviceHub: ServiceHub, log: Logger): Boolean {
      requireThat {
        "transaction id has been populated" using (meteringInvoiceSplitState.meteringInvoiceSplitProperties.meteredTransactionId.isNotBlank())
      }
      //lookup transaction in database to see if it's actually ours

      // if this invoice is not for me (e.g. I'm involved in this flow as the Dao), just ignore this test and return true
      if (!serviceHub.myInfo.legalIdentities.contains(meteringInvoiceSplitState.meteringInvoiceSplitProperties.invoicedParty)) {
        return true;
      }

      // lookup the metered transation in my database to see if it's actually mine
      val txId: SecureHash = SecureHash.parse(meteringInvoiceSplitState.meteringInvoiceSplitProperties.meteredTransactionId)
      val tx = serviceHub.validatedTransactions.getTransaction(txId)
      log.info("Original metered transaction was " + if (tx != null) "found" else "not found")
      val notary: Party? = tx?.notary
      log.info("Original metered transaction was notarised by: " + if (notary != null) notary else "unknown")

      // check that the notary on the orginal transaction is the same as the notary that's invoicing me for for it!
      return (notary ?: "").equals(meteringInvoiceSplitState.meteringInvoiceSplitProperties.invoicingNotary)
    }

  }
}