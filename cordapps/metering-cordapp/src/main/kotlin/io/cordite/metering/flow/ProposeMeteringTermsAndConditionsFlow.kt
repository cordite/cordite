/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.contract.v1.account.Account
import io.cordite.dgl.contract.v1.account.AccountAddress
import io.cordite.dgl.contract.v1.token.TokenType
import io.cordite.metering.contract.MeteringTermsAndConditionsCommands
import io.cordite.metering.contract.MeteringTermsAndConditionsContract
import io.cordite.metering.contract.MeteringTermsAndConditionsProperties
import io.cordite.metering.contract.MeteringTermsAndConditionsState
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import net.corda.core.utilities.unwrap
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.security.PublicKey

object ProposeMeteringTermsAndConditionsFlow {
  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class TermsAndConditionsProposer(val lastTransaction: StateAndRef<MeteringTermsAndConditionsState>, val termsAndConditionsProperties: MeteringTermsAndConditionsProperties) : FlowLogic<SignedTransaction>() {

    companion object {
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
        VERIFYING_TRANSACTION,
        SIGNING_TRANSACTION,
        GATHERING_SIGS,
        FINALISING_TRANSACTION
    )

    @Suspendable
    override fun call(): SignedTransaction {
      if (termsAndConditionsProperties.guardianNotaryParty == termsAndConditionsProperties.meteredParty) {
        throw FlowException("Cannot propose a Terms and Conditions to a guardian notary that you're using to notarise the proposal.")
      }

      val termsAndConditionsProposal = createTermsAndConditionsProposal(termsAndConditionsProperties)
      val termsAndConditionsTxBuilder = proposalTxBuilder(lastTransaction, termsAndConditionsProposal)
      val partiallySignedTransaction = verifyAndSignTransaction(termsAndConditionsTxBuilder, listOf(serviceHub.myInfo.legalIdentities.first().owningKey))
      val fullySignedTransaction = getSignatureFromReceiver(partiallySignedTransaction, getSignatureParties(termsAndConditionsProposal))
      return finaliseTransaction(fullySignedTransaction)
    }

    private fun createTermsAndConditionsProposal(termsAndConditionsProperties: MeteringTermsAndConditionsProperties): MeteringTermsAndConditionsState {
      return MeteringTermsAndConditionsState(termsAndConditionsProperties, termsAndConditionsProperties.meteredParty)
    }

    private fun proposalTxBuilder(lastTransaction: StateAndRef<MeteringTermsAndConditionsState>, proposal: MeteringTermsAndConditionsState): TransactionBuilder {
      val txCommand = Command(MeteringTermsAndConditionsCommands.Propose(), getSignatureParties(proposal).map { it.owningKey })

      val txBuilder = TransactionBuilder(notary = proposal.meteringTermsAndConditionsProperties.guardianNotaryParty).withItems(txCommand)
      txBuilder.addInputState(lastTransaction)
      txBuilder.addOutputState(proposal, MeteringTermsAndConditionsContract.METERING_T_AND_C_CONTRACT_ID)

      return txBuilder
    }

    private fun getSignatureParties(proposal: MeteringTermsAndConditionsState): List<Party> {
      if (proposal.meteringTermsAndConditionsProperties.guardianNotaryParty in proposal.participants) {
        // Defensive; shouldn't happen.
        throw FlowException("Tried to get signatures for a Metering T&C Proposal where guardian notary is a participant.")
      }
      return proposal.participants
          .filter { it !in serviceHub.myInfo.legalIdentities }
          .map { serviceHub.identityService.requireWellKnownPartyFromAnonymous(it) }
    }

    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder, signingPubKeys: Iterable<PublicKey>): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      return serviceHub.signInitialTransaction(txBuilder, signingPubKeys.distinct())
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction, parties: List<AbstractParty>): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS

      val flowSessions = parties.map { initiateFlow(serviceHub.identityService.requireWellKnownPartyFromAnonymous(it)) }
      return subFlow(CollectSignaturesFlow(partiallySignedTx, flowSessions.toSet(), GATHERING_SIGS.childProgressTracker()))
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      return subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
    }

    @Suspendable
    private fun checkAccountExists(accountId: String, accountParty: Party): Boolean {
      return subFlow(FindRemoteAccountFlow.RemoteAccountFindRequester(AccountAddress(accountId, accountParty.name)))
    }
  }

  @InitiatedBy(TermsAndConditionsProposer::class)
  class TermsAndConditionsProposerReceiver(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        @Suspendable
        override fun checkTransaction(stx: SignedTransaction): Unit = requireThat {
          val output = stx.tx.outputs.single().data
          "This must be a MeteringTermsAndConditions Transaction." using (output is MeteringTermsAndConditionsState)
          val meteringTermsAndConditionsState = output as MeteringTermsAndConditionsState
          "Check that the node sending this to me is the one actually named as the meteredParty." using (meteringTermsAndConditionsState.meteringTermsAndConditionsProperties.meteredParty == otherPartyFlow.counterparty)
        }
      }
      return subFlow(signTransactionFlow)
    }

    @Suspendable
    private fun checkAccountExists(accountId: String, accountParty: Party): Boolean {
      return subFlow(FindRemoteAccountFlow.RemoteAccountFindRequester(AccountAddress(accountId, accountParty.name)))
    }
  }

  @Suspendable
  private fun findTokenType(descriptor: TokenType.Descriptor, serviceHub: ServiceHub): List<StateAndRef<TokenType.State>> {
    // Unlike checkAccountExists, we don't bother asking the remote if the account exists because we need to be aware of the token too.
    val criteria = builder {
      val bySymbol = QueryCriteria.VaultCustomQueryCriteria(TokenType.TokenTypeSchemaV1.PersistedTokenType::symbol.equal(descriptor.symbol))
      val byExponent = QueryCriteria.VaultCustomQueryCriteria(TokenType.TokenTypeSchemaV1.PersistedTokenType::exponent.equal(descriptor.exponent))

      QueryCriteria.AndComposition(bySymbol, byExponent)
    }
    return serviceHub.vaultService.queryBy<TokenType.State>(criteria).states
  }
}

object FindRemoteAccountFlow {
  private val log: Logger = LoggerFactory.getLogger(FindRemoteAccountFlow.javaClass)

  @InitiatingFlow
  class RemoteAccountFindRequester(private val address: AccountAddress) : FlowLogic<Boolean>() {
    @Suspendable
    override fun call(): Boolean {
      val recipient = serviceHub.networkMapCache.getNodeByLegalName(address.party)?.legalIdentities?.first()
          ?: return false
      return if (!serviceHub.myInfo.isLegalIdentity(recipient)) {
        val session = initiateFlow(recipient)
        val response = session.sendAndReceive<String>(address).unwrap { log.info("tried to unpack"); it }
        response == "OK"
      } else {
        Account.exists(serviceHub, address)
      }
    }
  }

  @InitiatedBy(RemoteAccountFindRequester::class)
  class RemoteAccountFindReceiver(val otherPartyFlow: FlowSession) : FlowLogic<Boolean>() {
    @Suspendable
    override fun call(): Boolean {
      val address = otherPartyFlow.receive<AccountAddress>().unwrap { it }
      return if (!Account.exists(serviceHub, address)) {
        otherPartyFlow.send("NOT OK")
        false
      } else {
        otherPartyFlow.send("OK")
        true
      }
    }
  }
}
