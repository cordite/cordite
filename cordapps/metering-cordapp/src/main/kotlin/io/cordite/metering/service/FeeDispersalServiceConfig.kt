/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import com.fasterxml.jackson.annotation.JsonIgnore
import io.cordite.commons.utils.Resources
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import net.corda.core.utilities.loggerFor

const val FEE_DISPERSAL_SERVICE_CONFIG_FILENAME = "fee-dispersal-service-config.json"
const val DEFAULT_FEE_DISPERSAL_REFRESH_INTERVAL = 2000L

data class FeeDispersalServiceConfig(val feeDispersalRefreshInterval: Long = DEFAULT_FEE_DISPERSAL_REFRESH_INTERVAL,
                                     val feeDispersalServicePartyName: String = "",
                                     val daoName: String ="") {
  companion object {
    private fun getConfigFromFile(feeDispersalServiceConfigFileName: String): JsonObject {
      try {
        JsonObject(Resources.loadResourceAsString(feeDispersalServiceConfigFileName)).let {
          log.info("found fee dispersal service config file $feeDispersalServiceConfigFileName")
          return it
        }
      } catch (_: Throwable) {
        log.warn("could not find the fee dispersal service config file $DEFAULT_FEE_DISPERSAL_REFRESH_INTERVAL")
        return JsonObject()
      }
    }

    @JsonIgnore
    private val log = loggerFor<FeeDispersalServiceConfig>()

    fun loadConfig(feeDispersalServiceConfigFileName: String = FEE_DISPERSAL_SERVICE_CONFIG_FILENAME): FeeDispersalServiceConfig {

      val configFromFile = getConfigFromFile(feeDispersalServiceConfigFileName)
      return Json.decodeValue<FeeDispersalServiceConfig>(configFromFile.encode(), FeeDispersalServiceConfig::class.java)
    }
  }
}
