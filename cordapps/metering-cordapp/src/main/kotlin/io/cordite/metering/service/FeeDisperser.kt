/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import io.cordite.commons.utils.transaction
import io.cordite.metering.contract.MeteringInvoiceState
import io.cordite.metering.contract.MeteringState
import io.cordite.metering.daostate.MeteringModelData
import io.cordite.metering.flow.DisperseMeteringInvoiceFundsFlow
import io.cordite.metering.flow.MeteringInvoiceFlowCommands
import io.cordite.metering.schema.MeteringInvoiceSchemaV1.PersistentMeteringInvoice
import net.corda.core.contracts.StateAndRef
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.vault.Builder.equal
import net.corda.core.node.services.vault.QueryCriteria.VaultCustomQueryCriteria
import net.corda.core.utilities.getOrThrow
import net.corda.core.utilities.loggerFor

class FeeDisperser(val serviceHub: AppServiceHub, feeDispersalServiceConfig: FeeDispersalServiceConfig) {

  private val meteringDaoStateLoader = MeteringDaoStateLoader(serviceHub, feeDispersalServiceConfig.daoName)
  private val log = loggerFor<AbstractMeterer>()
  private lateinit var meteringModelData: MeteringModelData

  init {
    log.info("Fee Disperser Created")
  }

  fun disperseFeesForPaidInvoices() {
    if (!weAreReadyToDisperseFees()) {
      return
    }
    serviceHub.transaction {
      val paidInvoices = getPaidMeteringInvoices()
      log.info("found ${paidInvoices.size} paid invoices")
      paidInvoices.forEach {
        val meteringInvoice = it.state.data
        val disperseeMeteringInvoiceRequest = MeteringInvoiceFlowCommands.DisperseFundsForMeteringInvoiceRequest(meteringInvoice.meteringInvoiceProperties.meteredTransactionId)
        val disperseMeteringInvoiceFlow = DisperseMeteringInvoiceFundsFlow.MeteringInvoiceFundDisperser(
          disperseFundsForMeteringInvoiceRequest = disperseeMeteringInvoiceRequest,
          meteringModelData = meteringModelData)
        val disperseMeteringInvoiceFlowFuture = serviceHub.startFlow(disperseMeteringInvoiceFlow).returnValue

        log.info("About to disperse PAID Metering Invoice ${meteringInvoice.meteringInvoiceProperties.meteredTransactionId}")
        val result = disperseMeteringInvoiceFlowFuture.getOrThrow()

        log.info("Successfully Dispersed Fees txid ${result.coreTransaction.id}")
      }
    }
  }

  private fun weAreReadyToDisperseFees(): Boolean {
    //TODO - note: this loads the dao state each time, but we could change it to listener to make this more efficient https://gitlab.com/cordite/cordite/issues/196
    val loadedMeteringModelData = meteringDaoStateLoader.getMeteringDao() ?: return false
    log.debug("We have a metering DaoState")
    meteringModelData = loadedMeteringModelData
    return true
  }

  private fun getPaidMeteringInvoices(): List<StateAndRef<MeteringInvoiceState>> {
    val qc = VaultCustomQueryCriteria(PersistentMeteringInvoice::meteringState.equal(MeteringState.PAID.name))
    return serviceHub.vaultService.queryBy(MeteringInvoiceState::class.java, qc).states
  }
}


