/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

import com.fasterxml.jackson.annotation.JsonSetter
import io.cordite.commons.utils.Resources
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import net.corda.core.identity.CordaX500Name
import net.corda.core.utilities.loggerFor


const val METERING_SERVICE_CONFIG_FILENAME = "metering-service-config.json"
const val DEFAULT_METERING_REFRESH_INTERVAL = 2000L
const val DEFAULT_MINIMUM_METERING_TRANSACTION_SIZE = 1
const val DEFAULT_MAXIMUM_METERING_TRANSACTION_SIZE = 1000
const val DEFAULT_METERING_TRANSACTION_WAIT = 86400000L // 24 hours

data class MeteringServiceConfig(val meteringRefreshInterval: Long = DEFAULT_METERING_REFRESH_INTERVAL,
                                 val meteringServiceType: MeteringServiceType = MeteringServiceType.SingleNodeMeterer,
                                 val daoPartyName: String = "",
                                 val meteringNotaryAccountId: String = "",
                                 val meteringPartyName: String = "",
                                 var meteringDefaults: MeteringTermsAndConditionsDefaults = MeteringTermsAndConditionsDefaults(),
                                 val daoName: String = "",
                                 val minMeteringTransactionSize: Int = DEFAULT_MINIMUM_METERING_TRANSACTION_SIZE,
                                 val maxMeteringTransactionSize: Int = DEFAULT_MAXIMUM_METERING_TRANSACTION_SIZE,
                                 val maxMeteringTransactionTimeWait: Long = DEFAULT_METERING_TRANSACTION_WAIT) {
  companion object {
    private fun getConfigFromFile(meteringServiceConfigFileName: String): JsonObject {
      try {
        JsonObject(Resources.loadResourceAsString(meteringServiceConfigFileName)).let {
          log.info("found metering config file $meteringServiceConfigFileName")
          log.info("using config: ")
          log.info(it.encodePrettily())
          return it
        }
      } catch (e: Throwable) {
        log.warn("could not find or could not load the metering config file $meteringServiceConfigFileName")
        log.warn("error presented was ${e.message}")
        return JsonObject()
      }
    }

    @JsonIgnore
    private val log = loggerFor<MeteringServiceConfig>()

    fun loadConfig(meteringServiceConfigFileName: String = METERING_SERVICE_CONFIG_FILENAME): MeteringServiceConfig {

      val configFromFile = getConfigFromFile(meteringServiceConfigFileName)
      return Json.decodeValue<MeteringServiceConfig>(configFromFile.encode(), MeteringServiceConfig::class.java)
    }
  }

  @JsonSetter("meteringDefaults")
  fun setMeteringDefaultsNeverNull(s: MeteringTermsAndConditionsDefaults?) {
    if (s != null) {
      this.meteringDefaults = s
    }
  }
}

enum class MeteringServiceType {
  SingleNodeMeterer,
  BftSmartMeterer,
  RaftMeterer
}

data class MeteringTermsAndConditionsDefaults(
    val billingTokenTypeUri: String = "",
    val transactionCost: Long = -1,
    val transactionCreditLimit: Long = -1,
    val freeTransactions: Long = -1,
    val transactionCostForNotaries: Long = -1,
    val transactionCreditLimitForNotaries: Long = -1,
    val freeTransactionsForNotaries: Long = -1,
    val payPartyName: CordaX500Name = CordaX500Name("FAKE", "FAKE", "GB"),
    val payAccount: String = "",
    val guardianNotaryName: CordaX500Name = CordaX500Name("FAKE", "FAKE", "GB"),
    val authorizedForZeroTC: Set<CordaX500Name> = setOf()
) {
  // I tried writing a Jackson deserializer for CordaX500Name and using @JsonDeserializer
  // for the CordaX500Name fields, but for some reason Jackson/Vertx seemed to ignore the
  // annotation. This way is easier.
  @JsonCreator
  constructor (
      @JsonProperty("billingTokenTypeUri")
      billingTokenTypeUri: String,
      @JsonProperty("transactionCost")
      transactionCost: Long,
      @JsonProperty("transactionCreditLimit")
      transactionCreditLimit: Long,
      @JsonProperty("freeTransactions")
      freeTransactions: Long,
      @JsonProperty("transactionCostForNotaries")
      transactionCostForNotaries: Long,
      @JsonProperty("transactionCreditLimitForNotaries")
      transactionCreditLimitForNotaries: Long,
      @JsonProperty("freeTransactionsForNotaries")
      freeTransactionsForNotaries: Long,
      @JsonProperty("payPartyName")
      payPartyName: String,
      @JsonProperty("payAccount")
      payAccount: String,
      @JsonProperty("guardianNotaryName")
      guardianNotaryName: String,
      @JsonProperty("authorizedForZeroTC")
      authorizedForZeroTC: List<String>
  ) : this(
      billingTokenTypeUri,
      transactionCost,
      transactionCreditLimit,
      freeTransactions,
      transactionCostForNotaries,
      transactionCreditLimitForNotaries,
      freeTransactionsForNotaries,
      CordaX500Name.parse(payPartyName),
      payAccount,
      CordaX500Name.parse(guardianNotaryName),
      authorizedForZeroTC.map { CordaX500Name.parse(it) }.toSet()
  )
}
