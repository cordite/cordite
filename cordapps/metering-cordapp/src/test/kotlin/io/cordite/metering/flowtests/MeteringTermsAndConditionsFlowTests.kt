/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flowtests

import io.cordite.braid.core.async.getOrThrow
import io.cordite.dao.proposal.VoteForProposalFlowResponder
import io.cordite.dgl.LedgerApi
import io.cordite.dgl.contract.v1.account.Account
import io.cordite.dgl.contract.v1.token.TokenType
import io.cordite.dgl.impl.LedgerApiImpl
import io.cordite.metering.contract.MeteringPerTransactionBillingType
import io.cordite.metering.contract.MeteringTermsAndConditionsProperties
import io.cordite.metering.contract.MeteringTermsAndConditionsState
import io.cordite.metering.contract.MeteringTermsAndConditionsStatus
import io.cordite.metering.flow.IssueMeteringTermsAndConditionsFlow
import io.cordite.metering.flow.ProposeMeteringTermsAndConditionsFlow
import io.cordite.metering.testutils.MeteringRunAndRetry.Companion.runAndRetryGeneric
import io.cordite.test.utils.TempHackedAppServiceHubImpl
import net.corda.core.crypto.generateKeyPair
import net.corda.core.flows.FlowException
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.utilities.getOrThrow
import net.corda.node.services.transactions.SimpleNotaryService
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkNotarySpec
import net.corda.testing.node.StartedMockNode
import org.junit.After
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

@Ignore
class MeteringTermsAndConditionsFlowTests {

  private lateinit var mockNet: MockNetwork
  private lateinit var meteringNotary: StartedMockNode
  private lateinit var guardianNotary: StartedMockNode
  private lateinit var anotherNotary: StartedMockNode
  private lateinit var centralBankNode: StartedMockNode
  private lateinit var sector1BankNode: StartedMockNode
  private lateinit var sector2BankNode: StartedMockNode
  private lateinit var daoNode: StartedMockNode
  private lateinit var centralBank: Party
  private lateinit var sector1Bank: Party
  private lateinit var sector2Bank: Party
  private lateinit var meteringNotaryParty: Party
  private lateinit var guardianNotaryParty: Party
  private lateinit var anotherNotaryParty: Party
  private lateinit var daoNodeParty: Party
  private lateinit var fakeParty: Party

  private lateinit var centralBankNodeLedger: LedgerApi
  private lateinit var sector1BankNodeLedger: LedgerApi
  private lateinit var sector2BankNodeLedger: LedgerApi
  private lateinit var meteringNotaryLedger: LedgerApi
  private lateinit var daoNodeLedger: LedgerApi
  private lateinit var guardianNotaryLedger: LedgerApi
  private lateinit var anotherNotaryLedger: LedgerApi

  private lateinit var centralBankAccountResult: Account.State
  private lateinit var sector1BankAccountResult: Account.State
  private lateinit var sector2BankAccountResult: Account.State
  private lateinit var meteringNotaryAccountResult: Account.State
  private lateinit var daoHoldingAccountResult: Account.State
  private lateinit var daoFoundationAccountResult: Account.State
  private lateinit var guardianNotaryAccountResult: Account.State
  private lateinit var guardianNotaryAccountOnDaoResult: Account.State


  private val centralBankX500Name = CordaX500Name("CentralBank", "London", "GB")
  private val sector1BankX500Name = CordaX500Name("Sector1Bank", "London", "GB")
  private val sector2BankX500Name = CordaX500Name("Sector2Bank", "London", "GB")

  private val meteringNotaryX500Name = CordaX500Name(commonName = SimpleNotaryService::class.java.name, organisation = "MeteringNotary", locality = "Argleton", country = "GB")
  private val guardianNotaryX500Name = CordaX500Name(commonName = SimpleNotaryService::class.java.name, organisation = "GuardianNotary", locality = "Argleton", country = "GB")
  private val anotherNotaryX500Name = CordaX500Name(commonName = SimpleNotaryService::class.java.name, organisation = "AnotherNotary", locality = "Argleton", country = "GB")

  private val daoX500Name = CordaX500Name("Dao", "London", "GB")
  private val fakeX500Name = CordaX500Name("Fake", "Fake", "GB")

  private val testCurrencyXTS = "XTS"
  private val centralBankAccount1 = "central-bank-account1"
  private val sector1BankAccount1 = "sector1-bank-account1"
  private val sector2BankAccount1 = "sector2-bank-account1"
  private val meteringNotaryAccount1 = "metering-notary-account1"
  private val daoHoldingAccount = "dao-holding-account"
  private val daoFoundationAccount = "dao-foundation-account"
  private val guardianNotaryAccount1 = "guardian-notary-account1"

  private val DEFAULT_TRANSACTION_COST = 15L
  private val DEFAULT_TRANSACTION_CREDIT_LIMIT = 15L
  private val DEFAULT_FREE_TRANSACTIONS = 5L

  @Before
  fun before() {

    /* WARNING !! - The list of cordapp packages here has been carefully handpicked and selected so that the Metering Service is not loaded,
    this allows for more isolated and predictable flow testing
    The metering service is tested (unsuprisingly) in the MeteringServiceTests
    In conclusion, if you just put 'io.cordite' in the packages below you'll undermine the whole philosophy of this test - just saying!
    */
    mockNet = MockNetwork(
        cordappPackages = listOf("net.corda.core.contracts", "io.cordite.token.contract", "io.cordite.token.contract.schema", "io.cordite.token.flow",
            "io.cordite.metering.contract", "io.cordite.metering.flow", "io.cordite.metering.schema", "io.cordite.dgl", "io.cordite.dao", "io.cordite.test.utils"),
        notarySpecs = listOf(MockNetworkNotarySpec(meteringNotaryX500Name, true),
            MockNetworkNotarySpec(anotherNotaryX500Name, true),
            MockNetworkNotarySpec(guardianNotaryX500Name, true)))

    guardianNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == guardianNotaryX500Name }
    guardianNotary.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
    guardianNotary.registerInitiatedFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposerReceiver::class.java)
    guardianNotary.registerInitiatedFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuerReceiver::class.java)
    guardianNotaryParty = guardianNotary.info.legalIdentities.first()

    anotherNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == anotherNotaryX500Name }
    anotherNotary.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
    anotherNotary.registerInitiatedFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposerReceiver::class.java)
    anotherNotary.registerInitiatedFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuerReceiver::class.java)
    anotherNotaryParty = anotherNotary.info.legalIdentities.first()

    meteringNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == meteringNotaryX500Name }
    meteringNotary.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
    meteringNotary.registerInitiatedFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposerReceiver::class.java)
    meteringNotary.registerInitiatedFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuerReceiver::class.java)
    meteringNotaryParty = meteringNotary.info.legalIdentities.first()

    centralBankNode = mockNet.createPartyNode(legalName = centralBankX500Name)
    centralBankNode.registerInitiatedFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposerReceiver::class.java)
    centralBankNode.registerInitiatedFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuerReceiver::class.java)
    centralBank = centralBankNode.info.legalIdentities.first()

    sector1BankNode = mockNet.createPartyNode(legalName = sector1BankX500Name)
    sector1BankNode.registerInitiatedFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposerReceiver::class.java)
    sector1BankNode.registerInitiatedFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuerReceiver::class.java)
    sector1Bank = sector1BankNode.info.legalIdentities.first()

    sector2BankNode = mockNet.createPartyNode(legalName = sector2BankX500Name)
    sector2BankNode.registerInitiatedFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposerReceiver::class.java)
    sector2BankNode.registerInitiatedFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuerReceiver::class.java)
    sector2Bank = sector2BankNode.info.legalIdentities.first()

    daoNode = mockNet.createPartyNode(legalName = daoX500Name)
    daoNode.registerInitiatedFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposerReceiver::class.java)
    daoNode.registerInitiatedFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuerReceiver::class.java)
    daoNodeParty = daoNode.info.legalIdentities.first()

    fakeParty = Party(fakeX500Name, generateKeyPair().public)

    centralBankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(centralBankNode), { mockNet.runNetwork() })
    sector1BankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(sector1BankNode), { mockNet.runNetwork() })
    sector2BankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(sector2BankNode), { mockNet.runNetwork() })
    meteringNotaryLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(meteringNotary), { mockNet.runNetwork() })
    daoNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(daoNode), { mockNet.runNetwork() })
    guardianNotaryLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(guardianNotary), { mockNet.runNetwork() })
    anotherNotaryLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(anotherNotary), { mockNet.runNetwork() })

    centralBankAccountResult = centralBankNodeLedger.createAccount(centralBankAccount1, meteringNotaryX500Name).getOrThrow()
    assert(centralBankAccountResult.address.accountId == centralBankAccount1)
    sector1BankAccountResult = sector1BankNodeLedger.createAccount(sector1BankAccount1, meteringNotaryX500Name).getOrThrow()
    assert(sector1BankAccountResult.address.accountId == sector1BankAccount1)
    sector2BankAccountResult = sector2BankNodeLedger.createAccount(sector2BankAccount1, meteringNotaryX500Name).getOrThrow()
    assert(sector2BankAccountResult.address.accountId == sector2BankAccount1)
    meteringNotaryAccountResult = meteringNotaryLedger.createAccount(meteringNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(meteringNotaryAccountResult.address.accountId == meteringNotaryAccount1)
    daoHoldingAccountResult = daoNodeLedger.createAccount(daoHoldingAccount, meteringNotaryX500Name).getOrThrow()
    assert(daoHoldingAccountResult.address.accountId == daoHoldingAccount)
    daoFoundationAccountResult = daoNodeLedger.createAccount(daoFoundationAccount, meteringNotaryX500Name).getOrThrow()
    assert(daoFoundationAccountResult.address.accountId == daoFoundationAccount)
    guardianNotaryAccountResult = guardianNotaryLedger.createAccount(guardianNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(guardianNotaryAccountResult.address.accountId == guardianNotaryAccount1)
    guardianNotaryAccountOnDaoResult = daoNodeLedger.createAccount(guardianNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(guardianNotaryAccountOnDaoResult.address.accountId == guardianNotaryAccount1)

    daoNodeLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()
    meteringNotaryLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()
    anotherNotaryLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()
    sector1BankNodeLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()
    sector2BankNodeLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()
  }

  @Test
  fun `we can issue T&Cs from a notary to a node`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))

    mockNet.runNetwork()
    val lastTermsAndConditions = getMostRecentTermsAndConditions(sector1BankNode)
    val billingType = lastTermsAndConditions.meteringTermsAndConditionsProperties.billingType
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteringParty, meteringNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteredParty, sector1Bank)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.billingToken, TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name))
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payParty, daoNodeParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.guardianNotaryParty, guardianNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payAccountId, daoHoldingAccount)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.createdDateTime, creationTime)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.status, MeteringTermsAndConditionsStatus.ISSUED)
    assertEquals(billingType.transactionCost, DEFAULT_TRANSACTION_COST)
    assertEquals(billingType.transactionCreditLimit, DEFAULT_TRANSACTION_CREDIT_LIMIT)
    assertEquals(billingType.freeTransactions, DEFAULT_FREE_TRANSACTIONS)
  }

  @Test
  fun `we can propose T&Cs from a node to a notary`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val counterTC = dummyTC.copy(
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 140,
            transactionCreditLimit = 141,
            freeTransactions = 142
        ),
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val issueFuture = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val outStates = issueFuture.getOrThrow().coreTransaction.outRefsOfType<MeteringTermsAndConditionsState>()
    sector1BankNode.startFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposer(outStates.single(), counterTC))
    mockNet.runNetwork()

    val lastTermsAndConditions = getMostRecentTermsAndConditions(meteringNotary)
    val billingType = lastTermsAndConditions.meteringTermsAndConditionsProperties.billingType
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteringParty, meteringNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteredParty, sector1Bank)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.billingToken, TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name))
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payParty, daoNodeParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.guardianNotaryParty, guardianNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payAccountId, daoHoldingAccount)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.createdDateTime, creationTime)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.status, MeteringTermsAndConditionsStatus.PROPOSED)
    assertEquals(billingType.transactionCost, 140)
    assertEquals(billingType.transactionCreditLimit, 141)
    assertEquals(billingType.freeTransactions, 142)
  }


  @Test
  fun `we can propose T&Cs from a non-guardian notary to a notary`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = anotherNotaryParty,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val counterTC = dummyTC.copy(
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 140,
            transactionCreditLimit = 141,
            freeTransactions = 142
        ),
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val issueFuture = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val outStates = issueFuture.getOrThrow().coreTransaction.outRefsOfType<MeteringTermsAndConditionsState>()
    anotherNotary.startFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposer(outStates.single(), counterTC))
    mockNet.runNetwork()
    val lastTermsAndConditions = getMostRecentTermsAndConditions(anotherNotary)
    val billingType = lastTermsAndConditions.meteringTermsAndConditionsProperties.billingType
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteringParty, meteringNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteredParty, anotherNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.billingToken, TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name))
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payParty, daoNodeParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.guardianNotaryParty, guardianNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payAccountId, daoHoldingAccount)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.createdDateTime, creationTime)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.status, MeteringTermsAndConditionsStatus.PROPOSED)
    assertEquals(billingType.transactionCost, 140)
    assertEquals(billingType.transactionCreditLimit, 141)
    assertEquals(billingType.freeTransactions, 142)
  }

  @Test
  fun `we can issue T&Cs from a notary to another non-guardian notary`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = anotherNotaryParty,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val lastTermsAndConditions = getMostRecentTermsAndConditions(anotherNotary)
    val billingType = lastTermsAndConditions.meteringTermsAndConditionsProperties.billingType
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteringParty, meteringNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteredParty, anotherNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.billingToken, TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name))
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payParty, daoNodeParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.guardianNotaryParty, guardianNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payAccountId, daoHoldingAccount)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.createdDateTime, creationTime)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.status, MeteringTermsAndConditionsStatus.ISSUED)
    assertEquals(billingType.transactionCost, DEFAULT_TRANSACTION_COST)
    assertEquals(billingType.transactionCreditLimit, DEFAULT_TRANSACTION_CREDIT_LIMIT)
    assertEquals(billingType.freeTransactions, DEFAULT_FREE_TRANSACTIONS)
  }

  @Test
  fun `we can't propose metering T&Cs to a party different from the one we first spoke to`() {
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = Instant.now(),
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val counterTC = dummyTC.copy(
        meteredParty = anotherNotaryParty,
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 140,
            transactionCreditLimit = 141,
            freeTransactions = 142
        ),
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val issueFuture = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val outStates = issueFuture.getOrThrow().coreTransaction.outRefsOfType<MeteringTermsAndConditionsState>()
    val proposalFlow = sector1BankNode.startFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposer(outStates.single(), counterTC))
    mockNet.runNetwork()
    assertFailsWith<FlowException> { proposalFlow.getOrThrow() }
  }

  @Test
  fun `we can't propose metering T&Cs `() {
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = Instant.now(),
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val counterTC = dummyTC.copy(
        meteredParty = anotherNotaryParty,
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 140,
            transactionCreditLimit = 141,
            freeTransactions = 142
        ),
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val issueFuture = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val outStates = issueFuture.getOrThrow().coreTransaction.outRefsOfType<MeteringTermsAndConditionsState>()
    val proposalFlow = sector1BankNode.startFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposer(outStates.single(), counterTC))
    mockNet.runNetwork()
    assertFailsWith<FlowException> { proposalFlow.getOrThrow() }
  }

  @Test
  fun `we can't issue metering T&Cs from a non-notary node to a node`() {
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = sector2Bank,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = Instant.now(),
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val proposalFlow = sector2BankNode.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    assertFailsWith<FlowException> { proposalFlow.getOrThrow() }
  }

  @Test
  fun `we can't issue metering T&Cs to the T&C's own guardian notary`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = guardianNotaryParty,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 0,
            transactionCreditLimit = 0,
            freeTransactions = 0
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val proposalFlow = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    assertFailsWith<FlowException> { proposalFlow.getOrThrow() }
  }

  @Test
  fun `we can issue metering T&Cs to a node with arbitrary properties`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 10581L,
            transactionCreditLimit = -91091L,
            freeTransactions = -1027L
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val lastTermsAndConditions = getMostRecentTermsAndConditions(sector1BankNode)
    val billingType = lastTermsAndConditions.meteringTermsAndConditionsProperties.billingType
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteringParty, meteringNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteredParty, sector1Bank)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.billingToken, TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name))
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payParty, daoNodeParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.guardianNotaryParty, guardianNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payAccountId, daoHoldingAccount)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.createdDateTime, creationTime)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.status, MeteringTermsAndConditionsStatus.ISSUED)
    assertEquals(billingType.transactionCost, 10581L)
    assertEquals(billingType.transactionCreditLimit, -91091L)
    assertEquals(billingType.freeTransactions, -1027L)
  }

  @Test
  fun `we can issue metering T&Cs to a notary with arbitrary properties`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = anotherNotaryParty,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 1091L,
            transactionCreditLimit = -1091L,
            freeTransactions = 4721L
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val lastTermsAndConditions = getMostRecentTermsAndConditions(anotherNotary)
    val billingType = lastTermsAndConditions.meteringTermsAndConditionsProperties.billingType
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteringParty, meteringNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.meteredParty, anotherNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.billingToken, TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name))
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payParty, daoNodeParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.guardianNotaryParty, guardianNotaryParty)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.payAccountId, daoHoldingAccount)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.createdDateTime, creationTime)
    assertEquals(lastTermsAndConditions.meteringTermsAndConditionsProperties.status, MeteringTermsAndConditionsStatus.ISSUED)
    assertEquals(billingType.transactionCost, 1091L)
    assertEquals(billingType.transactionCreditLimit, -1091L)
    assertEquals(billingType.freeTransactions, 4721L)
  }

  /* TODO: Re-enable after making changes to Accept.

  @Test
  fun `accepting a metered T&C`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val counterTC = dummyTC.copy(
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 140,
            transactionCreditLimit = 141,
            freeTransactions = 142
        ),
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )

    val issueFuture = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val outStates = issueFuture.getOrThrow().coreTransaction.outRefsOfType<MeteringTermsAndConditionsState>()
    val future = meteringNotary.startFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposer(outStates.single(), counterTC))
    mockNet.runNetwork()

    val results = future.getOrThrow()
    val termsConditionState = results.tx.outputStates[0] as MeteringTermsAndConditionsState

    val acceptFlow = sector1BankNode.startFlow(AcceptMeteringTermsAndConditionsProposeFlow.TermsAndConditionsAccepter(termsConditionState.linearId))
    mockNet.runNetwork()

    val results2 = acceptFlow.getOrThrow()
    val newTermsConditionState = results2.tx.outputStates[0] as MeteringTermsAndConditionsState

    assertEquals(
        termsConditionState.meteringTermsAndConditionsProperties.meteringParty,
        newTermsConditionState.meteringTermsAndConditionsProperties.meteringParty
    )
    assertEquals(
        termsConditionState.meteringTermsAndConditionsProperties.meteredParty,
        newTermsConditionState.meteringTermsAndConditionsProperties.meteredParty
    )
    assertEquals(
        MeteringTermsAndConditionsStatus.ACCEPTED,
        newTermsConditionState.meteringTermsAndConditionsProperties.status
    )
  }

  @Test
  fun `accepting metered T&C failure when linearid not found`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val counterTC = dummyTC.copy(
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 140,
            transactionCreditLimit = 141,
            freeTransactions = 142
        ),
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )

    val issueFuture = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val outStates = issueFuture.getOrThrow().coreTransaction.outRefsOfType<MeteringTermsAndConditionsState>()
    val future = meteringNotary.startFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposer(outStates.single(), counterTC))
    mockNet.runNetwork()

    val results = future.getOrThrow()
    val termsConditionState = results.tx.outputStates[0] as MeteringTermsAndConditionsState

    try {
      // Using a node that shouldn't know about this linearid or state
      val totoro = sector2BankNode.startFlow(AcceptMeteringTermsAndConditionsProposeFlow.TermsAndConditionsAccepter(termsConditionState.linearId))
      mockNet.runNetwork()
      totoro.getOrThrow()
    } catch (e: FlowException) {
      assert(e.originalMessage?.startsWith("Query did not find exactly one state") ?: false)
    }
  }

  EndAccept */


  /* TODO: Re-enable after making changes to Reject
  @Test
  fun `rejecting a metered T&C`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val counterTC = dummyTC.copy(
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 140,
            transactionCreditLimit = 141,
            freeTransactions = 142
        ),
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val issueFuture = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val outStates = issueFuture.getOrThrow().coreTransaction.outRefsOfType<MeteringTermsAndConditionsState>()
    val future = meteringNotary.startFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposer(outStates.single(), counterTC))
    mockNet.runNetwork()

    val results = future.getOrThrow()
    val termsConditionState = results.tx.outputStates[0] as MeteringTermsAndConditionsState

    val rejectFlow = sector1BankNode.startFlow(RejectMeteringTermsAndConditionsProposeFlow.TermsAndConditionsRejecter(termsConditionState.linearId))
    mockNet.runNetwork()

    val results2 = rejectFlow.getOrThrow()
    val newTermsConditionState = results2.tx.outputStates[0] as MeteringTermsAndConditionsState

    assertEquals(
        termsConditionState.meteringTermsAndConditionsProperties.meteringParty,
        newTermsConditionState.meteringTermsAndConditionsProperties.meteringParty
    )
    assertEquals(
        termsConditionState.meteringTermsAndConditionsProperties.meteredParty,
        newTermsConditionState.meteringTermsAndConditionsProperties.meteredParty
    )
    assertEquals(
        MeteringTermsAndConditionsStatus.REJECTED,
        newTermsConditionState.meteringTermsAndConditionsProperties.status
    )
  }

  @Test
  fun `rejecting metered T&C failure when linearId not found`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = DEFAULT_TRANSACTION_COST,
            transactionCreditLimit = DEFAULT_TRANSACTION_CREDIT_LIMIT,
            freeTransactions = DEFAULT_FREE_TRANSACTIONS
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.ISSUED
    )
    val counterTC = dummyTC.copy(
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 140,
            transactionCreditLimit = 141,
            freeTransactions = 142
        ),
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val issueFuture = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    val outStates = issueFuture.getOrThrow().coreTransaction.outRefsOfType<MeteringTermsAndConditionsState>()
    val future = meteringNotary.startFlow(ProposeMeteringTermsAndConditionsFlow.TermsAndConditionsProposer(outStates.single(), counterTC))
    mockNet.runNetwork()

    val results = future.getOrThrow()
    val termsConditionState = results.tx.outputStates[0] as MeteringTermsAndConditionsState

    try {
      // Using a node that shouldn't know about this linearid or state
      val tandcFlowFuture = sector2BankNode.startFlow(RejectMeteringTermsAndConditionsProposeFlow.TermsAndConditionsRejecter(termsConditionState.linearId))
      mockNet.runNetwork()
      tandcFlowFuture.getOrThrow()
    } catch (e: FlowException) {
      assert(e.originalMessage?.startsWith("Query did not find exactly one state") ?: false)
    }
  }
  */

  @Test
  fun `we can't send metering T&Cs with a billingToken that doesn't exist`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor("NC", 1, anotherNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 0,
            transactionCreditLimit = 0,
            freeTransactions = 0
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val proposalFlow = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    assertFailsWith<FlowException> { proposalFlow.getOrThrow() }
  }

  @Test
  fun `we can't send metering T&Cs with a payAccountId that doesn't exist`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 0,
            transactionCreditLimit = 0,
            freeTransactions = 0
        ),
        payParty = daoNodeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = "bigBox1058",
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val proposalFlow = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    assertFailsWith<FlowException> { proposalFlow.getOrThrow() }
  }

  @Test
  fun `we can't send metering T&Cs with a payParty that doesn't exist`() {
    val creationTime = Instant.now()
    val dummyTC = MeteringTermsAndConditionsProperties(
        meteringParty = meteringNotaryParty,
        meteredParty = sector1Bank,
        billingToken = TokenType.Descriptor(testCurrencyXTS, 2, meteringNotaryX500Name),
        billingType = MeteringPerTransactionBillingType(
            transactionCost = 0,
            transactionCreditLimit = 0,
            freeTransactions = 0
        ),
        payParty = fakeParty,
        guardianNotaryParty = guardianNotaryParty,
        payAccountId = daoHoldingAccount,
        createdDateTime = creationTime,
        status = MeteringTermsAndConditionsStatus.PROPOSED
    )
    val proposalFlow = meteringNotary.startFlow(IssueMeteringTermsAndConditionsFlow.TermsAndConditionsIssuer(dummyTC))
    mockNet.runNetwork()
    assertFailsWith<FlowException> { proposalFlow.getOrThrow() }
  }

  private fun getMostRecentTermsAndConditions(node: StartedMockNode): MeteringTermsAndConditionsState {
    return node.transaction {
      val meteringTermsAndConditionsStatesPage = runAndRetryGeneric({ node.services.vaultService.queryBy<MeteringTermsAndConditionsState>() }, 10, 500)
      meteringTermsAndConditionsStatesPage.states.last().state.data
    }
  }

  @After
  fun tearDown() {
    mockNet.runNetwork()
    mockNet.stopNodes()
  }
}
