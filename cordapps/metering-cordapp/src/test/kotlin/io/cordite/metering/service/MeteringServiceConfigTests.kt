/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import net.corda.core.identity.CordaX500Name
import org.junit.Ignore
import org.junit.Test

@Ignore
class MeteringServiceConfigTests {

  @Test
  fun `I can load the config with all the parameters populated with default file`(){
    val meteringServiceConfig = MeteringServiceConfig.loadConfig()
    assert(meteringServiceConfig.daoPartyName=="Cordite Committee")
    assert(meteringServiceConfig.meteringRefreshInterval==2000L)
    assert(meteringServiceConfig.meteringServiceType==MeteringServiceType.SingleNodeMeterer)
    assert(meteringServiceConfig.meteringNotaryAccountId=="metering-account-acc1")
    assert(meteringServiceConfig.daoName=="Cordite Committee")
    assert(meteringServiceConfig.meteringPartyName=="Cordite Metering Notary")
    assert(meteringServiceConfig.minMeteringTransactionSize==1)
    assert(meteringServiceConfig.maxMeteringTransactionSize==1000)
    assert(meteringServiceConfig.maxMeteringTransactionTimeWait==86400000L)
    assert(meteringServiceConfig.meteringDefaults == MeteringTermsAndConditionsDefaults(
      billingTokenTypeUri = "XTS:2:CN=SimpleNotaryService,O=Cordite Metering Notary,L=Argleton,C=GB",
      transactionCost = 10,
      transactionCreditLimit = 30,
      freeTransactions = 1,
      transactionCostForNotaries = 0,
      transactionCreditLimitForNotaries = 0,
      freeTransactionsForNotaries = 0,
      payPartyName = CordaX500Name("Cordite Committee", "London", "GB"),
      payAccount = "dao-holding-account",
      guardianNotaryName = CordaX500Name("Cordite Guardian Notary", "Argleton", "GB"),
      authorizedForZeroTC = setOf(CordaX500Name("Cordite Guardian Notary", "Argleton", "GB"), CordaX500Name("Another Notary", "Argleton", "GB"))
    ))
  }

  @Test
  fun `I can load the config with all the parameters populated with a file of my choice`(){
    val meteringServiceConfig = MeteringServiceConfig.loadConfig("metering-config-file-of-my-choice.json")
    assert(meteringServiceConfig.daoPartyName=="daodao")
    assert(meteringServiceConfig.meteringRefreshInterval==3000L)
    assert(meteringServiceConfig.meteringServiceType==MeteringServiceType.BftSmartMeterer)
    assert(meteringServiceConfig.meteringNotaryAccountId=="metering-account-acc2")
    assert(meteringServiceConfig.daoName=="SomeOtherDao")
    assert(meteringServiceConfig.meteringPartyName=="MeteringNotary")
    assert(meteringServiceConfig.minMeteringTransactionSize==12)
    assert(meteringServiceConfig.maxMeteringTransactionSize==1002)
    assert(meteringServiceConfig.maxMeteringTransactionTimeWait==86400002L)
    assert(meteringServiceConfig.meteringDefaults == MeteringTermsAndConditionsDefaults(
            billingTokenTypeUri = "XTS:2:CN=SimpleNotaryService,O=Cordite Bleetering Notary,L=Argmelyon,C=GB",
            transactionCost = 100,
            transactionCreditLimit = -3,
            freeTransactions = 1000,
            transactionCostForNotaries = 5,
            transactionCreditLimitForNotaries = 6,
            freeTransactionsForNotaries = -10,
            payPartyName = CordaX500Name("Bordite Yammittee", "Wales", "GB"),
            payAccount = "dao-colding-account",
            guardianNotaryName = CordaX500Name("Cordite Yardian Yoteary", "Margleton", "GB"),
            authorizedForZeroTC = setOf(CordaX500Name("Uonmite Hardian Noticery", "Mardleton", "AU"))
    ))
  }

  @Test
  fun `I can load the config with all the metering parameters except metering defaults populated`(){
    val meteringServiceConfig = MeteringServiceConfig.loadConfig("metering-config-file-without-defaults.json")
    assert(meteringServiceConfig.daoPartyName=="daodao")
    assert(meteringServiceConfig.meteringRefreshInterval==3000L)
    assert(meteringServiceConfig.meteringServiceType==MeteringServiceType.BftSmartMeterer)
    assert(meteringServiceConfig.meteringNotaryAccountId=="metering-account-acc2")
    assert(meteringServiceConfig.daoName=="SomeOtherDao")
    assert(meteringServiceConfig.meteringPartyName=="MeteringNotary")
    assert(meteringServiceConfig.minMeteringTransactionSize==1)
    assert(meteringServiceConfig.maxMeteringTransactionSize==1000)
    assert(meteringServiceConfig.maxMeteringTransactionTimeWait==86400000L)
    assert(meteringServiceConfig.meteringDefaults == MeteringTermsAndConditionsDefaults())
  }

  @Test
  fun `I will resort to defaults if I cant find a file`(){
    val meteringServiceConfig = MeteringServiceConfig.loadConfig("you-will-never-find-this-metering-config-file-of-my-choice.json")
    assert(meteringServiceConfig.daoPartyName=="")
    assert(meteringServiceConfig.meteringRefreshInterval==2000L)
    assert(meteringServiceConfig.meteringServiceType==MeteringServiceType.SingleNodeMeterer)
    assert(meteringServiceConfig.meteringNotaryAccountId=="")
    assert(meteringServiceConfig.daoName=="")
    assert(meteringServiceConfig.meteringPartyName=="")
    assert(meteringServiceConfig.meteringDefaults == MeteringTermsAndConditionsDefaults())
    assert(meteringServiceConfig.minMeteringTransactionSize==1)
    assert(meteringServiceConfig.maxMeteringTransactionSize==1000)
    assert(meteringServiceConfig.maxMeteringTransactionTimeWait==86400000L)
  }
}
