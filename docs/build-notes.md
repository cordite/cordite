<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Dev Ops

This doc is to pull together our processes and known issues for our build.

# Runner setup

## gpg for maven release

1. Install gpg2: 

	sudo apt-get install gnupg2

2. Transfer keys: 

    ssh <host> mkdir keys 
    scp /keybase/team/cordite_post_rbs/gpg-keys/* <host>:./keys/.

3. Import keys to gpg as the gitlab runner user: 

    mv keys /tmp

    sudo chown -R gitlab-runner /tmp/keys

    sudo su - gitlab-runner

    gpg --import --batch --allow-secret-key-import /tmp/keys/*.asc

4. Delete keys: 

    rm -rf /tmp/keys

5. Ensure that the CI sets these variables:

    export GPG_KEYID=58BFE72E
    export GPG_KEYNAME=community@cordite.io
    export GPG_PASSPHRASE=<see-keybase>
    export MAVEN_REPO_PASS=<see-keybase>
    export MAVEN_REPO_URL=https://oss.sonatype.org
    export MAVEN_REPO_USER=devops@cordite.foundation

    The above is for Cordite. For Braid it's different - see Keybase.

6. Publish will now work. e.g. 

    ./gradlew publish 