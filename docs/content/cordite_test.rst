.. _test:

Cordite Test
============

.. toctree::
   :hidden:

   cordite_test/dgl-cli
   cordite_test/dao-cli
   cordite_test/metering-cli  
   cordite_test/braid-js-tutorial
   cordite_test/token
   cordite_test/daos


.. figure:: /images/world.png
   :alt: Cordite World Picture

   Cordite World Picture


There is also https://nms-test.cordite.foundation which provides a UI to view the network.

Edge network has the most bleeding edge version of Cordite deployed. This is the latest successful build of master in the Cordite repo. It should be seen as an unstable environment. Data is persisted but can be removed at any time without warning.

Test network is the latest release of Cordite deployed. This is the latest version to be pushed to the public registries e.g. DockerHub. It should be seen as a stable environment and a pre-cursor to the main network which will follow. Data is persisted and should remain. No guarantees are made.

The aim of the test network is to allow people to:
-  :ref:`Connect <braid-js-tutorial>` to cordite 
-  Build decentralised applications using cordite
-  :ref:`Build DAOs <dao>`
-  Add your own node

Connection details
------------------

+-------------------------------------------------------+------------+--------------+
| Node name (endpoint hyperlink)                        | Node       | Party        | 
|                                                       | location   | name         | 
+=======================================================+============+==============+
| `amer <https://amer-test.cordite.foundation>`__  | eastus     | O=Cordite    |
|                                                       |            | AMER,        |
|                                                       |            | OU=Cordite   |
|                                                       |            | Foundation,  |
|                                                       |            | L=New        |
|                                                       |            | York         |
|                                                       |            | City, C=US   |
+-------------------------------------------------------+------------+--------------+
| `apac <https://apac-test.cordite.foundation>`__  | southeast  | O=Cordite    |
|                                                       | asia       | APAC,        |
|                                                       |            | OU=Cordite   |
|                                                       |            | Foundation,  |
|                                                       |            | L=Singapore, |
|                                                       |            | C=SG         |
+-------------------------------------------------------+------------+--------------+
| `emea <https://emea-test.cordite.foundation>`__  | westeurope | O=Cordite    |
|                                                       |            | EMEA,        | 
|                                                       |            | OU=Cordite   | 
|                                                       |            | Foundation,  | 
|                                                       |            | L=London,    | 
|                                                       |            | C=GB         | 
+-------------------------------------------------------+------------+--------------+

Cordite Entities
----------------

+------------------+--------------------------+------------------------+
| Node name        | Node location            | Party name             |
+==================+==========================+========================+
| Cordite Metering | westeurope               | O=Cordite Metering     |
| Notary           |                          | Notary, OU=Cordite     |
|                  |                          | Foundation,            |
|                  |                          | L=London,C=GB          |
+------------------+--------------------------+------------------------+
| Cordite          | westeurope               | O=Cordite Committee,   |
| Committee        |                          | OU=Cordite Foundation, |
|                  |                          | L=London,C=GB          |
+------------------+--------------------------+------------------------+
