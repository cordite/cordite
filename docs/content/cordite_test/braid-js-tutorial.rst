.. _braid-js-tutorial:

Braid JS Client Example
=======================


Aim
---

The aim of this tutorial is to create a small cordite client in node js
using `braid <https://gitlab.com/bluebank/braid>`__. We will connect
this to the braid endpoint of the emea test node in the cordite test
network. We will then proceed to make a few doc type queries and also to
create your very own Dao!

At the end of this tutorial you should be comfortable creating
javascript cordite clients to call your serverside braid endpoints.

The completed code for this tutorial can be found
`here <https://gitlab.com/cordite/cordite/tree/master/clients/braid-js-example>`__.

Pre-requisites
--------------

- `NodeJS <https://nodejs.org>`__ installed on your machine

Steps
-----

1. In a terminal, create a directory in which to put your code and change to that directory

   .. code:: sh

      mkdir braidJsClient
      cd braidJsClient

|

2. Next, initialise a node project:

   .. code:: sh

      > npm init
      This utility will walk you through creating a package.json file.
      It only covers the most common items, and tries to guess sensible defaults.

      See `npm help json` for definitive documentation on these fields
      and exactly what they do.

      Use `npm install <pkg>` afterwards to install a package and
      save it as a dependency in the package.json file.

      Press ^C at any time to quit.
      package name: (wibble) cordite-braid-js-client
      version: (1.0.0)
      description:
      entry point: (index.js) client.js
      test command:
      git repository:
      keywords:
      author:
      license: (ISC)
      About to write to /private/tmp/wibble/package.json:

      {
      "name": "cordite-braid-js-client",
      "version": "1.0.0",
      "description": "",
      "main": "client.js",
      "scripts": {
         "test": "echo \"Error: no test specified\" && exit 1"
      },
      "author": "",
      "license": "ISC"
      }

      Is this ok? (yes) y

|

3. Install the braid javascript client

   .. code:: sh

      npm install --save braid-client

|

4. Connect braid to the cordite emea test test node

   -  create the client.js file we specified above: ``touch client.js``.
   -  edit this file to import the dependency and connect to emea test

   .. code:: js

      const Proxy = require('braid-client').Proxy;

      const emea = new Proxy({url: 'https://emea-test.cordite.foundation/api/'}, onOpen, onClose, onError, {strictSSL: false})

      function onOpen() {
          console.log("connected")
      }

      function onClose() {
          console.log("closed")
      }

      function onError(err) {
          console.error(err)
      }

   If we run this using ``node client.js`` we should get the output:
   ``connected!``.

|

5. Next we want to see what is in the braid endpoint. So change the
   ``onOpen()`` function to:

   .. code:: js

      function onOpen() {
          console.log("connected to the emea test cordite node")
          console.log(JSON.stringify(emea))
          console.log("\ndao functions:")
          Object.getOwnPropertyNames(emea.dao).forEach(f => console.log(f))
          console.log("")
          emea.dao.daoInfo.docs()
      }

   This should give the output: 
   
   .. code:: sh
        
      node client.js
      connected to the emea test cordite node 
      {“network”:{},“flows”:{},“ledger”:{},“dao”:{}}

      daoFunctions:
      getServiceHub
      voteForProposal
      daoInfo
      createDao
      newMemberProposalsFor
      normalProposalsFor
      modelDataProposalsFor
      createProposal
      createNewMemberProposal
      createRemoveMemberProposal
      acceptNewMemberProposal
      acceptRemoveMemberProposal
      acceptProposal
      voteForMemberProposal
      requestProposalConsistencyCheckFor
      createModelDataProposal
      voteForModelDataProposal
      acceptModelDataProposal

      API documentation
      -----------------
      * daoInfo(daoName) => array

      @param daoName - string

   Which we know means we have four ``ServiceProxy`` objects, one each
   for network, flows, ledger and dao. The second line prints out all
   the available methods and the last line prints out the docs for the
   daoInfo method.

|

6. Finally lets create a dao:

   .. code:: js

      let saltedDaoName = 'testDao-'+new Date().getTime()

      function onOpen() {
          console.log("connected to the emea test cordite node")

          emea.dao.daoInfo(saltedDaoName).then(daos => {
              console.log("there were", daos.length, "existing daos with name", saltedDaoName )

              return emea.dao.createDao(saltedDaoName, "O=Cordite Metering Notary, OU=Cordite Foundation, L=London,C=GB")
          }).then(dao => {
              console.log(saltedDaoName,"created with key",JSON.stringify(dao.daoKey))
          }).catch(error => {
              console.error(error)
          })
      }

   Running this gives us:

   .. code:: sh

      connected to the emea test cordite node
      there were 0 existing doas with name testDao-1524060634372
      testDao-1524060634372 created with key {"name":"testDao-1524060634372","uuid":"f99c32c4-7e9c-4c3a-af99-9765d8e6e5b4","uniqueIdentifier":{"externalId":"testDao-1524060634372","id":"f99c32c4-7e9c-4c3a-af99-9765d8e6e5b4"}}

|

The full code is now:

.. code:: js

    const Proxy = require('braid-client').Proxy;

    const emea = new Proxy({url: 'https://emea-test.cordite.foundation/api/'}, onOpen, onClose, onError, {strictSSL: false})

    let saltedDaoName = 'testDao-'+new Date().getTime()

    function onOpen() {
        console.log("connected to the emea test cordite node")

        emea.dao.daoInfo(saltedDaoName).then(daos => {
            console.log("there were", daos.length, "existing daos with name", saltedDaoName )

            return emea.dao.createDao(saltedDaoName, "O=Cordite Metering Notary, OU=Cordite Foundation, L=London,C=GB")
        }).then(dao => {
            console.log(saltedDaoName,"created with key",JSON.stringify(dao.daoKey))
        }).catch(error => {
            console.error(error)
        })
    }

    function onClose() {
        console.log("closed")
    }

    function onError(err) {
        console.error(err)
    }

