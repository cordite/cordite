Starting Cordite Development from Zero
======================================


.. toctree::
    :hidden:
    :glob:

    startingfromzero/*


Background Reading - Read the classics
--------------------------------------

To understand the purpose of Cordite and the wider picture around blockchain and crypto currencies
it is worth reading the following 'classic' whitepapers which between them contain the key concepts
of the underlying technology and the emerging concepts of governance, economics and incentives.

- `The Bitcoin whitepaper <https://bitcoin.org/en/bitcoin-paper>`__
- `The Ethereum whitepaper <https://github.com/ethereum/wiki/wiki/White-Paper>`__
- `The Dash white paper <https://github.com/dashpay/dash/wiki/Whitepaper>`__
- `The Corda Intro white paper <https://www.corda.net/content/corda-platform-whitepaper.pdf>`__
- `The Corda Technical white paper <https://www.corda.net/content/corda-technical-whitepaper.pdf>`__


Languages
---------

- Kotlin

Cordite is written in Kotlin, a JVM based language

If you are used to Java, Scala, C#, C++ or any other strongly typed language you will find it quite
easy to pick up kotlin. The code is concise, but very readable.
As Cordite is based on the JVM you can in theory use any JVM language to build your own Cordapps, but as the 
native platform (Corda) is written in Kotlin, it made sense for us Cordite developers to use the same language
as it makes it easier to understand and debug.

You can start to learn kotlin `here <https://kotlinlang.org/>`__

- Javascript

Cordite provides client libraries in javascript to enable web developers and nodejs developers to build 
applications and services that consume it.
We also provide a javascript command line interface which we use for demos and proof of concepts.

You can start to learn java script `here <https://www.w3schools.com/js/>`__


Tools and Installation
----------------------

To get up and running and developing with Cordite you will need to install a number of tools and
libraries. Todays modern package managers are really great for making this easier:

- For Mac Osx we recommend the 'home brew' package manager, install instructions are `here <https://brew.sh/>`__

- For Windows we recommend the 'chocolatey' package manager which can be found `here <https://chocolatey.org/>`__

- For Linux variants, these have their own inbuilt package managers such as apt-get and yum

Libraries
---------

- Nodejs 

  ``` brew install nodejs ```

  or

  ``` choco install nodejs ```

  or

  Nodejs, version 10.14.0 or above which you can get `here <https://nodejs.org/en/>`__


- Java

  Java JDK 8 191 or above, which you can get `here <https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html>`__


Source Control
--------------

All Corda and Cordite projects use git

```brew install git```

```choco install git```

Development IDEs
----------------

You can use whatever IDE you prefer to develop your cordite app.

Most cordite developers currently use the following:

- For Kotlin and Cordapp Development

IntelliJ, version 2018.2 which can be found `here <https://www.jetbrains.com/idea/download/previous.html>`__

- For Java Script or Web Development

Visual studio code which can be found `here <https://code.visualstudio.com/>`__


Containerisation
----------------

All of the Cordite test and production nodes run in docker which you can test locally 
on your laptop (if you have 8GB or more memory)

Install the Docker Community Edition from `here <https://hub.docker.com/search?q=&type=edition&offering=community>`__


Productivity Tools
------------------

To radically improve your command line experience, we thoroughly recommend Oh My ZSH
which is great if you want to be a 10x developer but can't type.

You can find it `here <https://ohmyz.sh/>`__

Join the Cordite Community
--------------------------

You can join the cordite community through the followin channels

-  We use #cordite channel on `Corda slack <https://slack.corda.net/>`__
-  We informally meet at the `Corda London meetup <https://www.meetup.com/pro/corda/>`__
-  email `community@cordite.foundation <mailto:community@cordite.foundation>`__

And then start contributing if you wish

.. _contributing-1:

