<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Getting set up to use the XTS Fountain

The XTS fountain will mint you XTS and send it to an account on your corda node.
But first, you need to set up your Corda node.

The following steps will guide you through this. 

## Select a Corda Network for your Node.

Currently you can access XTS fountains on three Corda Networks:

- [Corda Production Network](https://cordite.foundation/)
- [Corda Pre-Production Network](https://fountain.csl-uat.cordite.foundation/)
- Cordite Test Network

If you are on any of these networks already then you can move to install cordite section.

## Join the Corda Pre-Production or Production network

To join the Corda either of the Corda networks, follow these [instructions](https://corda.network/participation/)

## Join the Cordite Test Network

The instructions to install a Corda node with the Cordite Cordapp already installed are [here](https://gitlab.com/cordite/cordite#how-do-i-deploy-my-own-cordite-node)

If you prefer to configure your Corda node yourself, then you just need to set your network settings in your node.conf as follows:

```
networkServices {
    doormanURL = "https://nms-test.cordite.foundation"
    networkMapURL = "https://nms-test.cordite.foundation"
}
```

Note: Your node's P2P Address must be accessible from the public internet - you can use tools like [Ngrok](https://ngrok.com/product) to do this if you are testing with your laptop.

## Install the Cordite Cordapp in your node

The cordite cordapps can be found [here](http://central.maven.org/maven2/io/cordite/)

You need to set a number of environment variables according to your requirements, the details are [here](https://gitlab.com/cordite/cordite#environment-variables).

## Create a Cordite Account

The easiest way to create a Cordite account is to use the cordite command line interface (CLI)

### Install the Cordite CLI

Ensure you have node.js on your workstation

If you have not, install instructions are [here](https://nodejs.org/en/)

Now you can install the Cordite CLI

```
npm install -g cordite-cli
```

### Connect to you node

In a command prompt type:

Cordite https://your-nodes-address:8080

You should see the following in your command prompt if you have connected successfully.

```
  _____            ___ __     
 / ___/__  _______/ (_) /____ 
/ /__/ _ \/ __/ _  / / __/ -_)
\___/\___/_/  \_,_/_/\__/\__/

connecting to https://your-nodes-address:8080/api/ with { url: 'https://your-nodes-address:8080/api/' } and { strictSSL: false }

connected to node: OU=My Awesome Node, O=MyCompany, L=Antartica, C=AT

available objects: corda, notaries, network, flows, ledger, test, dao
cordite > 

```
### Save the notary name

To list the notaries in the network type the following at the cordite prompt:

```
notaries
```

You will get something like this:

```
cordite > notaries
{
  r3HoldCoLLC: {
    name: 'CN=Non-validating Prod SUB0 HA Notary, O=R3 HoldCo LLC, L=New York, C=US',
    owningKey: 'aSq9DsNNvGhYxYyqA9wd2eduEAZ5AXWgJTbTJd2KBaz6nCvjCuTE2caxB6Bfazjj1e4pE5gaPdaKHuNpUWvhQAg45V8CEE99Ckbop1Qgg6kVuSGqG1oCsCnBeZBR'
  }
}
```

Pick a notary from the list - in this example I'll pick 'corditeBootstrapNotary'

now you can save the notary name like this:

```
notary = notaries.corditeBootstrapNotary.name
```

### Create the account

Decide on an account name - let's use 'myaccount' as an example.
And then enter the following in the cordite command prompt:

```
ledger.createAccount('myaccount',notary)
```

You should then get a response like this:

```
{ address:
   { accountId: 'myaccount',
     party: 'OU=Cordite Foundation, O=Cordite APAC, L=Hong Kong, C=HK',
     uri:
      'myaccount@OU=Cordite Foundation, O=Cordite APAC, L=Hong Kong, C=HK' },
  tags:
   [ { category: 'DGL.ID',
       value:
        'myaccount@OU=Cordite Foundation, O=Cordite APAC, L=Hong Kong, C=HK' } ],
  linearId:
   { externalId: 'myaccount',
     id: '1daf6e65-63c8-4817-8a2c-5ccc61c02baf' },
  participants:
   [ { name: 'OU=Cordite Foundation, O=Cordite APAC, L=Hong Kong, C=HK',
       owningKey:
        'GfHq2tTVk9z4eXgyVqM96fbMWXFQzpqxVbp4A7o33H7kyhR9YgJuwtnBzQaV' } ] }
```

The key bit here is the account 'uri' which is:

```
'myaccount@OU=Cordite Foundation, O=Cordite APAC, L=Hong Kong, C=HK'
```

## Get some Tokens

Go to the apppropriate fountain URL:


* [Corda Production](https://cordite.foundation/)

* [Corda Pre-Production](https://cordite.gitlab.io/fountain/)
 
* Cordite Test


Paste you account URI into the text box just below ‘Put your account uri here’
The press the ‘Get Some Tokens’ Button.

![fountain entering account](images/fountain-main.png)

## Check your Balance

Don’t take our word for it that this worked, check your account balance

In the Cordite CLI type:

```
ledger.balanceForAccount('myaccount')
```

You should see something like this:

```
[ { quantity: '100.00',
    amountType:
     { symbol: 'XTS',
       issuerName:
        'OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB',
       uri:
        'XTS:OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB' } } ]
```

# Using the Echobot

The Echobot will send you back any tokens that you send to it.
You can use this to test your own token functionality in your Cordapps.

These instructions assume you have already set up your corda node and have
retrieved some tokens from the fountain.

The following steps show how we can use the Cordite CLI to test with the Echobot.
You can of course to this in your own Cordapp and write in Kotlin, Java or if you really must Scala.

## Create an Echo Account

Using the Cordite CLI, create a new account called 'echo'

```
 ledger.createAccount('echo',notary)
```

## Learn how to transfer tokens using the Cordite CLI

From the account you created above transfer some tokens to the Cordite Foundations echo account.

If you are not sure what parameters you need for a Cordite CLI function, you can always get the details using the CLI as follows:

```
ledger.transferAccountToAccount.docs()
```

You will get a response like this:

```
API documentation
-----------------
* transferAccountToAccount(amount, tokenTypeUri, fromAccount, toAccount, description, notary) => {offset:integer,size:integer,bytes:array}

  @param amount - string
  @param tokenTypeUri - string
  @param fromAccount - string
  @param toAccount - string
  @param description - string
  @param notary - {commonName:string,organisationUnit:string,organisation:string,locality:string,state:string,country:string,x500Principal:{name:string,encoded:array}}

```

## Get the 'echo' account details for the Cordite Foundation echo account

Using the Cordite CLI we can list all the nodes on a Corda Network

```
network.allNodes(a => {a.forEach( n => { console.log(n)})})
```

You will get something like this:

```
{ addresses: [ { host: 'fountain', port: 10002 } ],
  legalIdentities:
   [ { name:
        'OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB',
       owningKey:
        'GfHq2tTVk9z4eXgyKYTqDjADkftsyCZN3aTW5YGPAY74oT95ftuC12mmVYiv' } ] }
{ addresses: [ { host: 'bootstrap-notary', port: 10002 } ],
  legalIdentities:
   [ { name:
        'CN=Cordite Bootstrap Notary, OU=Cordite Foundation, O=Cordite Bootstrap Notary, L=London, ST=London, C=GB',
       owningKey:
        'GfHq2tTVk9z4eXgyPrn8j5oAKvbJ6N34bJ66FSNSR1wRFZxjKDw8EyNuCNin' } ] }
{ addresses: [ { host: 'amer', port: 10002 } ],
  legalIdentities:
   [ { name:
        'OU=Cordite Foundation, O=Cordite AMER, L=New York City, C=US',
       owningKey:
        'GfHq2tTVk9z4eXgyRqGYDzhzs6nJKyMtKuXrJtAo9nBeTQuZmnPQT9PRuBRB' } ] }
{ addresses: [ { host: 'apac', port: 10002 } ],
  legalIdentities:
   [ { name: 'OU=Cordite Foundation, O=Cordite APAC, L=Hong Kong, C=HK',
       owningKey:
        'GfHq2tTVk9z4eXgyVqM96fbMWXFQzpqxVbp4A7o33H7kyhR9YgJuwtnBzQaV' } ] }
```

From this list you can look for the 'Cordite Society Limited'

The full name will be:

```
OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB
```

You can then create the echo account name by pre-prending 'echo@' like this:

```
echo@OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB
```

## Get the full Uri of the token type

Within Cordite Tokens can be globally uniquely identified by their token 'uri'

You can list all the tokens types on your node with the following command:

```
ledger.listTokenTypes()
```

You will get a result like this:

```
[ { symbol: 'XTS',
    exponent: 2,
    description: '',
    issuer:
     { name:
        'OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB',
       owningKey:
        'GfHq2tTVk9z4eXgyKYTqDjADkftsyCZN3aTW5YGPAY74oT95ftuC12mmVYiv' },
    linearId:
     { externalId: 'XTS',
       id: '6f5a4bac-5fe8-468a-8592-ab492e9abd24' },
    metaData: null,
    settlements: [],
    participants: [ [Object] ],
    descriptor:
     { symbol: 'XTS',
       issuerName:
        'OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB',
       uri:
        'XTS:OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB' } } ]

```

So the token Uri for XTS is:

```
XTS:OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB
```

## Establish the full list of parameters

So now we should have


* ``` amount     = '5.00'    <-- or the amount you prefer to send ```
* ``` tokenTypeUri = 'XTS:OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB' ```
* ``` fromAccount = 'myaccount' ```
* ``` toAccount = 'echo@OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB' ```
* ``` description = 'sending some tokens to the echobot' <-- or whatever you like ```
* ``` notary = notary <-- See notes above on getting the notary ```

## Send Tokens to the EchoBot

Now we have assembled the parameters, we are now ready to send the tokens to the EchoBot.

Here's the full command

```
ledger.transferAccountToAccount('5.00','XTS:OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB','myaccount','echo@OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB','sending some tokens to the echobot',notary)

```
Run this command and you should get a reply with the transaction Id that looks like this:

```
'7pnh53KEopuLkoPt3RERsVsHpfkN1eRzaqQd7syiLraC'
```

## Check your echo balance

Don't take our word for it that this works, check your echo balance:

```
ledger.balanceForAccount('echo')
```

You should get something like this:

```
[ { quantity: '5.00',
    amountType:
     { symbol: 'XTS',
       issuerName:
        'OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB',
       uri:
        'XTS:OU=Cordite Foundation, O=Cordite Society Limited, L=London, C=GB' } } ]
```

And you can keep doing this as long as you have tokens, the echobot will always oblige !!!