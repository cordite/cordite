#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#
VERSION=${1}
ENV=${2:-edge}
NODES="notary-${ENV} amer-${ENV} emea-${ENV} apac-${ENV}"
NMS_HOST=nms-${ENV}.cordite.foundation

for NODE in ${NODES}; do 
    echo scaling ${NODE} to 0
    kubectl -n ${NODE} scale --replicas=0 deployment/${NODE}
done

for NODE in ${NODES}; do
    echo updating ${NODE} image to registry.gitlab.com/cordite/cordite:${VERSION}
    kubectl -n ${NODE} set image deployment/${NODE} cordite=registry.gitlab.com/cordite/cordite:${VERSION} --record
    echo now scaling ${NODE} up to 1
    kubectl -n ${NODE} scale --replicas=1 deployment/${NODE}
done

for NODE in ${NODES}; do
    echo "checking ${NODE} has started"
    echo "...waiting for correct version by scanning logs"
    until kubectl -n ${NODE} logs deployment/${NODE} -c cordite| grep PIPELINE | grep ${VERSION}; do
        echo waiting for ${NODE}...looking for image tag ${VERSION}
        sleep 5
    done
    echo "...now waiting for node to log started up and registered"
    until kubectl -n ${NODE} logs deployment/${NODE} -c cordite | grep -q "started up and registered"; do
        echo -e "waiting 5 secs for ${NODE} to start up and register..."
        sleep 5
    done
    echo "...next check the p2p endpoint is listening..."
    until nc -zv p2p.${NODE}.cordite.foundation 10002; do
        echo -e "waiting 5 secs for ${NODE} to listen on p2p..."
        sleep 5
    done
    if [[ ${NODE} != *notary* ]]; then
        echo "... finally checking https://${NODE}.cordite.foundation/rest/status/live"
        until curl -sSf https://${NODE}.cordite.foundation/rest/status/live; do
            echo -e "waiting 5 secs for ${NODE} to be alive..."
            sleep 5
        done
    else
        echo "not checking ${NODE} as it's a notary and we don't expose the endpoint"
    fi
    echo "\nnode ${NODE} has now started"
done


