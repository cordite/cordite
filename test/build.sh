#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# usage: <script> [image name - defaults to cordite/cordite:local]
set -e # stop on error
IMAGE_NAME=${1:-"cordite/cordite:local"}

# You can set the corda base image as the second parameter
# i.e. if you want the Zulu image, you can use: corda/corda-zulu-java1.8-4.4
IN_CORDA_DOCKER_IMAGE=${2:-"corda/corda-corretto-java1.8-4.5"}

echo "IMAGE ${IMAGE_NAME}"
echo "BASE ${IN_CORDA_DOCKER_IMAGE}"

echo "stopping all containers"
docker stop $(docker ps -q) 2>/dev/null || true
echo "removing all containers"
docker rm $(docker ps -aq) 2>/dev/null || true
echo "starting rebuild of image ${IMAGE_NAME}"
echo "building cordapps"
pushd ../cordapps
./gradlew clean deployNodes buildNode -PCI_COMMIT_REF_NAME=a -PCI_COMMIT_SHA=b -PCI_PIPELINE_ID=c
popd

echo "build cordite image"
pushd ../node
docker rmi ${IMAGE_NAME} || true
#docker build -f ./${DOCKER_FILE_NAME} --no-cache -t ${IMAGE_NAME} .
docker build --build-arg CORDA_DOCKER_IMAGE=${IN_CORDA_DOCKER_IMAGE} --no-cache -t ${IMAGE_NAME} .
popd

##FROM corda/corda-zulu-java1.8-4.4