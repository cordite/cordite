/**
The following sets up the databases required by each corda network in the docker-compose test cluster
*/
CREATE DATABASE emea;
CREATE DATABASE amer;
CREATE DATABASE apac;
CREATE DATABASE metering;
CREATE DATABASE bootstrap;
CREATE DATABASE guardian;
CREATE DATABASE committee;
