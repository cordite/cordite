/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const Proxy = require('@cordite/braid-client').Proxy;

const CSL_PASS = process.env.CSL_PASS
const PLU_PASS = process.env.PLU_PASS
const URA_PASS = process.env.URA_PASS

const CSL_UAT_PASS = process.env.CSL_UAT_PASS
const BCB_UAT_PASS = process.env.BCB_UAT_PASS
const LAB_UAT_PASS = process.env.LAB_UAT_PASS
const DASL_UAT_PASS = process.env.DASL_UAT_PASS
const TRUST_UAT_PASS = process.env.TRUST_UAT_PASS

const CSL_PROD_PASS = process.env.CSL_PROD_PASS
const BCB_PROD_PASS = process.env.BCB_PROD_PASS
const LAB_PROD_PASS = process.env.LAB_PROD_PASS
const DASL_PROD_PASS = process.env.DASL_PROD_PASS
const TRUST_PROD_PASS = process.env.TRUST_PROD_PASS

const cornerNotary = "O=Corner Notary, OU=Corner, L=London, C=GB"
const uatNotary = "CN=Non-validating UAT SUB1 HA Notary, O=R3 HoldCo LLC, L=New York, C=US"
const prodNotary = "CN=Non-validating Prod SUB0 HA Notary, O=R3 HoldCo LLC, L=New York, C=US"

var notary = cornerNotary

async function connectToCorner() {
  const csl = await connectTo(`csl-corner`, `csl-corner`, `lab577.co.uk`, CSL_PASS)
  const plu = await connectTo(`plutus-corner`, `plutus-corner`, `lab577.co.uk`, PLU_PASS)
  const ura = await connectTo(`uratus-corner`, `uratus-corner`, `lab577.co.uk`, URA_PASS)
  logStep(`using notary ${notary}`)
  return [csl, plu, ura]
}

async function connectToUat() {
  notary = uatNotary
  const csl = await connectTo(`csl-uat`, `csl-uat`, `cordite.foundation`, CSL_UAT_PASS)
  const bcb = await connectTo(`bcb-csl-uat`, `bcb-csl-uat`, `lab577.co.uk`, BCB_UAT_PASS)
  const lab = await connectTo(`lab577-uat`, `lab577-uat`, `lab577.co.uk`, LAB_UAT_PASS)
  const dasl = await connectTo(`dasl-uat`, `dasl-uat`, `lab577.co.uk`, DASL_UAT_PASS)
  const trust = await connectTo(`trustology-uat`, `trustology-uat`, `lab577.co.uk`, TRUST_UAT_PASS)
  logStep(`using notary ${notary}`)
  return [csl, bcb, lab, dasl, trust]
}

async function connectToProd() {
  notary = prodNotary
  const csl = await connectTo(`csl`, `csl-prod`, `cordite.foundation`, CSL_PROD_PASS)
  const bcb = await connectTo(`bcb-csl`, `bcb-csl-prod`, `lab577.co.uk`, BCB_PROD_PASS)
  const lab = await connectTo(`lab577`, `lab577-prod`, `lab577.co.uk`, LAB_PROD_PASS)
  const dasl = await connectTo(`dasl`, `dasl-prod`, `lab577.co.uk`, DASL_PROD_PASS)
  const trust = await connectTo(`trustology`, `trustology-prod`, `lab577.co.uk`, TRUST_PROD_PASS)

  logStep(`using notary ${notary}`)
  return [csl, bcb, lab, dasl, trust]
}

async function connectTo(urlPrefix, envDiscrim, urlSuffix, password) {
  // console.log(`connecting to ${discrim}.${urlSuffix}`)
  console.log(`connecting to ${urlPrefix}.${urlSuffix} with password ${password}`)
  return new Promise((resolve, reject) => {
    const csl = new Proxy({
      url: `https://${urlPrefix}.${urlSuffix}/api/`,
      credentials: {
        username: `${envDiscrim}`,
        password: `${password}`
      }
    }, onOpen, onClose, onError, {strictSSL: false})

    function onOpen() {
      console.log(`successfully logged in to ${envDiscrim}`)
      resolve(csl)  
    }

    function onError(err) {
      console.error(err)
      reject(err)
    }

    function onClose() {
      console.log(`closing ${discrim}`)
    }
  })
}

async function createPlutusDao(node, daoName, tokenSymbol) {
  logDoing(`creating dao ${daoName}, with token ${tokenSymbol}`)
  const plutusDao = await node.dao.createPlutusDao(daoName, notary, tokenSymbol, 0.14)
  logDone(`created dao ${plutusDao.name}`)
  return plutusDao
}

async function createPlutusDaoWithSeconds(node, daoName, tokenSymbol, seconds) {
  logDoing(`creating dao ${daoName}, with token ${tokenSymbol} and ${seconds} between proposals`)
  const plutusDao = await node.dao.createPlutusDao(daoName, notary, tokenSymbol, 0.14, seconds)
  logDone(`created dao ${plutusDao.name}`)
  return plutusDao
}

async function registerAsPlutusMember(myNode, daoName, sponsorName) {
  logDoing(`creating new member proposal with ${daoName} dao`)
  const proposal = await myNode.dao.createNewMemberProposal(daoName, sponsorName)
  logDone(`created new member proposal`)
  logDoing(`proposing to accept my membership`)
  const result = await myNode.dao.sponsorAcceptProposal(proposal.proposal.proposalKey, proposal.daoKey, sponsorName)
  logDone(`registered as member ${JSON.stringify(result)}`)
  assertThat(`member registered`, `member failed to be registered ${result}`, result.first == "ACCEPTED" && result.second.result == "PASSED")
}

function getTokenDescriptorForDao(daoState) {
  const pmd = daoState.modelDataMap["io.cordite.dao.plutus.PlutusModelData"]
  const tokenDescriptor = pmd.issuableTokens[0]
  return tokenDescriptor
}

async function createPlutusProposal(node, daoKey, bps) {
  logStep(`creating plutus proposal`)
  logDoing(`getting dao for key ${JSON.stringify(daoKey)}`)
  const daoState = await node.dao.daoFor(daoKey)
  const tokenDescriptor = getTokenDescriptorForDao(daoState)
  logDoing(`creating plutus proposal for ${tokenDescriptor.symbol}`)
  const proposalState = await node.dao.createPlutusProposal(tokenDescriptor, bps, daoKey)
  logDone(`proposal created with proposal key ${proposalState.proposalKey}`)
  const lifecycleState = proposalState.lifecycleState
  assertThat(`lifecycle state should be OPEN`, `lifecycle state not OPEN: ${lifecycleState}`, lifecycleState == "OPEN")
  return proposalState.proposal.proposalKey
}

async function getX500Name(node) {
  logDoing(`getting node x500 name`)
  const nodeInfo = await node.network.myNodeInfo();
  logDone(`name is: ${nodeInfo.legalIdentities[0].name}`)
  return nodeInfo.legalIdentities[0].name
}

async function voteForProposal(node, proposalKey, vote) {
  logDoing(`voting for proposal ${proposalKey.name} with vote: ${vote}`)
  const result = await node.dao.voteForProposal(proposalKey, {type: vote})
  logDone(`voted`)
}

async function acceptProposal(node, proposalKey, lifecycleState) {
  logDoing(`accepting proposal`)
  try {
    const result = await node.dao.acceptProposal(proposalKey)
    logDone(`proposal lifecycle: ${result.first} result: ${result.second.result}`)
    assertThat(`proposal accepted: ${result.first}`, `proposal has incorrect lifecycle ${result.first}`, result.first == lifecycleState)
  } catch(err) {
    logError(JSON.stringify(err))
    process.exit(1)
  }
}

async function acceptProposalExpectError(node, proposalKey, lifecycleState, voteResult) {
  logDoing(`accepting proposal`)
  try {
    const result = await node.dao.acceptProposal(proposalKey)
    logError(`proposal incorrectly accepted`)
    process.exit(1)
  } catch(err) {
    logVerified(JSON.stringify(err))
  }
}

function logStep(text) {
  logEmptyLine()
  console.log(`🟪 ${text}`)
}

function logDoing(text) {
  console.log(`〰️ ${text}`);
}

function logDone(text) {
  console.log(`✔️  ${text}`);
}

function logVerified(text) {
  console.log(`✅ ${text}`);
}

function logEmptyLine() {
  console.log();
}

function logError(text) {
  console.log(`❌ ${text}`);
}

function assertThat(message, errorMessage, result) {
  if (result == true) {
    logVerified(message)
  } else {
    logError(errorMessage)
    throw new Error(errorMessage)
  }
}

async function balanceForAccount(node, account, symbol) {
  const zeroBalance = "0.00";

  const balances = await node.ledger.balanceForAccount(account);
  if (balances.length == 0) {
    return zeroBalance;
  }

  const filtered = balances.filter((balance) => {
    return balance.amountType.symbol == symbol;
  });

  if (filtered.length == 0) {
      return zeroBalance
  }

  let balance = filtered[0].quantity;

  if (!balance) {
    balance = zeroBalance;
  }

  return balance;
}

async function checkBalanceForAccount(node, account, symbol, expectedAmount) {
  const myName = await getX500Name(node)
  logDoing(`asserting ${myName} balance of ${symbol} in ${account} is ${expectedAmount}`)
  const balance = await balanceForAccount(node, account, symbol);
  if (parseFloat(balance) == expectedAmount) {
    logDone(`balance for account ${account} is ${balance}`);
  } else {
    const msg = `balance for ${account} is ${balance}. expected ${expectedAmount}`;
    logError(msg);
    throw new Error(msg);
  }
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

async function plutusProposalsFor(node, daoKey) {
  return node.dao.plutusProposalsFor(daoKey)
}

async function daoForName(node, name) {
  const daoKey = await keyForDao(node, name)
  return await node.dao.daoFor(daoKey)
}

async function keyForDao(node, daoName) {
  const daoStates = await node.dao.daoInfo(daoName)
  const daoState = daoStates[0]
  return daoState.daoKey
}
