/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const fs = require("fs")
eval(fs.readFileSync("./common.js", "utf-8"))

function displayDao(dao) {
  if (process.argv.length > 3) {
    return dao.name == process.argv.slice(3)[0]
  }
  return true
}

async function getCslNode() {
  if (process.argv.length > 2) {
    const envName = process.argv.slice(2)[0]
    if (envName == 'uat') {
      const [csl, plu, ura] = await connectToUat()
      return csl
    } else if (envName == 'prod') {
      const [csl, plu, ura] = await connectToProd()
      return csl
    }
    const [csl, plu, ura] = await connectToCorner()
    return csl  
  } else {
    logError(`you must specify an environment`)
    process.exit(1)
  }
}

async function entireTest() {  
  const csl = await getCslNode()

  const daos = await csl.dao.listDaos()
  daos.filter(displayDao).forEach((daoState) => {
    console.log(`\ndao: ${daoState.name}`)
    console.log(`\tPlutusModelData:`)
    const pmd = daoState.modelDataMap["io.cordite.dao.plutus.PlutusModelData"]
    // console.log(JSON.stringify(pmd, null, 2))
    console.log(`\t\tcurrentMintingRate: ${pmd.currentMintingRate}`)
    console.log(`\t\ttotalIssued: ${pmd.totalIssued}`)
    console.log(`\t\tminNextProposal: ${pmd.minNextProposal}`)
    console.log(`\t\tproposalPeriod: ${pmd.proposalPeriod}`)
    console.log(`\t\tplutusDaoOwner: ${pmd.plutusDaoOwner}`)
    console.log(`\t\taccountName: ${pmd.accountName}`)
    console.log(`\t\tIssuableTokens:`)
    pmd.issuableTokens.forEach(token => console.log(`\t\t\t`,JSON.stringify(token)))
    console.log(`\tMembershipModelData:`)
    const mmd = daoState.modelDataMap["io.cordite.dao.membership.MembershipModelData"]
    console.log(`\t\tminimumMemberCount: ${mmd.minimumMemberCount}`)
    console.log(`\t\thasMinNumberOfMembers: ${mmd.hasMinNumberOfMembers}`)
    console.log(`\t\toneMemberPerOrg: ${mmd.oneMemberPerOrg}`)
    console.log(`\t\tstrictMode: ${mmd.strictMode}`)
    console.log(`\tMembers:`)
    daoState.members.forEach(member => console.log(`\t\t${member.name}`))
  })

  // or could pull out the above and extract an async function
  if (process.argv.length > 3) {
    // get dao key
    const daoName = process.argv.slice(3)[0]
    const daoKey = await keyForDao(csl, daoName)

    // add current csl balance

    const proposals = await plutusProposalsFor(csl, daoKey)
    console.log(`\tProposals:`)
    proposals.forEach((prop) => {
      console.log(`\t\t${prop.proposal.proposalKey.name}`)
      console.log(`\t\t\tproposer: ${prop.proposer.name}`)
      console.log(`\t\t\tlifecycle: ${prop.lifecycleState}`)
      console.log(`\t\t\tVotes:`)
      prop.votes.forEach((vote) => {
        console.log(`\t\t\t\t${vote.voteType.type}\t - ${vote.voter.name}`)
      })
    })

    // console.log(JSON.stringify(proposals, null, 2))
    // show csl balance
    // show proposals
  }
  process.exit(0)
}

entireTest()
