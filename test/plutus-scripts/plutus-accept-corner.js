const { SSL_OP_EPHEMERAL_RSA } = require("constants")
/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const fs = require("fs")
eval(fs.readFileSync("./common.js", "utf-8"))

function getSalt() {
  if (process.argv.length > 2 && process.argv.slice(2)[0] == 'live') {
    return ""
  }
  return process.argv.slice(2)[0]
}

async function entireTest() {

  let salt = getSalt()

  let saltedDaoName = `plutus${salt}`
  let tokenSymbol = `XKD${salt}`
  
  const [csl, plu, ura] = await connectToCorner()

  logStep("ok, got here")
  let daos = await csl.dao.daoInfo(saltedDaoName)
  logStep(`there were ${daos.length} daos with name ${saltedDaoName}`)

  let daoKey = daos[0].daoKey

  let proposals = await plutusProposalsFor(csl, daoKey)
  let proposalKey = proposals[0].proposal.proposalKey

  logStep(`Get csl balance before`)
  let beforeBalance = await balanceForAccount(csl, saltedDaoName, tokenSymbol)
  logDone(`balance before is: ${beforeBalance}`)

  logStep(`Now trying to accept proposal with key ${proposalKey}`)
  await acceptProposal(csl, proposalKey, "ACCEPTED")

  logStep(`Get csl balance after`)
  let beforeAfter = await balanceForAccount(csl, saltedDaoName, tokenSymbol)
  logDone(`balance after is: ${beforeAfter}`)

  logStep(`Creating next proposal`)
  const nextProposal = await createPlutusProposal(csl, daoKey, 50)

  logStep(`Voting for next proposal`)
  await voteForProposal(plu, nextProposal, "DOWN")
  await voteForProposal(ura, nextProposal, "DOWN")

  process.exit(0)
}

try {
  entireTest()
} catch (err) {
  logError(err)
}
