const { SSL_OP_EPHEMERAL_RSA } = require("constants")
/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const fs = require("fs")
eval(fs.readFileSync("./common.js", "utf-8"))

function getSalt() {
  if (process.argv.length > 2 && process.argv.slice(2)[0] == 'golive') {
    return ""
  }
  return new Date().getTime()
}

async function entireTest() {

  let salt = getSalt()

  let saltedDaoName = `plutus${salt}`
  let tokenSymbol = `XKD${salt}`
  
  const [csl, plu, ura] = await connectToCorner()

  console.log("ok, got here")
  let daos = await csl.dao.daoInfo(saltedDaoName)
  console.log(`there were ${daos.length} daos with name ${saltedDaoName}`)

  logStep(`Creating dao`)
  const daoState = await createPlutusDaoWithSeconds(csl, saltedDaoName, tokenSymbol, 15)

  logStep(`Adding plutus node to the dao`)
  const cslName = await getX500Name(csl)
  await registerAsPlutusMember(plu, saltedDaoName, cslName) 

  logStep(`Adding uratus node to the dao`)
  await registerAsPlutusMember(ura, saltedDaoName, cslName) 

  const proposalKey = await createPlutusProposal(csl, daoState.daoKey, 50)

  logStep(`Voting for and accepting proposal`)
  await voteForProposal(plu, proposalKey, "DOWN")
  await voteForProposal(ura, proposalKey, "DOWN")
  await acceptProposal(csl, proposalKey, "ACCEPTED")

  logStep(`Checking balances on nodes`)
  const symbol = getTokenDescriptorForDao(daoState).symbol
  await checkBalanceForAccount(csl, saltedDaoName, symbol, 333333.34)
  await checkBalanceForAccount(plu, saltedDaoName, symbol, 333333.33)
  await checkBalanceForAccount(ura, saltedDaoName, symbol, 333333.33)

  logStep(`Creating next proposal`)
  const nextProposal = await createPlutusProposal(csl, daoState.daoKey, 50)

  logStep(`Voting for next proposal`)
  await voteForProposal(plu, nextProposal, "DOWN")
  await voteForProposal(ura, nextProposal, "DOWN")

  process.exit(0)
}

try {
  entireTest()
} catch (err) {
  logError(err)
}
