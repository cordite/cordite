<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->

# Plutus stuff

Thought it was worthing noting down some of the commands we use to accept proposals and fire off the next.

## info on all daos

```shell
node dao-info.js uat
```

## info on specific dao

```shell
node dao-info.js prod plutus
```

## To create a test dao with salt

```shell
node plutus-init-corner.js
```

## To accept a test dao with salt

```shell
node plutus-accept-corner.js 1605971688207
```

## To accept the real dao

```shell
node plutus-accept-prod.js live
```
