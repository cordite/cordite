#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

version: "3.5"

networks:
  cordite:

services:
  corda-db:
    image: postgres:10.5
    ports:
      - "5432:5432"
    volumes:
      - ./db-init:/docker-entrypoint-initdb.d/
      - postgres_data:/var/lib/postgresql/data
    networks:
      cordite:
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 10s
      timeout: 5s
      retries: 5
  network-map:
    image: cordite/network-map:v0.5.0
    ports:
      - "9080:9080"
    volumes:
      - nms:/opt/cordite/db
    environment:
      - NMS_PORT=9080
      - NMS_DB=/opt/cordite/db
      - NMS_AUTH_USERNAME=admin
      - NMS_AUTH_PASSWORD=admin
      - NMS_TLS=false
      - NMS_DOORMAN=true
      - NMS_CERTMAN=false
      - NMS_CACHE_TIMEOUT=10S
      - NMS_STORAGE_TYPE=file
    networks:
      cordite:
  notary:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    ports:
      - "9087:8080"
    volumes:
      - ./nodes/notary/certificates:/opt/corda/certificates
      - notary_artemis:/opt/corda/artemis
      - notary_config:/etc/corda
    environment:
      - CORDITE_LEGAL_NAME=O=Notary, OU=Cordite Foundation, L=London, C=GB
      - CORDITE_P2P_ADDRESS=notary:10002
      - NETWORK_MAP_URL=http://network-map:9080
      - CORDITE_NOTARY=non-validating
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/bootstrap
      - CORDITE_DB_MAX_POOL_SIZE=5
      - CORDITE_NMS=true
    depends_on:
      - "corda-db"
    networks:
      cordite:
  emea:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    restart: always:0
    ports:
      - "9082:8080"
    volumes:
      - ./nodes/emea/certificates:/opt/corda/certificates
      - emea_artemis:/opt/corda/artemis
      - emea_config:/etc/corda
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite EMEA, OU=Cordite Foundation, L=London,C=GB
      - CORDITE_P2P_ADDRESS=emea:10002
      - NETWORK_MAP_URL=http://network-map:9080
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/emea
      - CORDITE_DB_MAX_POOL_SIZE=5
      - CORDITE_LOG_MODE=json
      - CORDA_ARGS=--logging-level=TRACE
      - CORDITE_NMS=true
    depends_on:
      - "corda-db"
    networks:
      cordite:
  amer:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    restart: always:0
    ports:
      - "9083:8080"
      - "2223:2223"
    volumes:
      - ./nodes/amer/certificates:/opt/corda/certificates
      - amer_artemis:/opt/corda/artemis
      - amer_config:/etc/corda
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite AMER, OU=Cordite Foundation, L=New York City,C=US
      - CORDITE_P2P_ADDRESS=amer:10002
      - NETWORK_MAP_URL=http://network-map:9080
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/amer
      - CORDITE_DB_MAX_POOL_SIZE=5
      - CORDA_ARGS=--logging-level=TRACE
      - CORDITE_NMS=true
      - CORDITE_SSH_PORT=2223
      - CORDITE_RPC_USERNAME=cordite
      - CORDITE_RPC_PASSWORD=letmein
      - CORDITE_RPC_PERMISSIONS=ALL
    depends_on:
      - "corda-db"
    networks:
      cordite:
  csl:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    restart: always:0
    ports:
      - "9084:8080"
    volumes:
      - ./nodes/csl/certificates:/opt/corda/certificates
      - csl_artemis:/opt/corda/artemis
      - csl_config:/etc/corda
    environment:
      - CORDITE_LEGAL_NAME=CN=csl.cordite.foundation, O=Cordite Society Limited, L=London, C=GB
      - CORDITE_P2P_ADDRESS=csl:10002
      - NETWORK_MAP_URL=http://network-map:9080
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/csl
      - CORDITE_DB_MAX_POOL_SIZE=5
      - CORDA_ARGS=--logging-level=TRACE
      - CORDITE_NMS=true
    depends_on:
      - "corda-db"
    networks:
      cordite:
  apac:
    image: ${IMAGE_TAG:-cordite/cordite:edge}
    restart: always:0
    ports:
      - "9085:8080"
      - "2224:2224"
    volumes:
      - ./nodes/apac/certificates:/opt/corda/certificates
      - apac_artemis:/opt/corda/artemis
      - apac_config:/etc/corda
    environment:
      - CORDITE_LEGAL_NAME=O=Cordite APAC,L=Singapore,C=SG,OU=Cordite Foundation
      - CORDITE_P2P_ADDRESS=apac:10002
      - NETWORK_MAP_URL=http://network-map:9080
      - CORDITE_DB_USER=postgres
      - CORDITE_DB_PASS=postgres
      - CORDITE_DB_DRIVER=org.postgresql.ds.PGSimpleDataSource
      - CORDITE_DB_URL=jdbc:postgresql://corda-db:5432/apac
      - CORDITE_DB_MAX_POOL_SIZE=5
      - CORDA_ARGS=--logging-level=TRACE
      - CORDITE_NMS=true
      - CORDITE_AUTH_USERNAME=geno
      - CORDITE_AUTH_PASSWORD=genossecretpassword
      - CORDITE_SSH_PORT=2224
      - CORDITE_RPC_USERNAME=cordite
      - CORDITE_RPC_PASSWORD=letmein
      - CORDITE_RPC_PERMISSIONS=ALL
    depends_on:
      - "corda-db"
    networks:
      cordite:
volumes:
  nms:
  postgres_data:
  notary_artemis:
  notary_config:
  emea_artemis:
  emea_config:
  amer_artemis:
  amer_config:
  apac_artemis:
  apac_config:
  csl_artemis:
  csl_config: