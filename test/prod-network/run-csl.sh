#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

set -e # stop on error

TEST_VERSION=${1:-cordite/cordite:edge}
ENVIRONMENT=${2:-csl}
JUST_TESTS=${3:-no}


if [ ${JUST_TESTS} != "tests" ]; then
  echo "--------------------------------------------------------"
  echo "starting up ${TEST_VERSION} version of cordite"
  echo "--------------------------------------------------------"

  ./build_prodlike_env.sh ${TEST_VERSION} ${ENVIRONMENT}
fi

SALT=$(date +%s)
DAO_NAME="plutus-${SALT}"

pushd ../regression
./cordite -f scripts/csl-1-csl.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9084
./cordite -f scripts/csl-2-amer.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9083
./cordite -f scripts/csl-3-emea.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9082
./cordite -f scripts/csl-4-csl.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9084
./cordite -f scripts/csl-5-emea.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9082
./cordite -f scripts/csl-6-csl.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9084
./cordite -f scripts/csl-7-emea.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9082 
./cordite -f scripts/csl-8-amer.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9083

docker-compose -p csl stop emea

./cordite -f ../regression/scripts/csl-9-csl.js -a '{"salt":"'${SALT}'","env":"'${ENVIRONMENT}'","plutusDao":"'${DAO_NAME}'"}' https://localhost:9084
popd

if [ ${JUST_TESTS} != "tests" ]; then
  echo "-----------------------------"
  echo "shutting down release version"
  echo "-----------------------------"

  docker-compose -p ${ENVIRONMENT} down --volumes
  docker volume prune -f
else
  docker-compose -p csl start emea
fi