<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Testing in Cordite

From this directory, you will be able to start up a local Cordite environment with a Network Map Service, all running in docker. The docker setup for this environment is specified in the `docker-compose.yml` file.
The configuration runs corda with the official Postgres image, using one database engine containing multiple databases, one per node.
To bootstrap the database the file `db-init/init-corda-db.sql` is run on the database using Postgres image's documented technique. 

Below is a table of the scripts in the test dir, what they are for and how to use them.

| Use | File to use | Example |
|---|---|---|
| Starting up Cordite locally, with all nodes and notaries.  | `./build_env.sh`  | For a **full** stack you will need to use the extra input parameter. Try `./build_env.sh edge dev yes`  |
| Starting up Cordite locally, with only one node and one notary. | `./build_env.sh` | For a **minimum** stack you will need to use the extra input parameter. Try `./build_env.sh edge dev no` |
| Starting up a local Cordite environment, and running integration tests against it | `./test-nms.sh` | Try `./test-nms.sh` |
| Run just the posgres database instance - useful when running nodes from cordapps directory using `deployPostgresNodes` | `./runDatabases.sh` |