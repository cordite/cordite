<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->

# Regression Test

The [`run.sh`](./run.sh) script takes two cordite docker images as parameters: the `baseline` and the `test` images.

Defaults are:

- `baseline`: `cordite/cordite:v0.4.10`
- `test`: `cordite/cordite:local`

The script starts the `baseline`, persisting all state to docker volumes, and executes the first two scripts in the [scripts](./scripts) directory.

Then it shutdown the network, and restarts it using the `test` image, and executes the remaining [scripts](./scripts).
