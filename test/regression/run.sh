#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

set -e # stop on error

BASELINE_VERSION=${1:-cordite/cordite:v0.5.2}
TEST_VERSION=${2:-cordite/cordite:local}
ENVIRONMENT=${3:-min}

echo "--------------------------------------"
echo "starting up release version of cordite"
echo "--------------------------------------"
./build_env.sh ${BASELINE_VERSION} ${ENVIRONMENT}

./cordite -f scripts/step-1-amer.js https://localhost:9083
./cordite -f scripts/step-2-emea.js https://localhost:9082

echo "-----------------------------"
echo "shutting down release version"
echo "-----------------------------"
docker-compose -p ${ENVIRONMENT} down

echo "----------------------------------------"
echo "starting up the local version of cordite"
echo "----------------------------------------"
./restart_env.sh ${TEST_VERSION} ${ENVIRONMENT}

./cordite -f scripts/step-3-amer.js https://localhost:9083
./cordite -f scripts/step-4-emea.js https://localhost:9082

docker-compose -p ${ENVIRONMENT} down --volumes
docker volume prune -f 