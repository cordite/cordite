/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

// NOTARY

const notary = notaries.notary.name;
let allNodes;
let nodesByHost;

// INIT
async function init() {
  allNodes = await network.allNodes();
  nodesByHost = allNodes
    .flatMap((node) => {
      return node.addresses.map((address) => {
        return { key: address.host, value: node };
      });
    })
    .reduce((map, item) => {
      map[item.key] = item.value;
      return map;
    }, {});
}

// LOGGING

function logDoing(text) {
  console.log(`〰️ ${text}`);
}

function logDone(text) {
  console.log(`✔️  ${text}`);
}

function logVerified(text) {
  console.log(`✅ ${text}`);
}

function logEmptyLine() {
  console.log();
}

function logError(text) {
  console.log(`❌ ${text}`);
}

// JSON

function json(obj, pretty) {
  let tabs = 0;
  if (pretty) {
    tabs = 2;
  }
  return JSON.stringify(obj, null, tabs);
}

// NETWORK AND NODES

function formatNode(node) {
  const simpleNode = {
    address: `${node.addresses[0].host}:${node.addresses[0].port}`,
    name: node.legalIdentities[0].name,
    owningKey: node.legalIdentities[0].owningKey,
  };
  return json(simpleNode);
}

function formatNodes(allNodes) {
  return allNodes
    .map((node) => {
      return formatNode(node);
    })
    .join("\n");
}

async function assertMyHost(host) {
  logEmptyLine();
  const nodeInfo = await network.myNodeInfo();
  if (nodeInfo.addresses[0].host !== host) {
    throw new Error(`host "${nodeInfo.addresses[0].host}" is not "${host}"`);
  } else {
    console.log(`connected to ${host}`);
  }
}

async function listAllNodes() {
  logEmptyLine();
  logVerified(`nodes on the network:\n${formatNodes(allNodes)}`);
}

function x500NameForHost(host) {
  const node = nodesByHost[host];
  if (!node) {
    throw new Error(`host ${host} not found`);
  }
  return node.legalIdentities[0].name;
}

function accountAtHost(account, host) {
  return `${account}@${x500NameForHost(host)}`;
}

function accountUri(account, nodeName){
  return `${account}@${nodeName}`;
}

// ACCOUNTS

async function ensureAccountExists(account) {
  try {
    await ledger.getAccount(account);
    logDone(`account ${account} exists`);
  } catch (err) {
    await ledger.createAccount(account, notary);
    logDone(`created account ${account}`);
  }
}

async function assertAccountExists(account) {
  logDoing(`checking account ${account} exists`);
  try {
    await ledger.getAccount(account);
    logDone(`account ${account} exists`);
  } catch (err) {
    const msg = `account ${account} does not exist`;
    logError(msg);
    throw new Error(msg);
  }
}

async function balanceForAccount(account, symbol) {
  const zeroBalance = "0.00";

  const balances = await ledger.balanceForAccount(account);
  if (balances.length == 0) {
    return zeroBalance;
  }

  const filtered = balances.filter((balance) => {
    return balance.amountType.symbol == symbol;
  });

  if (filtered.length == 0) {
      return zeroBalance
  }

  let balance = filtered[0].quantity;

  if (!balance) {
    balance = zeroBalance;
  }

  return balance;
}

async function transactionsForAccount(account) {
  logDoing(`getting transactions for ${account}`);
  const txs = await ledger.transactionsForAccount(account, {
    pageNumber: 1,
    pageSize: 10000,
  });
  logDone(`retrieved transactions for ${account}`);
  return txs;
}

// ASSETS

async function ensureAssetExists(symbol) {
  const tokenTypes = await ledger.listTokenTypes();
  const hasSymbol = tokenTypes.map((tt) => tt.symbol).includes(symbol);
  if (hasSymbol) {
    logDone(`token type ${symbol} exists`);
  } else {
    await ledger.createTokenType(symbol, 2, notary);
    logDone(`token type ${symbol} created`);
  }
}

async function issueAsset(amount, symbol, account, description) {
  logEmptyLine();
  logDoing("preparing issuance...");
  const balanceBefore = await balanceForAccount(account, symbol);
  logVerified(`balance for "${account}" is ${balanceBefore}`);

  logDoing(`issuing ${amount} ${symbol} to ${account}`);
  await ledger.issueToken(account, amount, symbol, description, notary);
  logDone(`issued ${amount} ${symbol} to ${account}`);

  const balanceAfter = await balanceForAccount(account, symbol);
  logVerified(`balance for "${account}" is now ${balanceAfter}`);

  const balanceExpected = parseFloat(balanceBefore) + parseFloat(amount);
  if (balanceExpected == balanceAfter) {
    logVerified("balance is correct");
  } else {
    const msg = `new balance is ${balanceAfter} but was expecting ${balanceExpected}`;
    logError(msg);
    throw new Error(msg);
  }
}

function assertThat(message, errorMessage, result) {
  if (result == true) {
    logVerified(message)
  } else {
    logError(errorMessage)
    throw new Error(errorMessage)
  }
}

async function transfer(
  amount,
  symbol,
  account,
  dstAccount,
  dstHost,
  description
) {
  logEmptyLine();
  logDoing("preparing transfer...");
  const balanceBefore = await balanceForAccount(account, symbol);
  logVerified(`balance for "${account}" is ${balanceBefore}`);

  const dst = accountAtHost(dstAccount, dstHost);
  logDoing(`about to transfer ${amount} ${symbol} from ${account} to ${dst}`);
  const txId = await ledger.transferToken(
    amount,
    symbol,
    account,
    dst,
    description,
    notary
  );
  logDone(`done. txid: ${txId}`);

  const balanceAfter = await balanceForAccount(account, symbol);
  logVerified(`balance for "${account}" is now ${balanceAfter}`);

  const balanceExpected = parseFloat(balanceBefore) - parseFloat(amount);
  if (balanceExpected == balanceAfter) {
    logVerified("balance is correct");
  } else {
    const msg = `new balance is ${balanceAfter} but was expecting ${balanceExpected}`;
    logError(msg);
    throw new Error(msg);
  }
}

async function daoInfo(name) {
  logDoing(`checking for daos with name ${name}`)  
  const daos = await dao.daoInfo(name);
  logDone(`found: ${daos.length}`)
}

async function daoForName(name) {
  const daoKey = await keyForDao(name)
  return await dao.daoFor(daoKey)
}

async function daoFor(daoKey) {
  return await dao.daoFor(daoKey)
}

async function createPlutusDao(daoName, tokenSymbol) {
  logDoing(`creating dao ${daoName}, with token ${tokenSymbol}`)
  const plutusDao = await dao.createPlutusDao(daoName, notary, tokenSymbol, 0.14)
  logDone(`created dao ${plutusDao.name}`)
}

async function registerAsPlutusMember(daoName) {
  logDoing(`creating new member proposal with ${daoName} dao`)
  const csl = nodesByHost.csl.legalIdentities[0].name
  const proposal = await dao.createNewMemberProposal(daoName, csl)
  logDone(`created new member proposal`)
  logDoing(`proposing to accept my membership`)
  const result = await dao.sponsorAcceptProposal(proposal.proposal.proposalKey, proposal.daoKey, csl)
  logDone(`registered as member ${JSON.stringify(result)}`)
  assertThat(`member registered`, `member failed to be registered ${result}`, result.first == "ACCEPTED" && result.second.result == "PASSED")
}

async function removePlutusMember(daoName, memberToRemoveHostName) {
  logDoing(`creating remove member proposal for ${memberToRemoveHostName} from ${daoName}`)
  const memberToRemove = nodesByHost[memberToRemoveHostName].legalIdentities[0].name
  const daoKey = await keyForDao(daoName)
  logDone(`will be removing member with name ${memberToRemove}`)
  const proposal = await dao.createRemoveMemberProposal(daoKey, memberToRemove)
  logDone(`created new member proposal`)
  logDoing(`proposing to accept my removal`)
  const result = await dao.acceptProposal(proposal.proposal.proposalKey)
  logDone(`proposal accepted ${JSON.stringify(result)}`)
  assertThat(`member removed`, `member failed to be removed ${result}`, result.first == "ACCEPTED" && result.second.result == "PASSED")
  const actualDao = await daoFor(daoKey)
  assertThat(`member not in dao members list`, `member failed to be removed from list ${actualDao.members}`, actualDao.members.filter(member => member.name == memberToRemove).length == 0)
}

async function keyForDao(daoName) {
  const daoStates = await dao.daoInfo(daoName)
  assertThat(`there should be 1 dao with name ${daoName}`,`incorrect number of daos with name ${daoName} (${daoStates.length})`, daoStates.length == 1)
  const daoState = daoStates[0]
  return daoState.daoKey
}

async function createPlutusProposal(tokenType, changeMultiple, daoName) {
  logDoing(`creating plutus proposal`)
  const daoKey = await keyForDao(daoName)
  const csl = nodesByHost.csl.legalIdentities[0].name
  const tokenDescriptor = { symbol: tokenType, issuerName: csl}
  const proposalState = await dao.createPlutusProposal(tokenDescriptor, 0.02, daoKey)
  const lifecycleState = proposalState.lifecycleState
  logDone(`created plutus proposal`)
  assertThat(`lifecycle state should be OPEN`, `lifecycle state not OPEN: ${lifecycleState}`, lifecycleState == "OPEN")
}

async function getPlutusProposalKey(daoName) {
  const daoKey = await keyForDao(daoName)
  const proposals = await dao.plutusProposalsFor(daoKey)
  assertThat(`there should be 1 plutus proposal`,`incorrect number of plutus proposals (${proposals.length})`, proposals.length == 1)
  const plutusProposal = proposals[0]
  return plutusProposal.proposal.proposalKey
}

async function voteForProposal(proposalKey, vote) {
  logDoing(`voting for proposal ${proposalKey.name} with vote: ${vote}`)
  const result = await dao.voteForProposal(proposalKey, {type: vote})
  logDone(`voted`)
}

async function acceptProposal(proposalKey, lifecycleState, voteResult) {
  logDoing(`accepting proposal`)
  const result = await dao.acceptProposal(proposalKey)
  logDone(`proposal accepted`)
  assertThat(`proposal accepted: ${result.first}`, `proposal has incorrect lifecycle ${result.first}`, result.first == lifecycleState)
  assertThat(`vote result correct" ${result.second.result}`, `vote result incorrect: ${result.second.result}`, result.second.result == voteResult)
}

function assetUri(assetSymbol, host) {
  return `${assetSymbol}:${host}`;
}

async function transferAccountsToAccounts(
  tokenTypeUri, amount, fromAccountUri, toAccountUri, description, tagList
){
  logDoing(`transferring ${amount} amount from ${fromAccountUri} to ${toAccountUri}`)
  
  const transactionSummary = await ledger.transferAccountsToAccounts(
    tokenTypeUri,
    {[amount]: fromAccountUri},
    {[amount]: toAccountUri},
    description,
    notary,
    tagList
  )
  logDone('transferred');
  return transactionSummary;
}

async function filterTransactionSummaryByTags(type, params) {
  const tagFilter = { type: type, params: params };
  logDoing(`filtering transactions`);
  const filteredTransactionSummary = await ledger.filterTransactionSummaryByTags(tagFilter);
  logDone(`filtered transactions`);
  return filteredTransactionSummary;
}

// dStates

async function createDState(tagList, data, updaterNodesList, readerNodesList) {
  const createDStateRequest = {
    tags: tagList,
    data: data,
    updaters: updaterNodesList,
    readers: readerNodesList,
    notary: notary
  };
  logDoing(`creating dState`);
  const createdDState = await ledger.createDState(createDStateRequest);
  logDone(`created dState`);
  return createdDState;
}

async function filterDStatesByTags(type, params) {
  const tagFilter = { type: type, params: params };
  logDoing(`filtering dState`);
  const filteredDState = await ledger.filterDStatesByTags(tagFilter);
  logDone(`filtered dState`);
  return filteredDState;
}
