/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
eval(read("./scripts/common.js"));

await init();

await assertMyHost("csl");
const currentNodeName = x500NameForHost('csl');

logDoing(`testing filtering dStates by all tags`)

const createdDState = await createDState(
  [
    {"category": "category1", "value": "value1"},
    {"category": "salt", "value": scriptArgs.salt}
  ],
  {"key1": "value1", "key2": "value2", "key3": "value3"},
  [currentNodeName],
  [currentNodeName],
);


const filteredDStates  = await filterDStatesByTags(
  "AllTagFilter", 
  [{"category": "salt", "value": scriptArgs.salt}]
);

if(!filteredDStates || filteredDStates.length !== 1) {
  const msg = "filterDStatesByTags expected to return 1 result";
  logError(msg);
  throw new Error(msg)
}

if(filteredDStates[0].id !== createdDState.id) {
  const msg = "filterDStatesByTags did not return the correct dState";
  logError(msg);
  throw new Error(msg)
}

logDone(`filterDStatesByTags returns expected result`);