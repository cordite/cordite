/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
eval(read("./scripts/common.js"));

await init();

await assertMyHost("csl");
const currentNodeName = x500NameForHost('csl');
console.log(currentNodeName)

const account1Name = `account-1-${scriptArgs.salt}`
const account2Name = `account-2-${scriptArgs.salt}`
const tokenType1Name = `TOK1-${scriptArgs.salt}`

logDoing(`testing filtering transactions by all tags`)

await ensureAccountExists(account1Name);
await ensureAccountExists(account2Name);
await ensureAssetExists(tokenType1Name);
await issueAsset('1000', tokenType1Name, account1Name, "")

const createdAccount1Uri = accountUri(account1Name, currentNodeName);
const createdAccount2Uri = accountUri(account2Name, currentNodeName);
const createdTokenType1Uri = assetUri(tokenType1Name, currentNodeName);

const transactionSummaryId = await transferAccountsToAccounts(
  createdTokenType1Uri,
  '100',
  createdAccount1Uri,
  createdAccount2Uri,
  "",
  [
    {"category": "category1", "value": "value1"},
    {"category": "salt", "value": scriptArgs.salt}
  ],
);

const filteredTransactions = await filterTransactionSummaryByTags(
  "AllTagFilter",
  [{"category": "salt", "value": scriptArgs.salt}]
)

if(!filteredTransactions || filteredTransactions.length !== 1) {
  const msg = "filterTransactionSummaryByTags expected to return 1 result";
  logError(msg);
  throw new Error(msg)
}

if(filteredTransactions[0].transactionId !== transactionSummaryId) {
  const msg = "filterTransactionSummaryByTags did not return the correct transaction summary";
  logError(msg);
  throw new Error(msg)
}

logDone(`filterTransactionSummaryByTags returns expected result`);