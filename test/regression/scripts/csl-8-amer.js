/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
eval(read("./scripts/common.js"));

await init();

await assertMyHost("amer");

logDoing(`checking that the amer account, which did not vote, has the correct amount in it`)

const account = scriptArgs.plutusDao;
const asset = `XCD-${scriptArgs.salt}`;

await assertAccountExists(account);

const balance = await balanceForAccount(account, asset);
if (parseFloat(balance) == 0) {
  logDone(`balance for account ${account} is ${balance}`);
} else {
  const msg = `balance for ${account} is ${balance}. expected 0.00`;
  logError(msg);
  throw new Error(msg);
}