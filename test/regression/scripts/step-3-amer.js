/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
eval(read("./scripts/common.js"));

await init();

await assertMyHost("amer");

await listAllNodes();

const account = "ben";
const asset = 'xtc';

await assertAccountExists(account);

const balance = await balanceForAccount(account, asset);
if (parseFloat(balance) > 5) {
  logDone(`balance for account ${account} is ${balance}`);
} else {
  const msg = `balance for ${account} is ${balance}. expected > 0.00`;
  logError(msg);
  throw new Error(msg);
}

const txs = await transactionsForAccount(account);
if (txs.length === 0) {
  const msg = `expected to find some transactions for account ${account}`;
  logError(msg);
  throw new Error(msg);
}

logDone(`found ${txs.length} transactions for account ${account}`);

await ensureAssetExists("xkcd");

await issueAsset("100.00", "xkcd", "ben", "making an issuance");

await transfer("10.00", "xkcd", "ben", "fuzz", "emea", "xkcd transfer");
